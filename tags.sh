#! /bin/sh

# Command line to generate TAGS file for Emacs

find . -name "*.py" -print | etags -

# Command line to generate TAGS file for Vim

# ctags -R .
