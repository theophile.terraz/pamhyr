#! /bin/sh

# > ssh-output.sh SERVER DESTDIR

# First argument is the server name/addr
# The second argument is the destination directory to copy input data

scp $1:$2/* ./
