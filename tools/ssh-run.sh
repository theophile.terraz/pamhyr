#! /bin/sh

# > ssh-run.sh SERVER DESTDIR SOLVER ARGS INPUT

# First argument is the server name/addr
# The second argument is the destination directory to copy input data
# The third argument is the solver path
# The fourth argument is an solver args separate by ',' or input name
# The sixth argument is the input name or nothing

args=$(echo $4 | tr ',' ' ')

echo "ssh $1 \"cd $2; $3 $args $5\""
ssh $1 "cd $2; $3 $args $5"
