#! /bin/sh

# > ssh-input.sh SERVER DESTDIR

# First argument is the server name/addr
# The second argument is the destination directory to copy input data

ssh $1 mkdir -p $2
scp ./* $1:$2
