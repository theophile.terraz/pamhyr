#! /bin/sh

echo " Setup ENV"

python3 -m venv venv
. venv/bin/activate
pip3 install -U pip
pip3 install -r ./full-requirements.txt
pip3 install -U -r ./full-requirements.txt

echo " UNITTEST"

cd src/
python3 -Walways -m unittest discover -v -t .
cd ..

echo " PEP8"

pycodestyle --exclude="*_to_*.py" ./src

if [ $? -eq 0 ]
then
    echo "OK"
else
    echo "WARNING"
fi
