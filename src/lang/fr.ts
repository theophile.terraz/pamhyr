<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="en_150">
<context>
    <name>About</name>
    <message>
        <location filename="../View/About/Window.py" line="43"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../View/About/Window.py" line="70"/>
        <source>Contributors: </source>
        <translation>Contributeurs : </translation>
    </message>
</context>
<context>
    <name>AdditionalFiles</name>
    <message>
        <location filename="../View/AdditionalFiles/Translate.py" line="29"/>
        <source>Additional files</source>
        <translation>Fichiers supplémentaires</translation>
    </message>
    <message>
        <location filename="../View/AdditionalFiles/Translate.py" line="33"/>
        <source>Edit additional file</source>
        <translation>Éditer le fichier supplémentaire</translation>
    </message>
</context>
<context>
    <name>BasicHydraulicStructures</name>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="34"/>
        <source>Change hydraulic structure type</source>
        <translation>Changement du type d&apos;ouvrage hydraulique</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="39"/>
        <source>Do you want to change the hydraulic structure type and reset hydraulic structure values?</source>
        <translation>Voulez-vous changer le type de cet ouvrage hydraulique et réinitialiser les valeurs ?</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="51"/>
        <source>Discharge coefficient</source>
        <translation>Coefficient de débit</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="54"/>
        <source>Upper elevation (m)</source>
        <translation>Cote de mise en charge (m)</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="57"/>
        <source>Half-angle tangent</source>
        <translation>Tangeante du demi-angle</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="60"/>
        <source>Maximal loading elevation</source>
        <translation>Cote de mise en charge maximale</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="63"/>
        <source>Siltation height (m)</source>
        <translation>Hauteur d&apos;envasement (m)</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="66"/>
        <source>Top of the vault (m)</source>
        <translation>Haut de la voûte (m)</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="69"/>
        <source>Bottom of the vault (m)</source>
        <translation>Bas de la voûte (m)</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="72"/>
        <source>Opening</source>
        <translation>Ouverture</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="75"/>
        <source>Maximal opening</source>
        <translation>Ouverture maximale</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="78"/>
        <source>Step space</source>
        <translation>Pas d&apos;espace</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="81"/>
        <source>Weir</source>
        <translation>Déversoir</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="84"/>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="90"/>
        <source>Parameter 1</source>
        <translation>Paramètre 1</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="93"/>
        <source>Parameter 2</source>
        <translation>Paramètre 2</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="96"/>
        <source>Parameter 3</source>
        <translation>Paramètre 3</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="99"/>
        <source>Parameter 4</source>
        <translation>Paramètre 4</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="102"/>
        <source>Parameter 5</source>
        <translation>Paramètre 5</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="113"/>
        <source>Trapezoidal weir</source>
        <translation>Déversoir trapézoidal</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="116"/>
        <source>Triangular weir</source>
        <translation>Déversoir triangulaire</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="119"/>
        <source>Rectangular orifice</source>
        <translation>Orifice rectangulaire</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="122"/>
        <source>Circular orifice</source>
        <translation>Orifice circulaire</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="125"/>
        <source>Vaulted orifice</source>
        <translation>Orifice voûte</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="128"/>
        <source>Rectangular gate</source>
        <translation>Vanne rectangulaire</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="131"/>
        <source>Simplified rectangular gate</source>
        <translation>Vanne rectangulaire simplifiée</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="134"/>
        <source>Borda-type head loss</source>
        <translation>Perte de charge de type Borda</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="137"/>
        <source>Check valve</source>
        <translation>Clapet</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="140"/>
        <source>User-defined</source>
        <translation>Défini par l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="110"/>
        <source>Rectangular weir</source>
        <translation>Déversoir rectangulaire</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/BasicHydraulicStructures/Translate.py" line="30"/>
        <source>Basic Hydraulic Structures</source>
        <translation>Ouvrages hydrauliques élémentaires</translation>
    </message>
</context>
<context>
    <name>BoundaryCondition</name>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="32"/>
        <source>Edit boundary conditions</source>
        <translation>Éditer les conditions aux limites</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="39"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="40"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="45"/>
        <source>Solid (kg/s)</source>
        <translation>Solide (kg/s)</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/translate.py" line="51"/>
        <source>Point sources</source>
        <translation>Apports ponctuels</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/translate.py" line="52"/>
        <source>Z(t)</source>
        <translation>Z(t)</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/translate.py" line="53"/>
        <source>Q(t)</source>
        <translation>Q(t)</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/translate.py" line="54"/>
        <source>Q(Z)</source>
        <translation>Q(Z)</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/translate.py" line="55"/>
        <source>Solid</source>
        <translation>Solide</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/translate.py" line="61"/>
        <source>Node</source>
        <translation>Nœud</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="35"/>
        <source>Boundary Condition Options</source>
        <translation>Options des conditions limites</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="48"/>
        <source>No geometry</source>
        <translation>Pas de géométrie</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="51"/>
        <source>No geometry found for this reach.
This feature requires a reach with a geometry.</source>
        <translation>Aucune géométrie n&apos;a été trouvée sur ce bief.
Cette fonctionnalité nécessite un bief muni d&apos;une géométrie.</translation>
    </message>
    <message>
        <location filename="../View/BoundaryCondition/Edit/translate.py" line="56"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../View/BoundaryConditionsAdisTS/translate.py" line="37"/>
        <source>Pollutant</source>
        <translation>Polluant</translation>
    </message>
</context>
<context>
    <name>BoundaryConditions</name>
    <message>
        <location filename="../View/BoundaryCondition/translate.py" line="45"/>
        <source>Boundary conditions</source>
        <translation>Conditions aux limites</translation>
    </message>
</context>
<context>
    <name>BoundaryConditionsAdisTS</name>
    <message>
        <location filename="../View/BoundaryConditionsAdisTS/translate.py" line="30"/>
        <source>Boundary conditions AdisTS</source>
        <translation>Conditions aux limites AdisTS</translation>
    </message>
    <message>
        <location filename="../View/BoundaryConditionsAdisTS/Edit/translate.py" line="32"/>
        <source>Edit boundary conditions AdisTS</source>
        <translation>Éditer les conditions aux limites AdisTS</translation>
    </message>
    <message>
        <location filename="../View/BoundaryConditionsAdisTS/Edit/translate.py" line="39"/>
        <source>Mass</source>
        <translation>Masse</translation>
    </message>
    <message>
        <location filename="../View/BoundaryConditionsAdisTS/Edit/translate.py" line="40"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
</context>
<context>
    <name>CheckList</name>
    <message>
        <location filename="../View/CheckList/Translate.py" line="36"/>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
</context>
<context>
    <name>Checker</name>
    <message>
        <location filename="../Checker/Mage.py" line="45"/>
        <source>Mage network graph {mode} checker</source>
        <translation>Vérificateur {mode} du graphe du réseau hydraulique pour Mage</translation>
    </message>
    <message>
        <location filename="../Checker/Mage.py" line="47"/>
        <source>Check if the network graph is valid</source>
        <translation>Vérifie si le graphe réseau est valide</translation>
    </message>
    <message>
        <location filename="../Checker/Mage.py" line="209"/>
        <source>Mage geometry guideline checker</source>
        <translation>Vérificateur des lignes directrices pour Mage</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="37"/>
        <source>Study reach network checker</source>
        <translation>Vérificateur des biefs de l&apos;étude</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="64"/>
        <source>Study geometry checker</source>
        <translation>Vérificateur de géometrie de l&apos;étude</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="65"/>
        <source>Check if the geometry of each reach exists</source>
        <translation>Vérifie si la géométrie de chaque bief de l&apos;étude existe</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="224"/>
        <source>Dummy ok</source>
        <translation>Dummy ok</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="240"/>
        <source>Dummy warning</source>
        <translation>Dummy warning</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="256"/>
        <source>Dummy error</source>
        <translation>Dummy error</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="105"/>
        <source>Study initial conditions checker</source>
        <translation>Vérificateur des conditions initial de l&apos;étude</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="106"/>
        <source>Check initial conditions for each node of study</source>
        <translation>Vérifie les conditions initial de l&apos;étude pour chaque nœud</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="154"/>
        <source>Study boundary conditions checker</source>
        <translation>Vérificateur des conditions aux limites de l&apos;étude</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="155"/>
        <source>Check boundary conditions for each node of study</source>
        <translation>Vérifie les conditions aux limites de l&apos;étude pour chaque nœud</translation>
    </message>
    <message>
        <location filename="../Checker/Mage.py" line="210"/>
        <source>Check if exists geometry guidelines are correctly defined for each reach</source>
        <translation>Vérifie si il existe des lignes directrices correctement définie pour chaque bief</translation>
    </message>
    <message>
        <location filename="../Checker/Study.py" line="38"/>
        <source>Check if exists at least one reach exists</source>
        <translation>Vérificateur si il exists au moins un bief</translation>
    </message>
    <message>
        <location filename="../Checker/Adists.py" line="37"/>
        <source>AdisTS output RK checker</source>
        <translation>Vérifie les PK de sortie AdisTS</translation>
    </message>
    <message>
        <location filename="../Checker/Adists.py" line="38"/>
        <source>Check output RK</source>
        <translation>Vérifie les PK de sortie</translation>
    </message>
</context>
<context>
    <name>Checklist</name>
    <message>
        <location filename="../View/CheckList/Translate.py" line="30"/>
        <source>Check list</source>
        <translation>Liste des vérificateurs</translation>
    </message>
</context>
<context>
    <name>CommonWord</name>
    <message>
        <location filename="../View/Translate.py" line="30"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="32"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="33"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="34"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="35"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="37"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="38"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="40"/>
        <source>Reach</source>
        <translation>Bief</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="41"/>
        <source>Reaches</source>
        <translation>Biefs</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="43"/>
        <source>Main channel</source>
        <translation>Lit mineur</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="44"/>
        <source>Floodway</source>
        <translation>Lit moyen</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="46"/>
        <source>Not defined</source>
        <translation>Non défini</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="47"/>
        <source>Not associated</source>
        <translation>Non associé</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="42"/>
        <source>Cross-section</source>
        <translation>Section en travers</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="31"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="51"/>
        <source>Method</source>
        <translation>Méthode</translation>
    </message>
</context>
<context>
    <name>Configure</name>
    <message>
        <location filename="../View/Configure/Translate.py" line="30"/>
        <source>Configure</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../View/Configure/Translate.py" line="41"/>
        <source>Edit solver</source>
        <translation>Éditer le solveur</translation>
    </message>
    <message>
        <location filename="../View/Configure/Translate.py" line="44"/>
        <source>Add a new solver</source>
        <translation>Ajouter un nouveau solveur</translation>
    </message>
</context>
<context>
    <name>CustomPlot</name>
    <message>
        <location filename="../View/Results/CustomPlot/Translate.py" line="30"/>
        <source>Custom plot selection</source>
        <translation>Sélection des graphiques personnalisés</translation>
    </message>
    <message>
        <location filename="../View/Results/CustomPlot/Translate.py" line="61"/>
        <source>Elevation (m)</source>
        <translation>Cote (m)</translation>
    </message>
</context>
<context>
    <name>D90AdisTS</name>
    <message>
        <location filename="../View/D90AdisTS/translate.py" line="30"/>
        <source>D90 AdisTS</source>
        <translation>D90 AdisTS</translation>
    </message>
</context>
<context>
    <name>DIFAdisTS</name>
    <message>
        <location filename="../View/DIFAdisTS/translate.py" line="30"/>
        <source>DIF AdisTS</source>
        <translation>DIF AdisTS</translation>
    </message>
</context>
<context>
    <name>Debug</name>
    <message>
        <location filename="../View/Debug/Window.py" line="57"/>
        <source>Debug REPL</source>
        <translation>REPL de débogage</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../View/ui/SelectSolver.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineDialog.ui" line="50"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="34"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="48"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="65"/>
        <source>Solver</source>
        <translation>Solveur</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="93"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="100"/>
        <source>Output formater</source>
        <translation>Formateur de sortie</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="110"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Command line format, for input formater (optional), solver execution and output formater (optional). This format can use some replacement values like:&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@install_dir&lt;/span&gt;: The Pamhyr2 install path&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@path&lt;/span&gt;: The associate path&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@input&lt;/span&gt;: Solver input data (depend of solver type)&lt;/p&gt;&lt;p&gt;-&lt;span style=&quot; font-style:italic;&quot;&gt; @output&lt;/span&gt;: Solver output data (depend of solver type)&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@dir&lt;/span&gt;: The working dir at solver execution&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@args&lt;/span&gt;: Solver arguments and additional arguments defined on solver parameters&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Format des lignes de commandes. Ce format peut contenir des valeurs de remplacement, telles que :&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@install_dir&lt;/span&gt;: Le dossier d&apos;installation de Pamhyr2&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@path&lt;/span&gt;: Le chemin associé à cette commande&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@input&lt;/span&gt;: Les données d&apos;entrées du solveur générées par Pamhyr2 (dépend du solveur)&lt;/p&gt;&lt;p&gt;-&lt;span style=&quot; font-style:italic;&quot;&gt; @output&lt;/span&gt;: Les données d&apos;entrées du solveur générées par Pamhyr2 (dépend du solveur)&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@dir&lt;/span&gt;: Le chemin vers le dossier d&apos;execution du solveur&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-style:italic;&quot;&gt;@args&lt;/span&gt;: Arguments du solveur et arguments complémentaires définis dans les paramètres du solveur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="116"/>
        <source>Command line</source>
        <translation>Ligne de commande</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureAddSolverDialog.ui" line="129"/>
        <source>Input formater</source>
        <translation>Formateur d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../View/ui/SelectSolver.ui" line="41"/>
        <source>Run</source>
        <translation>Lancer</translation>
    </message>
    <message>
        <location filename="../View/ui/SelectSolver.ui" line="52"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../View/ui/DialogReadingResults.ui" line="24"/>
        <source>Read results:</source>
        <translation>Lecture des résultats :</translation>
    </message>
    <message>
        <location filename="../View/ui/DialogReadingResults.ui" line="52"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="47"/>
        <source>Solvers</source>
        <translation>Solveurs</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="148"/>
        <source>Backup</source>
        <translation>Sauvegarde auto</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="160"/>
        <source>Auto save</source>
        <translation>Sauvegarde auto</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="167"/>
        <source>Frequency</source>
        <translation>Fréquence</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="178"/>
        <source>Enable</source>
        <translation>Activée</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="204"/>
        <source>HH:mm:ss</source>
        <translation>HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="328"/>
        <source>Editor</source>
        <translation>Editeur</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="352"/>
        <source>This value must be used for reading or editing files in speficic cases.</source>
        <translation>Cette valeur peut être utilisée dans des cas spécifiques pour lire ou écrire dans un fichier.</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="361"/>
        <source>Editor command</source>
        <translation>Commande d&apos;modification</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="377"/>
        <source>  - The &quot;@file&quot; keyword is replaced by the path of file to open.</source>
        <translation>  - Le mot clef &quot;@file&quot; sera remplacé par le chemin du fichier à ouvrir.</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="407"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="398"/>
        <source>Please restart application after language modification</source>
        <translation>Un redémarrage de l&apos;application est requis après le changement de langue</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="47"/>
        <source>Time system</source>
        <translation>System de temps</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="101"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="113"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="130"/>
        <source>Starting date</source>
        <translation>Date de départ</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="140"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <translation>dd/MM/yyyy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="153"/>
        <source>Creation date :</source>
        <translation>Date de création :</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="167"/>
        <source>Last modification :</source>
        <translation>Dernière modification :</translation>
    </message>
    <message>
        <location filename="../View/ui/CustomExportAdisDialog.ui" line="37"/>
        <source>X axis:</source>
        <translation>Axe X :</translation>
    </message>
    <message>
        <location filename="../View/ui/CustomExportAdisDialog.ui" line="48"/>
        <source>Y axis:</source>
        <translation>Axe Y :</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Height.ui" line="49"/>
        <source>Discharge</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="168"/>
        <source>First cross-section</source>
        <translation>Première section en travers</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="189"/>
        <source>Last cross-section</source>
        <translation>Dernière section en travers</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="133"/>
        <source>First guideline</source>
        <translation>Première ligne directrice</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="114"/>
        <source>Guideline used for distance computation</source>
        <translation>Lignes directrices pour le calcul des distances</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="79"/>
        <source>Spline</source>
        <translation>Spline</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="89"/>
        <source>Linear</source>
        <translation>Linéaire</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="60"/>
        <source>Edition</source>
        <translation>Édition</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="63"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Markdown is a plain text format (&lt;a href=&quot;https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cheatsheet&lt;/span&gt;&lt;/a&gt;).&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Title:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;# Title&lt;/p&gt;&lt;p&gt;## Subtitle&lt;/p&gt;&lt;p&gt;### Subsubtitle&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Emphasis:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;**&lt;span style=&quot; font-weight:600;&quot;&gt;bold&lt;/span&gt;**&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;_italic_&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Lists:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- First&lt;/p&gt;&lt;p&gt;- Second&lt;/p&gt;&lt;p&gt;1. First&lt;/p&gt;&lt;p&gt;2. Second&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Quote:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&amp;gt; This is a quote&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Code:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;```python&lt;/p&gt;&lt;p&gt;print(&amp;quot;hello&amp;quot;)&lt;/p&gt;&lt;p&gt;```&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Inline HTML:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&amp;lt;font color=&amp;quot;red&amp;quot;&amp;gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;foo bar&lt;/span&gt;&amp;lt;/font&amp;gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Markdown est un format de texte brut (&lt;a href=&quot;https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;antisèche&lt;/span&gt;&lt;/a&gt;).&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Titres :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;# Titre&lt;/p&gt;&lt;p&gt;## SousTitre&lt;/p&gt;&lt;p&gt;### SousSoutTitre&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Accentuations :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;**&lt;span style=&quot; font-weight:600;&quot;&gt;gras&lt;/span&gt;**&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;_italique_&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Listes :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;- Premier&lt;/p&gt;&lt;p&gt;- Second&lt;/p&gt;&lt;p&gt;1. Premier&lt;/p&gt;&lt;p&gt;2. Second&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Citations :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&amp;gt; Ceci est une citation&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Code :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;```python&lt;/p&gt;&lt;p&gt;print(&amp;quot;hello&amp;quot;)&lt;/p&gt;&lt;p&gt;```&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;HTML :&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&amp;lt;font color=&amp;quot;red&amp;quot;&amp;gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;foo bar&lt;/span&gt;&amp;lt;/font&amp;gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../View/ui/NewStudy.ui" line="77"/>
        <source>Preview</source>
        <translation>Prévisualisation</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineDialog.ui" line="22"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineDialog.ui" line="40"/>
        <source>Line</source>
        <translation>Ligne</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="157"/>
        <source>Limits</source>
        <translation>Limites</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditionsDialogGenerator.ui" line="14"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="36"/>
        <source>Space step (m)</source>
        <translation>Pas d&apos;espace (m)</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="70"/>
        <source>Type of interpolation:</source>
        <translation>Type d&apos;interpolation :</translation>
    </message>
    <message>
        <location filename="../View/ui/ConfigureDialog.ui" line="241"/>
        <source>Stricklers</source>
        <translation>Stricklers</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="23"/>
        <source>Distance computation</source>
        <translation>Calcul des distances</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="32"/>
        <source>Second guide-line</source>
        <translation>Deuxième ligne directrice</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="55"/>
        <source>First guide-line</source>
        <translation>Première ligne directrice</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="78"/>
        <source>Origin</source>
        <translation>Origine</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="92"/>
        <source>Origin value</source>
        <translation>Valeur à l&apos;origine</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="30"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineDialog.ui" line="57"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Comment lines start with &apos;*&apos; char (let see Mage documentation for more details)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Les lignes de commentaire commencent par un caractère &apos;*&apos; (voir la documentation de Mage pour plus de détails)</translation>
    </message>
    <message>
        <location filename="../View/ui/MeshingOptions.ui" line="123"/>
        <source>Second guideline</source>
        <translation>Seconde ligne directrice</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReachShift.ui" line="35"/>
        <source>Y coordinate</source>
        <translation>Coordonnées Y</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReachShift.ui" line="42"/>
        <source>X coordinate</source>
        <translation>Coordonnées X</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReachShift.ui" line="78"/>
        <source>Z coordinate</source>
        <translation>Coordonnées Z</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="39"/>
        <source>Upstream to downstream</source>
        <translation>De l&apos;amont à l&apos;aval</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="99"/>
        <source>Downstream to upstream</source>
        <translation>De l&apos;aval à l&apos;amont</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="116"/>
        <source>Orientation</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <location filename="../View/ui/UpdateRKOptions.ui" line="126"/>
        <source>Keep current</source>
        <translation>Actuelle</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Height.ui" line="25"/>
        <source>Upstream height (m)</source>
        <translation>Cote à l&apos;amont (m)</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Height.ui" line="73"/>
        <source>Downstream height (m)</source>
        <translation>Cote à l&apos;aval (m)</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Height.ui" line="107"/>
        <source>Generate constant discharge</source>
        <translation>Générer un débit (uniforme)</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditionsDialogGenerator.ui" line="38"/>
        <source>Slope</source>
        <translation>Pente</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditionsDialogGenerator.ui" line="55"/>
        <source>Estimate</source>
        <translation>Estimer</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Discharge.ui" line="54"/>
        <source>Generate height</source>
        <translation>Générer une profondeur</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Depth.ui" line="35"/>
        <source>Depth (m)</source>
        <translation>Profondeur (m)</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Depth.ui" line="51"/>
        <source>Generate discharge</source>
        <translation>Générer un débit</translation>
    </message>
    <message>
        <location filename="../View/ui/PurgeOptions.ui" line="30"/>
        <source>Maximum number of points to keep</source>
        <translation>Nombre de points maxi à conserver</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions_Dialog_Generator_Discharge.ui" line="35"/>
        <source>Discharge (m^3/s)</source>
        <translation>Débit (m³/s)</translation>
    </message>
    <message>
        <location filename="../View/ui/CustomExportAdisDialog.ui" line="59"/>
        <source>Pollutant:</source>
        <translation>Polluant:</translation>
    </message>
    <message>
        <location filename="../View/ui/SelectSolverAdisTS.ui" line="23"/>
        <source>AdisTS Solver:</source>
        <translation>Solveur AdisTS:</translation>
    </message>
    <message>
        <location filename="../View/ui/SelectSolverAdisTS.ui" line="71"/>
        <source>Mage Repertory:</source>
        <translation>Répertoire Mage:</translation>
    </message>
    <message>
        <location filename="../View/ui/CompareSolvers.ui" line="48"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>Documentation</name>
    <message>
        <location filename="../View/Doc/Window.py" line="60"/>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
</context>
<context>
    <name>Exception</name>
    <message>
        <location filename="../Model/Except.py" line="55"/>
        <source>Generic error message</source>
        <translation>Message d&apos;erreur générique</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="61"/>
        <source>Undefined error message</source>
        <translation>Message d&apos;erreur non définie</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="76"/>
        <source>Method not implemented</source>
        <translation>Méthode non implémentée</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="105"/>
        <source>Method</source>
        <translation>Méthode</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="105"/>
        <source>not implemented</source>
        <translation>non implémenté</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="105"/>
        <source>for class</source>
        <translation>pour la classe</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="102"/>
        <source>Not implemented method</source>
        <translation>Méthode non implémentée</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="116"/>
        <source>FileFormatError</source>
        <translation>ErreurFormatDeFichier</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="124"/>
        <source>Invalid file format:</source>
        <translation>Format de fichier invalide :</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="130"/>
        <source>File format error</source>
        <translation>Erreur de format de fichier</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="133"/>
        <source>Invalid file format</source>
        <translation>Format de fichier invalide</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="136"/>
        <source>Invalid file</source>
        <translation>Fichier invalide</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="136"/>
        <source>format because of</source>
        <translation>format à cause de</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="176"/>
        <source>Clipboard format error</source>
        <translation>Erreur de format dans le presse-papier</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="158"/>
        <source>without header</source>
        <translation>Sans en-tête</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="160"/>
        <source>with header</source>
        <translation>Avec en-tête</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="165"/>
        <source>Invalid clipboard data format:</source>
        <translation>Format des données du presse-papier invalide :</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="179"/>
        <source>Clipboard format unknown</source>
        <translation>Presse-papier format inconnu</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="214"/>
        <source>External file dependence is missing</source>
        <translation>Fichier d&apos;une dépendence externe manquant</translation>
    </message>
    <message>
        <location filename="../Model/Except.py" line="194"/>
        <source>&apos;@file&apos; is missing for module @module:
&apos;@path&apos;</source>
        <translation>le fichier &apos;@file&apos; est manquant pour le module @module : &apos;@path&apos;</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../View/ui/dummy.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../View/ui/about.ui" line="59"/>
        <source>About Pamhyr2</source>
        <translation>À propos de Pamhyr2</translation>
    </message>
    <message>
        <location filename="../View/ui/about.ui" line="269"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../View/ui/about.ui" line="168"/>
        <source>License: GPLv3+</source>
        <translation>Licence : GPLv3+</translation>
    </message>
    <message>
        <location filename="../View/ui/about.ui" line="228"/>
        <source>&lt;a href=&quot;https://gitlab.irstea.fr/theophile.terraz/pamhyr&quot;&gt;Source code&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;https://gitlab.irstea.fr/theophile.terraz/pamhyr&quot;&gt;Code source&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/extendedDateTimeEdit.ui" line="35"/>
        <source>dd/MM/yyyy HH:mm:ss</source>
        <translation>dd/MM/yyyy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/d50sigma.ui" line="30"/>
        <source>D50</source>
        <translation>D50</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/d50sigma.ui" line="51"/>
        <source>Sigma</source>
        <translation>Sigma</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/extendedTimeEdit.ui" line="39"/>
        <source>days</source>
        <translation>jours</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/extendedTimeEdit.ui" line="55"/>
        <source>HH:mm:ss</source>
        <translation>HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="48"/>
        <source>Study</source>
        <translation>Étude</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="64"/>
        <source>@study_name</source>
        <translation>@study_name</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="74"/>
        <source>River network</source>
        <translation>Réseau hydrographique</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="90"/>
        <source>Reach:</source>
        <translation>Bief :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="117"/>
        <source>Node:</source>
        <translation>Nœud :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="127"/>
        <source>@nb_nodes</source>
        <translation>@nb_nodes</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="144"/>
        <source>@nb_edges</source>
        <translation>@nb_edges</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="110"/>
        <source>Current reach:</source>
        <translation>Bief actuel :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="83"/>
        <source>@current_reach</source>
        <translation>@current_reach</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="202"/>
        <source>Geometry</source>
        <translation>Géometrie</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="244"/>
        <source>@nb_points</source>
        <translation>@nb_points</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="251"/>
        <source>Cross-sections:</source>
        <translation>Section en travers :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="258"/>
        <source>Points:</source>
        <translation>Points :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="268"/>
        <source>@nb_cs</source>
        <translation>@nb_cs</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="134"/>
        <source>@nb_res</source>
        <translation>@nb_res</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="151"/>
        <source>Boundary conditions:</source>
        <translation>Conditions aux limites :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="158"/>
        <source>Reservoir:</source>
        <translation>Casier :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="165"/>
        <source>@nb_bc</source>
        <translation>@nb_bc</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="185"/>
        <source>Lateral sources:</source>
        <translation>Apports latéraux :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="192"/>
        <source>@nb_lc</source>
        <translation>@nb_lc</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="234"/>
        <source>Hydraulic stuctures:</source>
        <translation>Ouvrages hydrauliques :</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabInfo.ui" line="275"/>
        <source>@nb_hs</source>
        <translation>@nb_hs</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabCheckers.ui" line="29"/>
        <source>Checkers list</source>
        <translation>Liste des vérificateurs</translation>
    </message>
    <message>
        <location filename="../View/ui/Widgets/MainWindowTabCheckers.ui" line="39"/>
        <source>Errors summary</source>
        <translation>Résumé des erreurs</translation>
    </message>
    <message>
        <location filename="../View/ui/about.ui" line="131"/>
        <source>Version: @version @codename</source>
        <translation>Version : @version @codename</translation>
    </message>
    <message>
        <location filename="../View/ui/about.ui" line="94"/>
        <source>Copyright &#xa9; 2022-2025  INRAE</source>
        <translation type="obsolete">Copyright © 2022-2025  INRAE</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../View/ui/about.ui" line="94"/>
        <source>Copyright © 2022-2025  INRAE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Frictions</name>
    <message>
        <location filename="../View/Frictions/translate.py" line="45"/>
        <source>Start (m)</source>
        <translation>PK de départ (m)</translation>
    </message>
    <message>
        <location filename="../View/Frictions/translate.py" line="46"/>
        <source>End (m)</source>
        <translation>PK de fin (m)</translation>
    </message>
    <message>
        <location filename="../View/Frictions/translate.py" line="34"/>
        <source>Stricklers</source>
        <translation>Stricklers</translation>
    </message>
    <message>
        <location filename="../View/Frictions/translate.py" line="38"/>
        <source>Edit frictions</source>
        <translation>Éditer les frottements</translation>
    </message>
    <message>
        <location filename="../View/Frictions/translate.py" line="31"/>
        <source>Strickler ($m^{1/3}/s$)</source>
        <translation>Strickler ($m^{1/3}/s$)</translation>
    </message>
    <message>
        <location filename="../View/Frictions/translate.py" line="47"/>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
</context>
<context>
    <name>Geometry</name>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="52"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="53"/>
        <source>Y (m)</source>
        <translation>Y (m)</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="54"/>
        <source>Z (m)</source>
        <translation>Z (m)</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="30"/>
        <source>Geometry</source>
        <translation>Géometrie</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="34"/>
        <source>Open a file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="35"/>
        <source>Mage geometry file (*.ST *.st)</source>
        <translation>Fichier Mage de géométrie (*.ST *.st)</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="37"/>
        <source>Mage meshed geometry file (*.M *.m)</source>
        <translation>Fichier Mage de géométrie maillée (*.M *.m)</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="41"/>
        <source>All files (*)</source>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="45"/>
        <source>cross-section</source>
        <translation>section en travers</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="46"/>
        <source>cross-sections</source>
        <translation>sections en travers</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="48"/>
        <source>Transverse abscissa (m)</source>
        <translation>Abscisse en travers (m)</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="52"/>
        <source>Previous cross-section</source>
        <translation>Section en travers précédente</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="55"/>
        <source>Cross-section</source>
        <translation>Section en travers</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="56"/>
        <source>Next cross-section</source>
        <translation>Section en travers suivante</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="65"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="82"/>
        <source>Meshing</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Profile/Translate.py" line="31"/>
        <source>Geometry cross-section</source>
        <translation>Géométrie des sections en travers</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Table.py" line="71"/>
        <source>upstream</source>
        <translation>amont</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Table.py" line="74"/>
        <source>downstream</source>
        <translation>aval</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="39"/>
        <source>Shapefile (*.SHP *.shp)</source>
        <translation>Fichier shape (*.SHP *.shp)</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="69"/>
        <source>the first guide-line</source>
        <translation>la première ligne directrice</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="70"/>
        <source>the second guide-line</source>
        <translation>la seconde ligne directrice</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Profile/Translate.py" line="35"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Profile/Translate.py" line="36"/>
        <source>Area</source>
        <translation>Aire</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Profile/Translate.py" line="37"/>
        <source>Perimeter</source>
        <translation>Périmètre</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Profile/Translate.py" line="44"/>
        <source>Traversal abs (m)</source>
        <translation>Abscisse en travers (m)</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="71"/>
        <source>the mean over the two guide-lines</source>
        <translation>la moyenne entre les deux ligne directrice</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="85"/>
        <source>UpdateRK</source>
        <translation>Mise à jour PK</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="88"/>
        <source>Purge</source>
        <translation>Simplifier</translation>
    </message>
    <message>
        <location filename="../View/Geometry/Translate.py" line="91"/>
        <source>Shift</source>
        <translation>Translater</translation>
    </message>
</context>
<context>
    <name>HydraulicStructures</name>
    <message>
        <location filename="../View/HydraulicStructures/Translate.py" line="30"/>
        <source>Hydraulic Structures</source>
        <translation>Ouvrages hydrauliques</translation>
    </message>
    <message>
        <location filename="../View/HydraulicStructures/Translate.py" line="34"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
</context>
<context>
    <name>InitialCondition</name>
    <message>
        <location filename="../View/InitialConditions/translate.py" line="30"/>
        <source>Initial conditions</source>
        <translation>Conditions initiales</translation>
    </message>
</context>
<context>
    <name>InitialConditionAdisTS</name>
    <message>
        <location filename="../View/InitialConditionsAdisTS/translate.py" line="30"/>
        <source>Initial condition AdisTS</source>
        <translation>Conditions initiales AdisTS</translation>
    </message>
</context>
<context>
    <name>LateralContribution</name>
    <message>
        <location filename="../View/LateralContribution/Edit/translate.py" line="30"/>
        <source>Edit lateral sources</source>
        <translation>Éditer les apports latéraux</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/Edit/translate.py" line="35"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/Edit/translate.py" line="36"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="47"/>
        <source>Lateral sources</source>
        <translation>Contributions latérales</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="48"/>
        <source>Rain</source>
        <translation>Pluie</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="49"/>
        <source>Evaporation</source>
        <translation>Évaporation</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="60"/>
        <source>Start (m)</source>
        <translation>PK de départ (m)</translation>
    </message>
    <message>
        <location filename="../View/LateralContribution/translate.py" line="61"/>
        <source>End (m)</source>
        <translation>PK de fin (m)</translation>
    </message>
    <message>
        <location filename="../View/LateralContributionsAdisTS/translate.py" line="36"/>
        <source>Lateral contribution</source>
        <translation>Apports latéraux</translation>
    </message>
    <message>
        <location filename="../View/LateralContributionsAdisTS/translate.py" line="47"/>
        <source>Begin rk (m)</source>
        <translation>Pk début</translation>
    </message>
    <message>
        <location filename="../View/LateralContributionsAdisTS/translate.py" line="48"/>
        <source>End rk (m)</source>
        <translation>Pk fin</translation>
    </message>
</context>
<context>
    <name>LateralContributionAdisTS</name>
    <message>
        <location filename="../View/LateralContributionsAdisTS/Edit/translate.py" line="30"/>
        <source>Edit lateral contribution AdisTS</source>
        <translation>Éditer les apports latéraux AdisTS</translation>
    </message>
    <message>
        <location filename="../View/LateralContributionsAdisTS/Edit/translate.py" line="37"/>
        <source>Mass Flow</source>
        <translation>Flux massique</translation>
    </message>
    <message>
        <location filename="../View/LateralContributionsAdisTS/translate.py" line="30"/>
        <source>Lateral contribution AdisTS</source>
        <translation>Apports latéraux AdisTS</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../View/Translate.py" line="160"/>
        <source>Open debug window</source>
        <translation>Ouvrir la fenêtre de débogage</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="163"/>
        <source>Open SQLite debuging tool (&apos;sqlitebrowser&apos;)</source>
        <translation>Ouvrir l&apos;outil de débogage SQLite (&apos;sqlitebrowser&apos;)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="166"/>
        <source>Enable this window</source>
        <translation>Activer cette fenêtre</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="70"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../View/ui/ReservoirList.ui" line="84"/>
        <source>Add a new reservoir</source>
        <translation>Ajouter un casier</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="82"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../View/ui/ReservoirList.ui" line="96"/>
        <source>Delete reservoirs</source>
        <translation>Supprimer les casier(s)</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributionsAdisTS.ui" line="112"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../View/ui/ReservoirList.ui" line="108"/>
        <source>Edit reservoir equations</source>
        <translation>Éditer la loi du casier</translation>
    </message>
    <message>
        <location filename="../View/ui/Network.ui" line="118"/>
        <source>Add node or edge</source>
        <translation>Ajouter un nœud ou une arête</translation>
    </message>
    <message>
        <location filename="../View/ui/Network.ui" line="130"/>
        <source>Remove node or edge</source>
        <translation>Supprimer un nœud ou une arête</translation>
    </message>
    <message>
        <location filename="../View/ui/EditSedimentLayers.ui" line="63"/>
        <source>Add sediment layer</source>
        <translation>Ajouter une couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../View/ui/EditSedimentLayers.ui" line="78"/>
        <source>Delete sediment layer</source>
        <translation>Supprimer une couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../View/ui/SedimentLayersList.ui" line="86"/>
        <source>Edit sediment layer</source>
        <translation>Éditer la couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="275"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="37"/>
        <source>PAMHYR</source>
        <translation>PAMHYR</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="102"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="120"/>
        <source>&amp;River network</source>
        <translation>&amp;Réseau</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="129"/>
        <source>&amp;Geometry</source>
        <translation>&amp;Géometrie</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="138"/>
        <source>&amp;Execute</source>
        <translation>&amp;Exécuter</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="149"/>
        <source>&amp;Hydraulics</source>
        <translation>&amp;Hydraulique</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="160"/>
        <source>&amp;Results</source>
        <translation>&amp;Résultats</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="169"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="173"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="177"/>
        <source>Pamhyr2 </source>
        <translation>Pamhyr2 </translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="191"/>
        <source>&amp;Sediment</source>
        <translation>&amp;Sédiment</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="202"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenêtres</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="349"/>
        <source>New study</source>
        <translation>Nouvelle étude</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributionsAdisTS.ui" line="88"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="377"/>
        <source>Open a study</source>
        <translation>Ouvrir une étude</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="380"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="389"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="562"/>
        <source>Close current study</source>
        <translation>Fermer l&apos;étude en cours</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="401"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="404"/>
        <source>Save study</source>
        <translation>Sauvegarder l&apos;étude</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="407"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="416"/>
        <source>Save as ...</source>
        <translation>Sauvegarder sous ...</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="419"/>
        <source>Save study as ...</source>
        <translation>Sauvegarder l&apos;étude sous ...</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="422"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="427"/>
        <source>Pamhyr2 configuration</source>
        <translation>Configuration de Pamhyr2</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="436"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="439"/>
        <source>Quit application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="442"/>
        <source>Ctrl+F4</source>
        <translation>Ctrl+F4</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="592"/>
        <source>Edit river network</source>
        <translation>Éditer le réseau</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="460"/>
        <source>Edit geometry</source>
        <translation>Éditer la géométrie</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="106"/>
        <source>Import geometry</source>
        <translation>Importer une géométrie</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="118"/>
        <source>Export geometry</source>
        <translation>Exporter la géométrie</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="468"/>
        <source>Numerical parameters of solvers</source>
        <translation>Paramètres numériques des solveurs</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="736"/>
        <source>Boundary conditions and point sources</source>
        <translation>Conditions aux limites et apports ponctuels</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="664"/>
        <source>Initial conditions</source>
        <translation>Conditions initiales</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="643"/>
        <source>Edit friction</source>
        <translation>Éditer les frottements</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="631"/>
        <source>Edit lateral sources</source>
        <translation>Éditer les contributions latérales</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="577"/>
        <source>Run solver</source>
        <translation>Lancer le solveur</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="519"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../View/ui/WebView.ui" line="53"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="527"/>
        <source>Visualize last results</source>
        <translation>Visualisation des derniers résultats</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="530"/>
        <source>Visualize the last results</source>
        <translation>Visualisation des derniers résultats</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="538"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="547"/>
        <source>Save current study</source>
        <translation>Sauvegarder l&apos;étude</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="550"/>
        <source>Save the study (Ctrl+S)</source>
        <translation>Sauvegarde de l&apos;étude (Ctrl+S)</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="565"/>
        <source>Close the study (Ctrl+F)</source>
        <translation>Fermeture de l&apos;étude (Ctrl+F)</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="568"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="580"/>
        <source>Run a solver</source>
        <translation>Lancer un solveur</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="589"/>
        <source>River network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="601"/>
        <source>Geometry</source>
        <translation>Géometrie</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="604"/>
        <source>Edit reach geometry</source>
        <translation>Éditer la géométrie du bief actuel</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="613"/>
        <source>Boundary conditions</source>
        <translation>Conditions aux limites</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="616"/>
        <source>Edit boundary conditions and point sources</source>
        <translation>Éditer les conditions aux limites et les apports ponctuels</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="628"/>
        <source>Lateral sources</source>
        <translation>Contributions latérales</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="640"/>
        <source>Friction</source>
        <translation>Frottements</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="652"/>
        <source>Edit study</source>
        <translation>Éditer l&apos;étude</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="667"/>
        <source>Define initial conditions</source>
        <translation>Définir les conditions initiales</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="672"/>
        <source>Sediment layers</source>
        <translation>Couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="675"/>
        <source>Define sediment layers</source>
        <translation>Définition des couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="680"/>
        <source>Edit reach sediment layers</source>
        <translation>Éditer les couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="685"/>
        <source>Mage</source>
        <translation>Mage</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="688"/>
        <source>Open Mage documentation</source>
        <translation>Ouvrir la documentation de Mage</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="693"/>
        <source>Users (wiki)</source>
        <translation>Utilisateurs (wiki)</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="698"/>
        <source>Developers (pdf)</source>
        <translation>Développeurs (pdf)</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="703"/>
        <source>Developers (html)</source>
        <translation>Développeurs (html)</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="708"/>
        <source>Reservoirs</source>
        <translation>Casiers</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="711"/>
        <source>Edit reservoirs</source>
        <translation>Éditer les casiers</translation>
    </message>
    <message>
        <location filename="../View/ui/OutputRKAdisTS.ui" line="14"/>
        <source>Hydraulic structures</source>
        <translation>Ouvrages hydrauliques</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="719"/>
        <source>Edit hydraulic structures</source>
        <translation>Éditer les ouvrages hydrauliques</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="727"/>
        <source>Open results from file</source>
        <translation>Ouvrir des résultats depuis un fichier</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLogFile.ui" line="65"/>
        <source>Revert</source>
        <translation>Renverser</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLogFile.ui" line="74"/>
        <source>Open in editor</source>
        <translation>Ouvrir dans l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="../View/ui/DebugRepl.ui" line="26"/>
        <source>Evaluate</source>
        <translation>Évaluer</translation>
    </message>
    <message>
        <location filename="../View/ui/DebugRepl.ui" line="39"/>
        <source>Ctrl+Return</source>
        <translation>Ctrl+Return</translation>
    </message>
    <message>
        <location filename="../View/ui/EditSedimentLayers.ui" line="66"/>
        <source>Add a new sediment layer</source>
        <translation>Ajouter une nouvelle couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../View/ui/EditSedimentLayers.ui" line="81"/>
        <source>Delete selected sediment layer(s)</source>
        <translation>Supprimer les couches sédimentaires sélectionnées</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributionsAdisTS.ui" line="103"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../View/ui/EditSedimentLayers.ui" line="96"/>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../View/ui/EditSedimentLayers.ui" line="108"/>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditions.ui" line="44"/>
        <source>Liquid</source>
        <translation>Liquide</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditions.ui" line="54"/>
        <source>Solid</source>
        <translation>Solide</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributions.ui" line="64"/>
        <source>Suspension</source>
        <translation>Suspension</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributions.ui" line="117"/>
        <source>Add a new boundary condition or lateral source</source>
        <translation>Ajouter une condition aux limites ou un apport latéral</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributionsAdisTS.ui" line="100"/>
        <source>Delete current selected rows</source>
        <translation>Supprimer les lignes selectionnées</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributions.ui" line="147"/>
        <source>Edit boundary condition or lateral source</source>
        <translation>Éditer une condition aux limites ou un apport latéral</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="94"/>
        <source>Sort</source>
        <translation>Trier</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditions.ui" line="152"/>
        <source>Sort by names</source>
        <translation>Trier par nom</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="28"/>
        <source>Strickler coefficients of the study</source>
        <translation>Coefficients de Strickler de l&apos;étude</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="38"/>
        <source>Strickler coefficients of the application</source>
        <translation>Coefficients de Strickler de l&apos;application</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="73"/>
        <source>Add new Strickler coefficients</source>
        <translation>Ajouter un coefficient de Strickler</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="85"/>
        <source>Delete selected Strickler coefficients</source>
        <translation>Supprimer les coefficients de Strickler selectionnés</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="97"/>
        <source>Sort Strickler coefficients</source>
        <translation>Trier les coefficients de Strickler</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditions.ui" line="107"/>
        <source>Add a new boundary condition or point source</source>
        <translation>Ajouter une condition aux limites ou un apport ponctuel</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditions.ui" line="137"/>
        <source>Edit boundary condition or point source</source>
        <translation>Éditer une condition aux limites ou un apport ponctuel</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="137"/>
        <source>Raw data</source>
        <translation>Données brutes</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="166"/>
        <source>Water elevation</source>
        <translation>Cote de l&apos;eau</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="198"/>
        <source>Discharge time series</source>
        <translation>Hydrogramme</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="251"/>
        <source>Add customized visualization</source>
        <translation>Ajouter une visualisation personnalisée</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="260"/>
        <source>Reload</source>
        <translation>Recharger</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="269"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="82"/>
        <source>delete</source>
        <translation>supprimer</translation>
    </message>
    <message>
        <location filename="../View/ui/Frictions.ui" line="104"/>
        <source>Edit Strickler coefficients</source>
        <translation>Éditer les coefficients de Strickler</translation>
    </message>
    <message>
        <location filename="../View/ui/WebView.ui" line="56"/>
        <source>Open document in Firefox</source>
        <translation>Ouvrir le document dans Firefox</translation>
    </message>
    <message>
        <location filename="../View/ui/WebView.ui" line="65"/>
        <source>back</source>
        <translation>arrière</translation>
    </message>
    <message>
        <location filename="../View/ui/WebView.ui" line="74"/>
        <source>forward</source>
        <translation>avant</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="39"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../View/ui/CheckList.ui" line="85"/>
        <source>Retry</source>
        <translation>Réessayer</translation>
    </message>
    <message>
        <location filename="../View/ui/CheckList.ui" line="66"/>
        <source>Run</source>
        <translation>Lancer</translation>
    </message>
    <message>
        <location filename="../View/ui/CheckList.ui" line="88"/>
        <source>Retry check</source>
        <translation>Réessayer les vérifications</translation>
    </message>
    <message>
        <location filename="../View/ui/BasicHydraulicStructures.ui" line="14"/>
        <source>Basic hydraulic structures</source>
        <translation>Ouvrages hydrauliques élémentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/BasicHydraulicStructures.ui" line="49"/>
        <source>Enable / Disable basic hydraulic structure</source>
        <translation>Activer/Désactiver l&apos;ouvrage hydraulique élémentaire</translation>
    </message>
    <message>
        <location filename="../View/ui/Reservoir.ui" line="78"/>
        <source>Add a new point</source>
        <translation>Ajouter un nouveau point</translation>
    </message>
    <message>
        <location filename="../View/ui/Reservoir.ui" line="90"/>
        <source>Delete points</source>
        <translation>Supprimer les points</translation>
    </message>
    <message>
        <location filename="../View/ui/BasicHydraulicStructures.ui" line="136"/>
        <source>Edit selected hydraulic structure</source>
        <translation>Éditer l&apos;ouvrage hydraulique sélectionné</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLog.ui" line="78"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLog.ui" line="87"/>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLog.ui" line="96"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLog.ui" line="105"/>
        <source>LogFile</source>
        <translation>Fichier de log</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLog.ui" line="110"/>
        <source>results</source>
        <translation>résultats</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="73"/>
        <source>Add a point to cross-section</source>
        <translation>Ajouter un point à la section en travers</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="85"/>
        <source>Delete selected point(s)</source>
        <translation>Supprimer les points sélectionnés</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="94"/>
        <source>Up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="97"/>
        <source>Move up selected point(s)</source>
        <translation>Déplacer les points sélectionnés vers le haut</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="106"/>
        <source>Down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="109"/>
        <source>Move down selected point(s)</source>
        <translation>Déplacer les points sélectionnés vers le bas</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="118"/>
        <source>sort_asc</source>
        <translation>sort_asc</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="121"/>
        <source>Sort points by nearest neighbor</source>
        <translation>Trier les points par leurs plus proches voisins</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="130"/>
        <source>sort_des</source>
        <translation>sort_des</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="133"/>
        <source>Sort reversed points by nearest neighbor</source>
        <translation>Trier/inverser les points par leurs plus proches voisins</translation>
    </message>
    <message>
        <location filename="../View/ui/HydraulicStructures.ui" line="49"/>
        <source>Enable / Disable hydraulic structure</source>
        <translation>Activer/Désactiver l&apos;ouvrage hydraulique</translation>
    </message>
    <message>
        <location filename="../View/ui/Reservoir.ui" line="102"/>
        <source>Edit stage-area relation</source>
        <translation>Éditer loi cote/surface</translation>
    </message>
    <message>
        <location filename="../View/ui/Reservoir.ui" line="114"/>
        <source>Sort points by elevations</source>
        <translation>Trier les points par cote</translation>
    </message>
    <message>
        <location filename="../View/ui/ReachSedimentLayers.ui" line="31"/>
        <source>Edit sediment layers list</source>
        <translation>Éditer la liste des couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/ReachSedimentLayers.ui" line="38"/>
        <source>Apply sediment layers to all reaches</source>
        <translation>Appliquer une liste de couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/ReachSedimentLayers.ui" line="80"/>
        <source>Edit profile</source>
        <translation>Éditer le profil</translation>
    </message>
    <message>
        <location filename="../View/ui/ReachSedimentLayers.ui" line="83"/>
        <source>Edit sediment layers of the profile</source>
        <translation>Éditer les couches sédimentaires du profil</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="105"/>
        <source>Add new initial condition</source>
        <translation>Ajouter une nouvelle condition initiale</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="117"/>
        <source>Delete inital condition</source>
        <translation>Supprimer une condition initiale</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="126"/>
        <source>sort</source>
        <translation>sort</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="129"/>
        <source>Sort inital conditions</source>
        <translation>Trier les conditions initiales</translation>
    </message>
    <message>
        <location filename="../View/ui/EditLateralContribution.ui" line="82"/>
        <source>Add a new point in boundary condition or lateral source</source>
        <translation>Ajouter un nouveau point</translation>
    </message>
    <message>
        <location filename="../View/ui/ProfileSedimentLayers.ui" line="48"/>
        <source>Add sediment layers</source>
        <translation>Ajouter une couche sédimentaire</translation>
    </message>
    <message>
        <location filename="../View/ui/ProfileSedimentLayers.ui" line="51"/>
        <source>Add specific sediment layers on selected point(s)</source>
        <translation>Ajouter des couches spécifiques aux points sélectionnés</translation>
    </message>
    <message>
        <location filename="../View/ui/ProfileSedimentLayers.ui" line="60"/>
        <source>Delete sediment layers</source>
        <translation>Supprimer les couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/ProfileSedimentLayers.ui" line="63"/>
        <source>Delete specific sediment layers of selected point(s)</source>
        <translation>Supprimer les couches sédimentaires aux points sélectionnés</translation>
    </message>
    <message>
        <location filename="../View/ui/ProfileSedimentLayers.ui" line="72"/>
        <source>Edit sediment layers</source>
        <translation>Éditer les couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="138"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="130"/>
        <source>Add a cross-section</source>
        <translation>Ajouter une section en travers</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="142"/>
        <source>Delete selected cross-section(s)</source>
        <translation>Supprimer les sections en travers sélectionnées</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="154"/>
        <source>Edit selected cross section(s)</source>
        <translation>Éditer les sections en travers sélectionnées</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="166"/>
        <source>Sort cross-sections by ascending position</source>
        <translation>Trier les sections en travers par PK croissant</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="178"/>
        <source>Sort cross-sections by descending position</source>
        <translation>Trier les sections en travers par PK décroissant</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="190"/>
        <source>Move up selected cross-section(s)</source>
        <translation>Déplacer les sections en travers vers le haut</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="202"/>
        <source>Move down selected cross-section(s)</source>
        <translation>Déplacer les sections en travers vers le bas</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="211"/>
        <source>Meshing</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="153"/>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="156"/>
        <source>Checks</source>
        <translation>Vérifications</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="207"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Avancé</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="741"/>
        <source>&amp;Additional files</source>
        <translation>Fichiers &amp;supplémentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="746"/>
        <source>REP additional lines</source>
        <translation>Lignes REP supplémentaires</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineList.ui" line="46"/>
        <source>Add new line</source>
        <translation>Ajouter une nouvelle ligne</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineList.ui" line="61"/>
        <source>Delete additional line(s)</source>
        <translation>Supprimer les lignes supplementaires</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineList.ui" line="64"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../View/ui/REPLineList.ui" line="76"/>
        <source>Edit selected line(s)</source>
        <translation>Éditer les lignes selectionnées</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="46"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="55"/>
        <source>File text</source>
        <translation>Texte du fichier</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="67"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="73"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="83"/>
        <source>The relative file path on executable directory</source>
        <translation>Le chemin relatif du fichier dans le répertoire d&apos;exécution</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="93"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFile.ui" line="100"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFileList.ui" line="46"/>
        <source>Add a new file</source>
        <translation>Ajouter un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFileList.ui" line="58"/>
        <source>Delete selected file(s)</source>
        <translation>Supprimer les fichiers sélectionnés</translation>
    </message>
    <message>
        <location filename="../View/ui/AdditionalFileList.ui" line="70"/>
        <source>Edit file</source>
        <translation>Éditer fichier</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="655"/>
        <source>Edit the study information</source>
        <translation>Éditer les informations de l&apos;étude</translation>
    </message>
    <message>
        <location filename="../View/ui/Stricklers.ui" line="52"/>
        <source>toolBar</source>
        <translation>toolBar</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="319"/>
        <source>toolBar_2</source>
        <translation>toolBar_2</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="498"/>
        <source>Edit frictions</source>
        <translation>Éditer les frottements</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="138"/>
        <source>Purge</source>
        <translation>Purger</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="141"/>
        <source>Purge the cross-section to keep a given number of points</source>
        <translation>Purger les profiles pour garder qu&apos;un nombre donné de points</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="146"/>
        <source>Reverse</source>
        <translation>Retourner</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryCrossSection.ui" line="149"/>
        <source>Reverse the points order</source>
        <translation>Retourner l&apos;ordre des points</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="141"/>
        <source>Import from file</source>
        <translation>Importer depuis un fichier</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="216"/>
        <source>Update RK</source>
        <translation>Mise à jour des PK</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="219"/>
        <source>Recompute RK</source>
        <translation>Recalcule des PK</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="227"/>
        <source>Purge cross-sections to keep a given number of points</source>
        <translation>Purger les profiles pour garder un nombre fixer de points</translation>
    </message>
    <message>
        <location filename="../View/ui/EditLateralContribution.ui" line="112"/>
        <source>Sort points</source>
        <translation>Trier les points</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="272"/>
        <source>Export data as CSV</source>
        <translation>Exporter les données au format CSV</translation>
    </message>
    <message>
        <location filename="../View/ui/EditBoundaryConditions.ui" line="130"/>
        <source>Generate uniform</source>
        <translation>Générer un regime uniforme</translation>
    </message>
    <message>
        <location filename="../View/ui/EditBoundaryConditions.ui" line="133"/>
        <source>Generate rating curve from Manning law</source>
        <translation>Générer une courbe de tarage (loi de Maning)</translation>
    </message>
    <message>
        <location filename="../View/ui/EditBoundaryConditions.ui" line="138"/>
        <source>Generate critical</source>
        <translation>Générer régime critique</translation>
    </message>
    <message>
        <location filename="../View/ui/EditBoundaryConditions.ui" line="141"/>
        <source>Generate rating curve as Q(z) = Sqrt(g*S(z)^3/L(z))</source>
        <translation>Générer une courbe de tarage (Q(z) = Sqrt(g*S(z)^3/L(z)))</translation>
    </message>
    <message>
        <location filename="../View/ui/EditBoundaryConditions.ui" line="146"/>
        <source>Make increasing</source>
        <translation>Augmenter</translation>
    </message>
    <message>
        <location filename="../View/ui/EditBoundaryConditions.ui" line="149"/>
        <source>Remove points to make the curve increasing</source>
        <translation>Supprimer des points pour rendre la courbe croissante</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="232"/>
        <source>Shift</source>
        <translation>Translater</translation>
    </message>
    <message>
        <location filename="../View/ui/GeometryReach.ui" line="235"/>
        <source>Shift selected sections coordinates</source>
        <translation>Translater les coordonnées des sections sélectionnées</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="30"/>
        <source>Generate uniform depth</source>
        <translation>Générer une profondeur uniforme</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="37"/>
        <source>Generate uniform discharge</source>
        <translation>Générer un débit uniforme</translation>
    </message>
    <message>
        <location filename="../View/ui/InitialConditions.ui" line="44"/>
        <source>Generate uniform elevation</source>
        <translation>Générer une cote uniforme</translation>
    </message>
    <message>
        <location filename="../View/ui/Results.ui" line="149"/>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="174"/>
        <source>Please select a reach</source>
        <translation>Veuillez sélectionner un bief</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="183"/>
        <source>Last open study</source>
        <translation>Dernière étude ouverte</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="186"/>
        <source>Do you want to open again the last open study?</source>
        <translation>Voulez-vous rouvrir la dernière étude ?</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="177"/>
        <source>This edition window need a reach selected into the river network to work on it</source>
        <translation>Cette fenêtre d&apos;édition a besoin d&apos;un bief sélectionné dans le réseau pour travailler dessus</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="191"/>
        <source>Close without saving study</source>
        <translation>Fermer sans sauvegarder l&apos;étude</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="194"/>
        <source>Do you want to save current study before closing it?</source>
        <translation>Souhaitez-vous sauvegarder l&apos;étude en cours avant de la fermer ?</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="171"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="198"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="199"/>
        <source>Y (m)</source>
        <translation>Y (m)</translation>
    </message>
    <message>
        <location filename="../View/ui/Pollutants.ui" line="49"/>
        <source>Enable / Disable Pollutant</source>
        <translation>Activer / Désactiver ce polluant</translation>
    </message>
    <message>
        <location filename="../View/ui/Pollutants.ui" line="129"/>
        <source>Characteristics</source>
        <translation>Caracteristiques</translation>
    </message>
    <message>
        <location filename="../View/ui/Pollutants.ui" line="137"/>
        <source>InitialConditions</source>
        <translation>Conditions Initiales</translation>
    </message>
    <message>
        <location filename="../View/ui/Pollutants.ui" line="142"/>
        <source>BoundaryConditions</source>
        <translation>Conditions Limites</translation>
    </message>
    <message>
        <location filename="../View/ui/Pollutants.ui" line="147"/>
        <source>LateralContributions</source>
        <translation>Apports Latéraux</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="220"/>
        <source>AdisTS</source>
        <translation>AdisTS</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="751"/>
        <source>Output RK</source>
        <translation>Pk de sortie</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="760"/>
        <source>Run AdisTS</source>
        <translation>Lancer AdisTS</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="765"/>
        <source>Pollutants</source>
        <translation>Polluants</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="770"/>
        <source>D90</source>
        <translation>D90</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="775"/>
        <source>DIF</source>
        <translation>DIF</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="788"/>
        <source>Open results AdisTS</source>
        <translation>Ouvrir des résultats AdisTS</translation>
    </message>
    <message>
        <location filename="../View/ui/OutputRKAdisTS.ui" line="49"/>
        <source>Enable / Disable Output RK AdisTS</source>
        <translation>Activer / Désactiver la sortie AdisTS</translation>
    </message>
    <message>
        <location filename="../View/ui/EditBoundaryConditionsAdisTS.ui" line="88"/>
        <source>Add a new point in boundary condition or punctual contribution</source>
        <translation>Ajouter un nouveau point dans la condition limite</translation>
    </message>
    <message>
        <location filename="../View/ui/SolverLogAdisTS.ui" line="116"/>
        <source>results_Mage</source>
        <translation>Résultats Mage</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditionsAdisTS.ui" line="89"/>
        <source>Add a new boundary condition or punctual contribution</source>
        <translation>Ajouter une condition limite ou apport ponctuel</translation>
    </message>
    <message>
        <location filename="../View/ui/BoundaryConditionsAdisTS.ui" line="119"/>
        <source>Edit boundary condition or punctual contribution</source>
        <translation>Éditer une condition limite ou un apport ponctuel</translation>
    </message>
    <message>
        <location filename="../View/ui/EditLateralContributionAdisTS.ui" line="81"/>
        <source>Add a new point in boundary condition or lateral contribution</source>
        <translation>Ajouter un point dans la condition limite ou l&apos;apport latéral</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributionsAdisTS.ui" line="85"/>
        <source>Add a new boundary condition or lateral contribution</source>
        <translation>Ajouter une condition limite ou un apport latéral</translation>
    </message>
    <message>
        <location filename="../View/ui/LateralContributionsAdisTS.ui" line="115"/>
        <source>Edit boundary condition or lateral contribution</source>
        <translation>Éditer une condition limite ou un apport latéral</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="157"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="169"/>
        <source>Concentration dx</source>
        <translation>Concentration dx</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="179"/>
        <source>Concentration dt</source>
        <translation>Concentration dt</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="198"/>
        <source>Linear mass density</source>
        <translation>Masse linéique</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="305"/>
        <source>Left bed</source>
        <translation>Lit majeur gauche</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="315"/>
        <source>Minor bed</source>
        <translation>Lit mineur</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="328"/>
        <source>Right bed</source>
        <translation>Lit majeur droit</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="269"/>
        <source>Linear mass density dx</source>
        <translation>Masse linéique dx</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="279"/>
        <source>Linear mass density dt</source>
        <translation>Masse linéique dt</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="295"/>
        <source>Thickness</source>
        <translation>Épaisseur</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="357"/>
        <source>Thickness dx</source>
        <translation>Épaisseur dx</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="367"/>
        <source>Thickness dt</source>
        <translation>Épaisseur dt</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="426"/>
        <source>Add custom visualization</source>
        <translation>Ajouter une courbe personnalisée</translation>
    </message>
    <message>
        <location filename="../View/ui/ResultsAdisTS.ui" line="447"/>
        <source>Export raw data</source>
        <translation>Export données brutes</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="200"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="201"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../View/ui/MainWindow.ui" line="780"/>
        <source>Compare results</source>
        <translation>Comparaison de résultats</translation>
    </message>
</context>
<context>
    <name>MainWindow_reach</name>
    <message>
        <location filename="../View/Results/PlotSedReach.py" line="233"/>
        <source>Position (m)</source>
        <translation>Pk (m)</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedProfile.py" line="153"/>
        <source>Height (m)</source>
        <translation>Cote (m)</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedProfile.py" line="149"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="225"/>
        <source>Select destination file</source>
        <translation>Sélectionner fichier de destination</translation>
    </message>
</context>
<context>
    <name>Network</name>
    <message>
        <location filename="../View/Network/translate.py" line="30"/>
        <source>River network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="37"/>
        <source>Add node</source>
        <translation>Ajouter un nœud</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="38"/>
        <source>Delete the node</source>
        <translation>Supprimer le nœud</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="39"/>
        <source>Edit node reservoir</source>
        <translation>Éditer le casier du nœud</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="42"/>
        <source>Add node reservoir</source>
        <translation>Ajouter un casier au nœud</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="49"/>
        <source>Delete the reach</source>
        <translation>Supprimer l&apos;arrête</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="50"/>
        <source>Enable the reach</source>
        <translation>Activer l&apos;arête</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="51"/>
        <source>Disable the reach</source>
        <translation>Désactiver l&apos;arête</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="53"/>
        <source>Reverse the reach orientation</source>
        <translation>Inverser l&apos;orientation de l&apos;arête</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="65"/>
        <source>Source node</source>
        <translation>Nœud source</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="66"/>
        <source>Destination node</source>
        <translation>Nœud destination</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="45"/>
        <source>Delete node reservoir</source>
        <translation>Supprimer le casier du nœud</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="34"/>
        <source>Node</source>
        <translation>Nœud</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="35"/>
        <source>Reach</source>
        <translation>Bief</translation>
    </message>
    <message>
        <location filename="../View/Network/translate.py" line="67"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
</context>
<context>
    <name>OutputRKAdisTS</name>
    <message>
        <location filename="../View/OutputRKAdisTS/Translate.py" line="30"/>
        <source>Output RK</source>
        <translation>Pk de sortie</translation>
    </message>
    <message>
        <location filename="../View/OutputRKAdisTS/Translate.py" line="34"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
</context>
<context>
    <name>Pamhyr</name>
    <message>
        <location filename="../View/Tools/PamhyrTranslate.py" line="31"/>
        <source>Pamhyr2</source>
        <translation>Pamhyr2</translation>
    </message>
</context>
<context>
    <name>Pollutants</name>
    <message>
        <location filename="../View/Pollutants/Translate.py" line="30"/>
        <source>Pollutants</source>
        <translation>Polluants</translation>
    </message>
    <message>
        <location filename="../View/Pollutants/Translate.py" line="34"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
    <message>
        <location filename="../View/Pollutants/Edit/Translate.py" line="32"/>
        <source>Edit Pollutant</source>
        <translation>Éditer le polluant</translation>
    </message>
</context>
<context>
    <name>REPLines</name>
    <message>
        <location filename="../View/REPLines/Translate.py" line="29"/>
        <source>Mage REP lines</source>
        <translation>Lignes REP Mage</translation>
    </message>
    <message>
        <location filename="../View/REPLines/Translate.py" line="33"/>
        <source>Edit Mage REP lines</source>
        <translation>Éditer les lignes REP Mage</translation>
    </message>
</context>
<context>
    <name>Reservoir</name>
    <message>
        <location filename="../View/Reservoir/Translate.py" line="30"/>
        <source>Reservoir</source>
        <translation>Casier</translation>
    </message>
    <message>
        <location filename="../View/Reservoir/Translate.py" line="36"/>
        <source>Node</source>
        <translation>Nœud</translation>
    </message>
    <message>
        <location filename="../View/Reservoir/Edit/Translate.py" line="32"/>
        <source>Edit Reservoir</source>
        <translation>Éditer le casier</translation>
    </message>
</context>
<context>
    <name>Results</name>
    <message>
        <location filename="../View/Results/translate.py" line="43"/>
        <source>days</source>
        <translation>jours</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="42"/>
        <source>day</source>
        <translation>jour</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="47"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotXY.py" line="62"/>
        <source>Y (m)</source>
        <translation>Y (m)</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotH.py" line="59"/>
        <source>Cross-section discharge</source>
        <translation>Débit de la section en travers</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotH.py" line="60"/>
        <source>Max discharge</source>
        <translation>Débit maximum</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotH.py" line="61"/>
        <source>Current timestamp</source>
        <translation>Date actuelle</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="37"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="38"/>
        <source>Reading results</source>
        <translation>Lecture des résultats</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="147"/>
        <source>Water elevation</source>
        <translation>Cote de l&apos;eau</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="51"/>
        <source>Max water elevation</source>
        <translation>Cote maximum de l&apos;eau</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="57"/>
        <source>Reach name</source>
        <translation>Nom du bief</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="153"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="49"/>
        <source>Bottom</source>
        <translation>Fond</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="63"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="64"/>
        <source>Max Concentration</source>
        <translation>Concentration max</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="65"/>
        <source>Min Concentration</source>
        <translation>Concentration min</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="67"/>
        <source>Thickness</source>
        <translation>Épaisseur</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="68"/>
        <source>Max Thickness</source>
        <translation>Épaisseur max</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="69"/>
        <source>Min Thickness</source>
        <translation>Épaisseur min</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="72"/>
        <source>Mass</source>
        <translation>Masse</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="73"/>
        <source>Max Mass</source>
        <translation>Masse max</translation>
    </message>
    <message>
        <location filename="../View/Results/PlotSedAdis.py" line="74"/>
        <source>Min Mass</source>
        <translation>Masse min</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="61"/>
        <source>Variables names</source>
        <translation>Noms des variables</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="74"/>
        <source>Pollutant name</source>
        <translation>Nom des polluants</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="45"/>
        <source>envelop</source>
        <translation>enveloppe</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="65"/>
        <source>Profile name</source>
        <translation>Nom du profil</translation>
    </message>
    <message>
        <location filename="../View/Results/translate.py" line="70"/>
        <source>Solver</source>
        <translation>Solveur</translation>
    </message>
</context>
<context>
    <name>SedimentLayers</name>
    <message>
        <location filename="../View/SedimentLayers/Reach/Profile/Window.py" line="88"/>
        <source>(no name)</source>
        <translation>(sans nom)</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Edit/translate.py" line="30"/>
        <source>Edit Sediment Layers</source>
        <translation>Éditer les couches sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Edit/translate.py" line="40"/>
        <source>D50</source>
        <translation>D50</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Edit/translate.py" line="41"/>
        <source>Sigma</source>
        <translation>Sigma</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Edit/translate.py" line="42"/>
        <source>Critical constraint</source>
        <translation>Contrainte critique</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/Profile/Window.py" line="92"/>
        <source>(no name - @rk)</source>
        <translation>(sans nom - @rk)</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/Profile/translate.py" line="40"/>
        <source>X (m)</source>
        <translation>X (m)</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/Profile/translate.py" line="35"/>
        <source>Profile sediment layers</source>
        <translation>Couches sédimentaires de la section en travers</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/Profile/translate.py" line="41"/>
        <source>Y (m)</source>
        <translation>Y (m)</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/Profile/translate.py" line="42"/>
        <source>Z (m)</source>
        <translation>Z (m)</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/translate.py" line="43"/>
        <source>Sediment layers</source>
        <translation>Couche sédimentaires</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/translate.py" line="30"/>
        <source>Reach sediment layers</source>
        <translation>Couches sédimentaires du bief</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/Reach/translate.py" line="33"/>
        <source>Apply sediment layers to reach</source>
        <translation>Appliquer des couches sédimentaires au bief</translation>
    </message>
    <message>
        <location filename="../View/SedimentLayers/translate.py" line="30"/>
        <source>Sediment Layers List</source>
        <translation>Liste des ensembles de couches sédimentaires</translation>
    </message>
</context>
<context>
    <name>Solver</name>
    <message>
        <location filename="../View/RunSolver/WindowAdisTS.py" line="68"/>
        <source>Select solver</source>
        <translation>Sélection du solveur</translation>
    </message>
    <message>
        <location filename="../View/RunSolver/WindowAdisTS.py" line="176"/>
        <source>Select log</source>
        <translation>Selection du log</translation>
    </message>
    <message>
        <location filename="../View/RunSolver/Log/Window.py" line="58"/>
        <source>Solver logs</source>
        <translation>Sélection des logs</translation>
    </message>
    <message>
        <location filename="../View/RunSolver/Window.py" line="150"/>
        <source>Compare solvers</source>
        <translation>Comparaison de solveurs</translation>
    </message>
</context>
<context>
    <name>SolverParameters</name>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="30"/>
        <source>Solver parameters</source>
        <translation>Paramètres du solveur</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="47"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="48"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="49"/>
        <source>Y</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="50"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="55"/>
        <source>Initial time (jj:hh:mm:ss)</source>
        <translation>Temps initial (jj:hh:mm:ss)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="58"/>
        <source>Final time (jj:hh:mm:ss)</source>
        <translation>Temps final (jj:hh:mm:ss)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="61"/>
        <source>Timestep (second)</source>
        <translation>Pas de temps (en secondes)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="64"/>
        <source>Command line arguments</source>
        <translation>Arguments en ligne de commande</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="68"/>
        <source>Minimum timestep (second)</source>
        <translation>Pas de temps minimal (secondes)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="71"/>
        <source>Time step of writing on .TRA</source>
        <translation>Pas de temps d&apos;écriture dans le fichier .TRA</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="74"/>
        <source>Time step of writing on .BIN</source>
        <translation>Pas de temps d&apos;écriture dans le fichier .BIN</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="77"/>
        <source>Implicitation parameter</source>
        <translation>Paramètre d&apos;implicitation</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="80"/>
        <source>Continuity discretization type (S/L)</source>
        <translation>Type de discrétisation de la continuité (S/L)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="83"/>
        <source>QSJ discretization (A/B)</source>
        <translation>QSJ discrétisation (A/B)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="86"/>
        <source>Stop criterion iterations (G/A/R)</source>
        <translation>Critère d&apos;arrêt d&apos;itérations (G/A/R)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="89"/>
        <source>Iteration type</source>
        <translation>Type d&apos;itérations</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="92"/>
        <source>Smoothing coefficient</source>
        <translation>Coefficient de lissage</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="95"/>
        <source>Maximun accepted number of CFL</source>
        <translation>Nombre maximum de CFL accepté</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="98"/>
        <source>Minimum water height (meter)</source>
        <translation>Profondeur minimale (mètres)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="101"/>
        <source>Maximum number of iterations (&lt; 100)</source>
        <translation>Nombre maximum d&apos;itérations (&lt; 100)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="104"/>
        <source>Timestep reduction factor</source>
        <translation>Facteur de réduction du pas de temps</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="107"/>
        <source>Reduction precision factor of Z</source>
        <translation>Facteur de réduction de la précision en Z</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="110"/>
        <source>Reduction precision factor of Q</source>
        <translation>Facteur de réduction de la précision en Q</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="113"/>
        <source>Reduction precision factor of residue</source>
        <translation>Facteur de réduction de la précision des résidus</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="116"/>
        <source>Number of iterations at maximum precision</source>
        <translation>Nombre d&apos;itérations à la précision maximum</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="119"/>
        <source>Number of iterations before switch</source>
        <translation>Nombre d&apos;itérations avant changement</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="122"/>
        <source>Maximum accepted Froude number</source>
        <translation>Nombre de Froude maximum accepté</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="125"/>
        <source>Diffluence node height balance</source>
        <translation>Équilibre de la hauteur des nœuds de diffluence</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="128"/>
        <source>Compute reach volume balance (Y/N)</source>
        <translation>Calcul de l&apos;équilibre du volume de bief (O/N)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="131"/>
        <source>Maximum reach volume balance</source>
        <translation>Équilibre du volume de bief maximum</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="134"/>
        <source>Minimum reach volume to check</source>
        <translation>Volume de bief minimum à vérifier</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="137"/>
        <source>Use Mage internal initialization (Y/N)</source>
        <translation>Utiliser l&apos;initialisation interne de Mage (O/N)</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="141"/>
        <source>Sediment density</source>
        <translation>Masse volumique des sédiments</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="143"/>
        <source>Sediment rest angle</source>
        <translation>Angle de repos des sédiments</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="145"/>
        <source>Sediment porosity</source>
        <translation>Porosité des sédiments</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="147"/>
        <source>Han distance</source>
        <translation>Distance de Han</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="149"/>
        <source>D50 changing distance</source>
        <translation>D50 changing distance</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="151"/>
        <source>Sigma changing distance</source>
        <translation>Distance de changement du Sigma</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="153"/>
        <source>Geometry modification method</source>
        <translation>Méthode de modification de la géometrie</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="155"/>
        <source>Critical Shields number</source>
        <translation>Nombre de Shields critique</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="157"/>
        <source>Shields correction</source>
        <translation>Correction de Shields</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="159"/>
        <source>Solid capacity</source>
        <translation>Capacité solide</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="161"/>
        <source>Bed load time step</source>
        <translation>Pas de temps charriage</translation>
    </message>
    <message>
        <location filename="../View/SolverParameters/translate.py" line="163"/>
        <source>Multiplicator factor</source>
        <translation>Facteur de multiplication</translation>
    </message>
</context>
<context>
    <name>Stricklers</name>
    <message>
        <location filename="../View/Stricklers/translate.py" line="30"/>
        <source>Strickler coefficients</source>
        <translation>Coefficients de Stricklers</translation>
    </message>
</context>
<context>
    <name>Study</name>
    <message>
        <location filename="../View/Study/Window.py" line="51"/>
        <source>Edit study</source>
        <translation>Éditer l&apos;étude</translation>
    </message>
    <message>
        <location filename="../View/Study/Window.py" line="56"/>
        <source>New study</source>
        <translation>Nouvelle étude</translation>
    </message>
</context>
<context>
    <name>Toolbar</name>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="252"/>
        <source>Default view</source>
        <translation>Vue par défaut</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="253"/>
        <source>Back to previous view</source>
        <translation>Retour à la vue précédente</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="255"/>
        <source>Back to next view</source>
        <translation>Retour à la vue suivante</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="257"/>
        <source>Panoramic axes</source>
        <translation>Axes panoramiques</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="259"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="262"/>
        <source>Isometric view (Shift+W)</source>
        <translation>Vue isométrique (Shift+W)</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="264"/>
        <source>Auto scale view (Shift+X)</source>
        <translation>Vue automatique (Shift+X)</translation>
    </message>
    <message>
        <location filename="../View/Tools/Plot/PamhyrToolbar.py" line="260"/>
        <source>Save figure</source>
        <translation>Sauvegarder la figure</translation>
    </message>
</context>
<context>
    <name>Unit</name>
    <message>
        <location filename="../View/Translate.py" line="59"/>
        <source>Width (m)</source>
        <translation>Largeur (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="65"/>
        <source>Depth (m)</source>
        <translation>Profondeur (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="72"/>
        <source>Diameter (m)</source>
        <translation>Diamètre (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="73"/>
        <source>Thickness (m)</source>
        <translation>Épaisseur (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="74"/>
        <source>Elevation (m)</source>
        <translation>Cote (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="116"/>
        <source>Area (hectare)</source>
        <translation>Aire (hectare)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="118"/>
        <source>Time (sec)</source>
        <translation>Temps (s)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="119"/>
        <source>Time (JJJ:HH:MM:SS)</source>
        <translation>Temps (JJJ:HH:MM:SS)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="121"/>
        <source>Date (sec)</source>
        <translation>Date (s)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="122"/>
        <source>Date (ISO format)</source>
        <translation>Date (format ISO)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="58"/>
        <source>River Kilometer (m)</source>
        <translation>Point Kilométrique (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="66"/>
        <source>Max Depth (m)</source>
        <translation>Profondeur max (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="71"/>
        <source>Mean Depth (m)</source>
        <translation>Profondeur moyenne (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="99"/>
        <source>Velocity (m/s)</source>
        <translation>Vitesse (m/s)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="137"/>
        <source>Wet Perimeter (m)</source>
        <translation>Périmètre mouillé (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="140"/>
        <source>Hydraulic Radius (m)</source>
        <translation>Rayon hydraulique (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="143"/>
        <source>Froude number</source>
        <translation>Nombre de Froude</translation>
    </message>
    <message>
        <location filename="../View/InitialConditionsAdisTS/translate.py" line="49"/>
        <source>EG (m)</source>
        <translation>Épaisseur lit
moyen gauche (m)</translation>
    </message>
    <message>
        <location filename="../View/InitialConditionsAdisTS/translate.py" line="50"/>
        <source>EM (m)</source>
        <translation>Épaisseur lit
mineur (m)</translation>
    </message>
    <message>
        <location filename="../View/InitialConditionsAdisTS/translate.py" line="51"/>
        <source>ED (m)</source>
        <translation>Épaisseur lit
moyen droit (m)</translation>
    </message>
    <message>
        <location filename="../View/DIFAdisTS/translate.py" line="45"/>
        <source>Start_RK (m)</source>
        <translation>Pk début</translation>
    </message>
    <message>
        <location filename="../View/DIFAdisTS/translate.py" line="46"/>
        <source>End_RK (m)</source>
        <translation>Pk fin</translation>
    </message>
    <message>
        <location filename="../View/InitialConditionsAdisTS/translate.py" line="52"/>
        <source>Rate</source>
        <translation>Taux</translation>
    </message>
    <message>
        <location filename="../View/D90AdisTS/translate.py" line="45"/>
        <source>D90</source>
        <translation>D90</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="60"/>
        <source>Width Envelop (m)</source>
        <translation>Enveloppe de la argeur (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="63"/>
        <source>Max Width (m)</source>
        <translation>Largeur max (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="64"/>
        <source>Min Width (m)</source>
        <translation>Largeur min (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="67"/>
        <source>Min Depth (m)</source>
        <translation>Profondeur min (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="68"/>
        <source>Depth Envelop (m)</source>
        <translation>Enveloppe de la profondeur (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="75"/>
        <source>Bed Elevation (m)</source>
        <translation>Cote du fond (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="78"/>
        <source>Bed Elevation Envelop (m)</source>
        <translation>Enveloppe de la cote du fond (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="81"/>
        <source>Max Bed Elevation (m)</source>
        <translation>Cote du fond max (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="84"/>
        <source>Min Bed Elevation (m)</source>
        <translation>Cote du fond min (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="87"/>
        <source>Water Elevation (m)</source>
        <translation>Cote de l&apos;eau (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="90"/>
        <source>Water Elevation Envelop (m)</source>
        <translation>Enveloppe de la cote de l&apos;eau (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="93"/>
        <source>Max Water Elevation (m)</source>
        <translation>Cote de l&apos;eau max (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="96"/>
        <source>Min Water Elevation (m)</source>
        <translation>Cote de l&apos;eau min (m)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="100"/>
        <source>Velocity Envelop (m/s)</source>
        <translation>Enveloppe de la vitesse (m/s)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="103"/>
        <source>Max Velocity (m/s)</source>
        <translation>Vitesse max (m/s)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="106"/>
        <source>Min Velocity (m/s)</source>
        <translation>Vitesse min (m/s)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="124"/>
        <source>Area</source>
        <translation>Aire</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="125"/>
        <source>Rho</source>
        <translation>Rho</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="126"/>
        <source>Porosity</source>
        <translation>Porosité</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="127"/>
        <source>CDC_RIV</source>
        <translation>CDC_RIV</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="128"/>
        <source>CDC_CAS</source>
        <translation>CDC_CAS</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="129"/>
        <source>APD</source>
        <translation>APD</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="130"/>
        <source>AC</source>
        <translation>AC</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="131"/>
        <source>BC</source>
        <translation>BC</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="133"/>
        <source>Concentration (g/l)</source>
        <translation>Concentration (g/l)</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="144"/>
        <source>Mass (kg)</source>
        <translation>Masse</translation>
    </message>
    <message>
        <location filename="../View/DIFAdisTS/translate.py" line="47"/>
        <source>DIF</source>
        <translation>DIF</translation>
    </message>
    <message>
        <location filename="../View/DIFAdisTS/translate.py" line="48"/>
        <source>Coeff b</source>
        <translation>Coeff b</translation>
    </message>
    <message>
        <location filename="../View/DIFAdisTS/translate.py" line="49"/>
        <source>Coeff c</source>
        <translation>Coeff c</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="109"/>
        <source>Discharge</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="110"/>
        <source>Discharge Envelop</source>
        <translation>Enveloppe du débit</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="112"/>
        <source>Max Discharge</source>
        <translation>Débit max</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="114"/>
        <source>Min Discharge</source>
        <translation>Débit min</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="145"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../View/Translate.py" line="136"/>
        <source>Wet Area</source>
        <translation>Aire mouillée</translation>
    </message>
</context>
</TS>
