#!/usr/bin/env python3

# pamhyr.py -- Pamhyr entrypoint
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import sys
import os
import locale
import logging

from PyQt5.QtCore import QTranslator
from PyQt5.QtWidgets import QApplication

from config import Config
from tools import (
    reset_timers, display_timers, timer,
    logger_color_blue, logger_color_red, logger_color_green, logger_color_reset
)

from View.MainWindow import ApplicationWindow
from Model.Study import Study

from Scripts.P3DST import Script3DST
from Scripts.Hello import ScriptHello
from Scripts.ListSolver import ScriptListSolver
from Scripts.Run import ScriptExport, ScriptRun
from Scripts.MageMesh import MageMesh

from init import legal_info, debug_info, setup_lang

logger = logging.getLogger()

scripts = {
    "hello": ScriptHello,
    "solvers": ScriptListSolver,
    "export": ScriptExport,
    "run": ScriptRun,
    "3DST": Script3DST,
    "mesh": MageMesh,
}


def usage(argv):
    logger.info("")
    logger.info(f"Usage: {argv[0]} <script> <args...>")
    logger.info("")
    logger.info("Available scripts:")
    logger.info("\thelp\t\tDisplay this message")
    logger.info("\tgui\t\tRun Pamhyr graphics user interface (by default)")
    logger.info(
        "\tdebug\t\tRun Pamhyr graphics user interface " +
        "as debug mode (for developers)"
    )
    for s in scripts:
        logger.info(f"\t{s}\t\t{scripts[s].description}")
    logger.info("")


def gui(app: QApplication, conf: Config):
    application = ApplicationWindow(conf=conf)
    application.show()

    debug_info()

    return app.exec_()


def main():
    conf = Config.load()

    legal_info()

    if len(sys.argv) > 1:
        script = sys.argv[1]
    else:
        script = "gui"

    if script == "help":
        ret = usage(sys.argv)
    elif script == "gui" or script == "debug":
        if script == "debug":
            conf.debug = True

        app = QApplication(sys.argv)
        tr = setup_lang(app, conf)
        app.installTranslator(tr)
        ret = gui(app, conf)
    else:
        if script not in scripts:
            logger.error(
                f"{logger_color_red()}Invalid script name " +
                f"'{sys.argv[1]}'{logger_color_reset()}"
            )
            usage(sys.argv)
            sys.exit(-1)

        # By default script as no QApplication (allow run script
        # in tty mode)
        app = None

        application = scripts[script](app, conf, sys.argv)
        ret = application.run()
        if ret != 0:
            application.usage()

    display_timers()
    sys.exit(ret)


if __name__ == "__main__":
    main()
