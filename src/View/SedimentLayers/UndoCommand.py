# UndoCommand.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.SedimentLayer.SedimentLayer import SedimentLayer
from Model.SedimentLayer.SedimentLayerList import SedimentLayerList


class SetNameCommand(QUndoCommand):
    def __init__(self, sediment_layers_list, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers_list = sediment_layers_list
        self._index = index
        self._old = self._sediment_layers_list.get(self._index).name
        self._new = new_value

    def undo(self):
        self._sediment_layers_list.get(self._index).name = self._old

    def redo(self):
        self._sediment_layers_list.get(self._index).name = self._new


class SetCommentCommand(QUndoCommand):
    def __init__(self, sediment_layers_list, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers_list = sediment_layers_list
        self._index = index
        self._old = self._sediment_layers_list.get(self._index).comment
        self._new = new_value

    def undo(self):
        self._sediment_layers_list.get(self._index).comment = self._old

    def redo(self):
        self._sediment_layers_list.get(self._index).comment = self._new


class AddCommand(QUndoCommand):
    def __init__(self, sediment_layers_list, index):
        QUndoCommand.__init__(self)

        self._sediment_layers_list = sediment_layers_list
        self._index = index
        self._new = None

    def undo(self):
        self._sediment_layers_list.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._sediment_layers_list.new(self._index)
        else:
            self._sediment_layers_list.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, sediment_layers_list, rows):
        QUndoCommand.__init__(self)

        self._sediment_layers_list = sediment_layers_list
        self._rows = rows

        self._sl_pos = []
        for row in rows:
            self._sl_pos.append((row, self._sediment_layers_list.get(row)))
        self._sl_pos.sort()

    def undo(self):
        for row, el in self._sl_pos:
            self._sediment_layers_list.insert(row, el)

    def redo(self):
        self._sediment_layers_list.delete_i(self._rows)


class MoveCommand(QUndoCommand):
    def __init__(self, sediment_layers_list, up, i):
        QUndoCommand.__init__(self)

        self._sediment_layers_list = sediment_layers_list
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._sediment_layers_list.move_up(self._i)
        else:
            self._sediment_layers_list.move_down(self._i)

    def redo(self):
        if self._up:
            self._sediment_layers_list.move_up(self._i)
        else:
            self._sediment_layers_list.move_down(self._i)
