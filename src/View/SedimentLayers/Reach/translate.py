# translate.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.SedimentLayers.translate import SedimentTranslate

_translate = QCoreApplication.translate


class SedimentReachTranslate(SedimentTranslate):
    def __init__(self):
        super(SedimentReachTranslate, self).__init__()

        self._dict["Reach sediment layers"] = _translate(
            "SedimentLayers", "Reach sediment layers"
        )
        self._dict["SL"] = _translate(
            "SedimentLayers", "Apply sediment layers to reach"
        )

        self._dict["rk"] = self._dict["unit_rk"]
        self._dict["elevation"] = self._dict["unit_elevation"]

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "rk": self._dict["unit_rk"],
            "sl": _translate("SedimentLayers", "Sediment layers"),
        }
