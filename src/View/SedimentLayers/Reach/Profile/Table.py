# Table.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox,
)

from View.SedimentLayers.Reach.Profile.UndoCommand import *
from View.SedimentLayers.Reach.Profile.translate import *

from View.Tools.PamhyrTable import PamhyrTableModel

_translate = QCoreApplication.translate

logger = logging.getLogger()


class ComboBoxDelegate(QItemDelegate):
    def __init__(self, study=None, trad=None, parent=None):
        super(ComboBoxDelegate, self).__init__(parent)

        self._trad = trad
        self._study = study

    def createEditor(self, parent, option, index):
        self.editor = QComboBox(parent)

        self.editor.addItems(
            # [self._trad["not_defined"]] +
            list(
                map(
                    lambda sl: str(sl),
                    self._study.river.sediment_layers.sediment_layers
                )
            )
        )

        self.editor.setCurrentText(index.data(Qt.DisplayRole))
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(Qt.DisplayRole)
        self.editor.currentTextChanged.connect(self.currentItemChanged)

    def setModelData(self, editor, model, index):
        text = str(editor.currentText())
        model.setData(index, text)
        editor.close()
        editor.deleteLater()

    def updateEditorGeometry(self, editor, option, index):
        r = QRect(option.rect)
        if self.editor.windowFlags() & Qt.Popup:
            if editor.parent() is not None:
                r.setTopLeft(self.editor.parent().mapToGlobal(r.topLeft()))
        editor.setGeometry(r)

    @pyqtSlot()
    def currentItemChanged(self):
        self.commitData.emit(self.sender())


class TableModel(PamhyrTableModel):
    def _setup_lst(self):
        self._lst = self._data
        self._study = self._opt_data

    def data(self, index, role):
        if role != Qt.ItemDataRole.DisplayRole:
            return QVariant()

        row = index.row()
        column = index.column()

        if self._headers[column] == "name":
            return self._data.point(row).name
        elif self._headers[column] == "sl":
            value = self._data.point(row).sl
            if value is None:
                text = self._trad["not_defined"]
                return text
            return str(value)
        elif self._headers[column] == "x":
            return self._data.point(row).x
        elif self._headers[column] == "y":
            return self._data.point(row).y
        elif self._headers[column] == "z":
            return self._data.point(row).z

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or role != Qt.EditRole:
            return False

        row = index.row()
        column = index.column()

        if self._headers[column] == "sl":
            new = None
            if value != self._trad["not_defined"]:
                new = next(
                    filter(
                        lambda sl: str(sl) == value,
                        self._study.river.sediment_layers.sediment_layers
                    )
                )

            self._undo.push(
                SetSLCommand(
                    self._data, row, new
                )
            )

        self.dataChanged.emit(index, index)
        return True

    def undo(self):
        self._undo.undo()
        self.layoutChanged.emit()

    def redo(self):
        self._undo.redo()
        self.layoutChanged.emit()
