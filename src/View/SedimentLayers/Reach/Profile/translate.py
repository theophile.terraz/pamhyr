# translate.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.SedimentLayers.Reach.translate import SedimentReachTranslate

_translate = QCoreApplication.translate


class SedimentProfileTranslate(SedimentReachTranslate):
    def __init__(self):
        super(SedimentProfileTranslate, self).__init__()

        self._dict["x"] = _translate(
            "SedimentLayers", "X (m)"
        )
        self._dict["elevation"] = self._dict["unit_elevation"]

        self._dict["Profile sediment layers"] = _translate(
            "SedimentLayers", "Profile sediment layers"
        )

        self._sub_dict["table_headers"] = {
            "x": _translate("SedimentLayers", "X (m)"),
            "y": _translate("SedimentLayers", "Y (m)"),
            "z": _translate("SedimentLayers", "Z (m)"),
            "name": self._dict["name"],
            "sl": _translate("SedimentLayers", "Sediment layers"),
        }
