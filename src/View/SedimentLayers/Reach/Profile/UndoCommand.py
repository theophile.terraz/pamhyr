# UndoCommand.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Geometry.Reach import Reach
from Model.Geometry.Profile import Profile
from Model.SedimentLayer.SedimentLayer import SedimentLayer
from Model.SedimentLayer.SedimentLayerList import SedimentLayerList

logger = logging.getLogger()


class SetSLCommand(QUndoCommand):
    def __init__(self, profile, index, new_value):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._index = index
        self._old = self._profile.point(self._index).sl
        self._new = new_value

    def undo(self):
        self._profile.point(self._index).sl = self._old

    def redo(self):
        self._profile.point(self._index).sl = self._new
