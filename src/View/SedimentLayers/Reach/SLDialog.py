# SLDialog.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QComboBox, QUndoStack, QShortcut,
    QDoubleSpinBox,
)

from View.SedimentLayers.Reach.translate import *

_translate = QCoreApplication.translate


class SLDialog(PamhyrDialog):
    _pamhyr_ui = "SLDialog"
    _pamhyr_name = "SL"

    def __init__(self, study=None, config=None, trad=None, parent=None):
        super(SLDialog, self).__init__(
            title=trad[self._pamhyr_name],
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self.setup_combobox()

        self.value = None

    def setup_combobox(self):
        self.combobox_add_items(
            "comboBox",
            [self._trad["not_defined"]] +
            list(
                map(
                    lambda sl: str(sl),
                    self._study.river.sediment_layers.sediment_layers
                )
            )
        )

    @property
    def sl(self):
        if self.value == self._trad["not_defined"]:
            return None

        return next(
            filter(
                lambda sl: str(sl) == self.value,
                self._study.river.sediment_layers.sediment_layers
            )
        )

    def accept(self):
        self.value = self.get_combobox_text("comboBox")
        super().accept()
