# Table.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox,
)

from View.Tools.PamhyrTable import PamhyrTableModel

from View.SedimentLayers.Edit.UndoCommand import *

_translate = QCoreApplication.translate


class TableModel(PamhyrTableModel):
    def data(self, index, role):
        if role != Qt.ItemDataRole.DisplayRole:
            return QVariant()

        row = index.row()
        column = index.column()

        if self._headers[column] == "name":
            return self._data.get(row).name
        elif self._headers[column] == "type":
            return self._data.get(row).type
        elif self._headers[column] == "height":
            return self._data.get(row).height
        elif self._headers[column] == "d50":
            return self._data.get(row).d50
        elif self._headers[column] == "sigma":
            return self._data.get(row).sigma
        elif self._headers[column] == "critical_constraint":
            return self._data.get(row).critical_constraint

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or role != Qt.EditRole:
            return False

        row = index.row()
        column = index.column()

        if self._headers[column] == "name":
            self._undo.push(
                SetNameCommand(
                    self._data, row, value
                )
            )
        if self._headers[column] == "type":
            self._undo.push(
                SetTypeCommand(
                    self._data, row, value
                )
            )
        if self._headers[column] == "height":
            self._undo.push(
                SetHeightCommand(
                    self._data, row, value
                )
            )
        if self._headers[column] == "d50":
            self._undo.push(
                SetD50Command(
                    self._data, row, value
                )
            )
        if self._headers[column] == "sigma":
            self._undo.push(
                SetSigmaCommand(
                    self._data, row, value
                )
            )
        if self._headers[column] == "critical_constraint":
            self._undo.push(
                SetCriticalConstraintCommand(
                    self._data, row, value
                )
            )

        self.dataChanged.emit(index, index)
        return True

    def add(self, row, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row - 1)

        self._undo.push(
            AddCommand(
                self._data, row
            )
        )

        self.endInsertRows()
        self.layoutChanged.emit()

    def delete(self, rows, parent=QModelIndex()):
        self.beginRemoveRows(parent, rows[0], rows[-1])

        self._undo.push(
            DelCommand(
                self._data, rows
            )
        )

        self.endRemoveRows()
        self.layoutChanged.emit()

    def move_up(self, row, parent=QModelIndex()):
        if row <= 0:
            return

        target = row + 2

        self.beginMoveRows(parent, row - 1, row - 1, parent, target)

        self._undo.push(
            MoveCommand(
                self._data, "up", row
            )
        )

        self.endMoveRows()
        self.layoutChanged.emit()

    def move_down(self, row, parent=QModelIndex()):
        if row + 1 >= len(self._data):
            return

        target = row

        self.beginMoveRows(parent, row + 1, row + 1, parent, target)

        self._undo.push(
            MoveCommand(
                self._data, "down", row
            )
        )

        self.endMoveRows()
        self.layoutChanged.emit()

    def undo(self):
        self._undo.undo()
        self.layoutChanged.emit()

    def redo(self):
        self._undo.redo()
        self.layoutChanged.emit()
