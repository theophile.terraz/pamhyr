# UndoCommand.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.SedimentLayer.SedimentLayer import SedimentLayer
from Model.SedimentLayer.SedimentLayerList import SedimentLayerList


class SetNameCommand(QUndoCommand):
    def __init__(self, sediment_layers, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._index = index
        self._old = self._sediment_layers.get(self._index).name
        self._new = new_value

    def undo(self):
        self._sediment_layers.get(self._index).name = self._old

    def redo(self):
        self._sediment_layers.get(self._index).name = self._new


class SetTypeCommand(QUndoCommand):
    def __init__(self, sediment_layers, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._index = index
        self._old = self._sediment_layers.get(self._index).type
        self._new = new_value

    def undo(self):
        self._sediment_layers.get(self._index).type = self._old

    def redo(self):
        self._sediment_layers.get(self._index).type = self._new


class SetHeightCommand(QUndoCommand):
    def __init__(self, sediment_layers, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._index = index
        self._old = self._sediment_layers.get(self._index).height
        self._new = new_value

    def undo(self):
        self._sediment_layers.get(self._index).height = self._old

    def redo(self):
        self._sediment_layers.get(self._index).height = self._new


class SetD50Command(QUndoCommand):
    def __init__(self, sediment_layers, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._index = index
        self._old = self._sediment_layers.get(self._index).d50
        self._new = new_value

    def undo(self):
        self._sediment_layers.get(self._index).d50 = self._old

    def redo(self):
        self._sediment_layers.get(self._index).d50 = self._new


class SetSigmaCommand(QUndoCommand):
    def __init__(self, sediment_layers, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._index = index
        self._old = self._sediment_layers.get(self._index).sigma
        self._new = new_value

    def undo(self):
        self._sediment_layers.get(self._index).sigma = self._old

    def redo(self):
        self._sediment_layers.get(self._index).sigma = self._new


class SetCriticalConstraintCommand(QUndoCommand):
    def __init__(self, sediment_layers, index, new_value):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._index = index
        self._old = self._sediment_layers.get(self._index).critical_constraint
        self._new = new_value

    def undo(self):
        self._sediment_layers.get(self._index).critical_constraint = self._old

    def redo(self):
        self._sediment_layers.get(self._index).critical_constraint = self._new


class AddCommand(QUndoCommand):
    def __init__(self, sediment_layers, index):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._index = index
        self._new = None

    def undo(self):
        self._sediment_layers.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._sediment_layers.new(self._index)
        else:
            self._sediment_layers.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, sediment_layers, rows):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._rows = rows

        self._sl_pos = []
        for row in rows:
            self._sl_pos.append((row, self._sediment_layers.get(row)))
        self._sl_pos.sort()

    def undo(self):
        for row, el in self._sl_pos:
            self._sediment_layers.insert(row, el)

    def redo(self):
        self._sediment_layers.delete_i(self._rows)


class MoveCommand(QUndoCommand):
    def __init__(self, sediment_layers, up, i):
        QUndoCommand.__init__(self)

        self._sediment_layers = sediment_layers
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._sediment_layers.move_up(self._i)
        else:
            self._sediment_layers.move_down(self._i)

    def redo(self):
        if self._up:
            self._sediment_layers.move_up(self._i)
        else:
            self._sediment_layers.move_down(self._i)
