# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
)

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.Tools.Plot.PamhyrToolbar import PamhyrPlotToolbar

from View.SedimentLayers.Edit.UndoCommand import *
from View.SedimentLayers.Edit.Table import *
from View.SedimentLayers.Edit.Plot import Plot
from View.SedimentLayers.Edit.translate import SedimentEditTranslate

_translate = QCoreApplication.translate

logger = logging.getLogger()


class EditSedimentLayersWindow(PamhyrWindow):
    _pamhyr_ui = "EditSedimentLayers"
    _pamhyr_name = "Edit Sediment Layers"

    def __init__(self, study=None, config=None,
                 sl=None, parent=None):
        self._sl = sl

        sl_name = self._sl.name
        if sl_name == "":
            sl_name = _translate("SedimentLayers", "(no name)")

        trad = SedimentEditTranslate()
        name = (
            trad[self._pamhyr_name] + " - " + study.name + " - " + sl_name
        )

        super(EditSedimentLayersWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self.setup_table()
        self.setup_graph()
        self.setup_connections()

    def setup_table(self):
        table_headers = self._trad.get_dict("table_headers")
        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=table_headers,
            editable_headers=list(table_headers),
            data=self._sl,
            trad=self._trad,
            undo=self._undo_stack,
        )
        table.setModel(self._table)
        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        table.setAlternatingRowColors(True)

    def setup_graph(self):
        self.canvas = MplCanvas(width=5, height=4, dpi=100)
        self.canvas.setObjectName("canvas")
        self.toolbar = PamhyrPlotToolbar(
            self.canvas, self
        )
        self.plot_layout = self.find(QVBoxLayout, "verticalLayout")
        self.plot_layout.addWidget(self.toolbar)
        self.plot_layout.addWidget(self.canvas)
        self._set_plot()

    def _set_plot(self):
        self.plot = Plot(
            canvas=self.canvas,
            data=self._sl,
            toolbar=self.toolbar,
            trad=self._trad,
        )
        self.plot.draw()

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_del").triggered.connect(self.delete)
        self.find(QAction, "action_move_up").triggered.connect(self.move_up)
        self.find(QAction, "action_move_down").triggered.connect(
            self.move_down)

        self._table.dataChanged.connect(self._set_plot)
        self._table.layoutChanged.connect(self._set_plot)

    def index_selected_rows(self):
        table = self.find(QTableView, f"tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()
        if len(self._sl) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)

    def move_up(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.move_up(rows[0])

    def move_down(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.move_down(rows[0])

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()
