# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
)

from View.SedimentLayers.UndoCommand import *
from View.SedimentLayers.Table import *

from View.SedimentLayers.Edit.Plot import Plot

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.SedimentLayers.translate import SedimentTranslate

from View.SedimentLayers.Edit.Window import EditSedimentLayersWindow

logger = logging.getLogger()


class SedimentLayersWindow(PamhyrWindow):
    _pamhyr_ui = "SedimentLayersList"
    _pamhyr_name = "Sediment Layers List"

    def __init__(self, study=None, config=None, parent=None):
        self._sediment_layers = study.river.sediment_layers

        trad = SedimentTranslate()
        name = (
            trad[self._pamhyr_name] + " - " + study.name
        )

        super(SedimentLayersWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self.setup_table()
        self.setup_plot()
        self.setup_connections()

    def setup_table(self):
        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=["name", "comment"],
            data=self._study.river.sediment_layers,
            trad=self._trad,
            undo=self._undo_stack,
        )
        table.setModel(self._table)
        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        table.setAlternatingRowColors(True)

    def setup_plot(self):
        self.canvas = MplCanvas(width=2, height=4, dpi=100)
        self.canvas.setObjectName("canvas")
        self.plot_layout = self.find(QVBoxLayout, "verticalLayout")
        self.plot_layout.addWidget(self.canvas)

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_del").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(
            self.edit_sediment_layers)

        table = self.find(QTableView, f"tableView")
        table.selectionModel()\
             .selectionChanged\
             .connect(self._set_current_sl)

        self._table.dataChanged\
                   .connect(self._set_current_sl)

    def _set_current_sl(self):
        rows = self.index_selected_rows()

        if len(rows) == 0:
            return

        self.plot = Plot(
            canvas=self.canvas,
            data=self._sediment_layers.get(rows[0]),
            trad=self._trad,
            toolbar=None,
        )
        self.plot.draw()

    def index_selected_rows(self):
        table = self.find(QTableView, f"tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()
        if len(self._sediment_layers) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()

    def edit_sediment_layers(self):
        rows = self.index_selected_rows()

        for row in rows:
            slw = EditSedimentLayersWindow(
                study=self._study,
                sl=self._sediment_layers.get(row),
                parent=self
            )
            slw.show()
