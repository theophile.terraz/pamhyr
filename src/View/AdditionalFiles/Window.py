# Window.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import trace, timer

from PyQt5.QtWidgets import (
    QAction, QListView,
)

from View.Tools.PamhyrWindow import PamhyrWindow

from View.AdditionalFiles.List import ListModel
from View.AdditionalFiles.Translate import AddFileTranslate
from View.AdditionalFiles.Edit.Window import EditAddFileWindow


class AddFileListWindow(PamhyrWindow):
    _pamhyr_ui = "AdditionalFileList"
    _pamhyr_name = "Additional files"

    def __init__(self, study=None, config=None,
                 parent=None):
        trad = AddFileTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(AddFileListWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            options=[],
            parent=parent
        )

        self.setup_list()
        self.setup_connections()

    def setup_list(self):
        lst = self.find(QListView, f"listView")
        self._list = ListModel(
            list_view=lst,
            data=self._study.river.additional_files,
            undo=self._undo_stack,
            trad=self._trad,
        )

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)

    def update(self):
        self._list.update()

    def selected_rows(self):
        lst = self.find(QListView, f"listView")
        return list(map(lambda i: i.row(), lst.selectedIndexes()))

    def add(self):
        rows = self.selected_rows()
        if len(rows) > 0:
            row = rows[0]
        else:
            row = 0

        self._list.add(row)

    def delete(self):
        rows = self.selected_rows()
        if len(rows) == 0:
            return

        self._list.delete(rows[0])

    def edit(self):
        rows = self.selected_rows()

        for row in rows:
            add_file = self._study.river.additional_files.files[row]

            if self.sub_window_exists(
                    EditAddFileWindow,
                    data=[self._study, self._config, add_file]
            ):
                continue

            win = EditAddFileWindow(
                study=self._study,
                config=self._config,
                add_file=add_file,
                trad=self._trad,
                undo=self._undo_stack,
                parent=self,
            )
            win.show()

    def _undo(self):
        self._undo_stack.undo()
        self.update()

    def _redo(self):
        self._undo_stack.redo()
        self.update()
