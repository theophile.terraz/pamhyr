# Window.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from Modules import Modules
from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtWidgets import (
    QLabel, QPlainTextEdit, QPushButton,
    QCheckBox,
)

from View.AdditionalFiles.Translate import AddFileTranslate
from View.AdditionalFiles.UndoCommand import (
    SetCommand
)

logger = logging.getLogger()


class EditAddFileWindow(PamhyrWindow):
    _pamhyr_ui = "AdditionalFile"
    _pamhyr_name = "Edit additional file"

    def __init__(self, study=None, config=None, add_file=None,
                 trad=None, undo=None, parent=None):

        name = trad[self._pamhyr_name] + " - " + study.name
        super(EditAddFileWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            options=[],
            parent=parent
        )

        self._add_file = add_file
        self._hash_data.append(self._add_file)

        self._undo = undo

        self.setup_values()
        self.setup_connection()

    def setup_values(self):
        self.set_check_box("checkBox", self._add_file.enabled)
        self.set_line_edit_text("lineEdit_name", self._add_file.name)
        self.set_line_edit_text("lineEdit_path", self._add_file.path)
        self.set_plaintext_edit_text("plainTextEdit", self._add_file.text)

    def setup_connection(self):
        self.find(QPushButton, "pushButton_cancel")\
            .clicked.connect(self.close)
        self.find(QPushButton, "pushButton_ok")\
            .clicked.connect(self.accept)

    def accept(self):
        is_enabled = self.get_check_box("checkBox")
        name = self.get_line_edit_text("lineEdit_name")
        path = self.get_line_edit_text("lineEdit_path")
        text = self.get_plaintext_edit_text("plainTextEdit")

        self._undo.push(
            SetCommand(
                self._add_file,
                enabled=is_enabled,
                name=name,
                path=path,
                text=text,
            )
        )

        self._propagate_update(key=Modules.ADDITIONAL_FILES)
        self.close()
