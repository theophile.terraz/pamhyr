# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
)

from View.Reservoir.Table import (
    TableModel, ComboBoxDelegate
)

from View.Network.GraphWidget import GraphWidget
from View.Reservoir.Translate import ReservoirTranslate
from View.Reservoir.Edit.Window import EditReservoirWindow

_translate = QCoreApplication.translate

logger = logging.getLogger()


class ReservoirWindow(PamhyrWindow):
    _pamhyr_ui = "ReservoirList"
    _pamhyr_name = "Reservoir"

    def __init__(self, study=None, config=None, parent=None):
        trad = ReservoirTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(ReservoirWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._reservoir_lst = self._study.river.reservoir

        self.setup_table()
        self.setup_graph()
        self.setup_connections()

    def setup_table(self):
        self._table = None

        self._delegate_node = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            parent=self
        )

        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=["name", "node"],
            delegates={
                "node": self._delegate_node,
            },
            trad=self._trad,
            data=self._study.river,
            undo=self._undo_stack,
        )

    def setup_graph(self):
        self.graph_widget = GraphWidget(
            self._study.river,
            min_size=None, size=(200, 200),
            only_display=True,
            parent=self
        )
        self.graph_layout = self.find(QVBoxLayout, "verticalLayout")
        self.graph_layout.addWidget(self.graph_widget)

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)

    def index_selected_row(self):
        table = self.find(QTableView, "tableView")
        return table.selectionModel()\
                    .selectedRows()[0]\
                    .row()

    def index_selected_rows(self):
        table = self.find(QTableView, "tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()
        if len(self._reservoir_lst) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()

    def edit(self):
        rows = self.index_selected_rows()
        for row in rows:
            data = self._reservoir_lst.get(row)

            if self.sub_window_exists(
                EditReservoirWindow,
                data=[self._study, None, data]
            ):
                continue

            win = EditReservoirWindow(
                data=data,
                study=self._study,
                parent=self
            )
            win.show()
