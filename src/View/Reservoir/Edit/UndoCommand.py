# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Reservoir.Reservoir import Reservoir

logger = logging.getLogger()


class SetDataCommand(QUndoCommand):
    def __init__(self, data, index, column, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._column = column
        self._old = self._data.get_i(self._index)[self._column]
        self._new = new_value

    def undo(self):
        self._data._set_i_c_v(self._index, self._column, self._old)

    def redo(self):
        self._data._set_i_c_v(self._index, self._column, self._new)


class AddCommand(QUndoCommand):
    def __init__(self, data, index):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._new = None

    def undo(self):
        self._data.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._data.add(self._index)
        else:
            self._data.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, data, rows):
        QUndoCommand.__init__(self)

        self._data = data
        self._rows = rows

        self._point = []
        for row in rows:
            self._point.append((row, self._data.get_i(row)))
        self._point.sort()

    def undo(self):
        for row, el in self._point:
            self._data.insert(row, el)

    def redo(self):
        self._data.delete_i(self._rows)


class SortCommand(QUndoCommand):
    def __init__(self, data, _reverse):
        QUndoCommand.__init__(self)

        self._data = data
        self._reverse = _reverse

        self._old = self._data.data
        self._indexes = None

    def undo(self):
        ll = self._data.data
        self._data.sort(
            key=lambda x: self._indexes[ll.index(x)]
        )

    def redo(self):
        self._data.sort(
            _reverse=self._reverse,
            key=lambda x: x[0]
        )
        if self._indexes is None:
            self._indexes = list(
                map(
                    lambda p: self._old.index(p),
                    self._data.data
                )
            )
            self._old = None


class PasteCommand(QUndoCommand):
    def __init__(self, data, row, hs):
        QUndoCommand.__init__(self)

        self._data = data
        self._row = row
        self._h = hs
        self._h.reverse()

    def undo(self):
        self._data.delete_i(
            range(self._row, self._row + len(self._h))
        )

    def redo(self):
        for h in self._h:
            self._data.insert(self._row, h)
