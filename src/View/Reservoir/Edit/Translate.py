# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

from View.Reservoir.Translate import ReservoirTranslate

_translate = QCoreApplication.translate


class EditReservoirTranslate(ReservoirTranslate):
    def __init__(self):
        super(EditReservoirTranslate, self).__init__()

        self._dict["Edit Reservoir"] = _translate(
            "Reservoir", "Edit Reservoir"
        )

        self._sub_dict["table_headers"] = {
            "z": self._dict["unit_elevation"],
            "area": self._dict["unit_area_he"],
        }
