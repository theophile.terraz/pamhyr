# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timer, trace

from View.Tools.PamhyrWindow import PamhyrWindow
from View.Tools.PamhyrWidget import PamhyrWidget

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5 import QtCore
from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel, QCoreApplication,
    pyqtSlot, pyqtSignal,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QHeaderView, QDoubleSpinBox, QVBoxLayout,
)

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.Tools.Plot.PamhyrToolbar import PamhyrPlotToolbar

from View.Reservoir.Edit.Translate import EditReservoirTranslate
from View.Reservoir.Edit.Table import TableModel
from View.Reservoir.Edit.Plot import Plot

_translate = QCoreApplication.translate

logger = logging.getLogger()


class EditReservoirWindow(PamhyrWindow):
    _pamhyr_ui = "Reservoir"
    _pamhyr_name = "Edit Reservoir"

    def __init__(self, data=None, study=None, config=None, parent=None):
        self._data = data
        trad = EditReservoirTranslate()

        name = trad[self._pamhyr_name]
        if self._data is not None:
            node_name = (self._data.node.name if self._data.node is not None
                         else trad['not_associated'])
            name += (
                f" - {study.name} " +
                f" - {self._data.name} ({self._data.id}) " +
                f"({node_name})"
            )

        super(EditReservoirWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._hash_data.append(data)

        self.setup_table()
        self.setup_plot()
        self.setup_connections()

    def setup_table(self):
        headers = {}
        table_headers = self._trad.get_dict("table_headers")

        table = self.find(QTableView, "tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=table_headers,
            editable_headers=table_headers,
            delegates={},
            data=self._data,
            undo=self._undo_stack,
            opt_data=self._study.time_system
        )

        table.setModel(self._table)
        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        table.setAlternatingRowColors(True)

    def setup_plot(self):
        self.canvas = MplCanvas(width=5, height=4, dpi=100)
        self.canvas.setObjectName("canvas")
        self.toolbar = PamhyrPlotToolbar(
            self.canvas, self
        )
        self.verticalLayout.addWidget(self.toolbar)
        self.verticalLayout.addWidget(self.canvas)

        self.plot = Plot(
            canvas=self.canvas,
            data=self._data,
            mode=self._study.time_system,
            trad=self._trad,
            toolbar=self.toolbar,
        )
        self.plot.draw()

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self.find(QAction, "action_sort").triggered.connect(self.sort)

        self._table.dataChanged.connect(self.update)

    def update(self):
        self.plot.update()

    def index_selected_row(self):
        table = self.find(QTableView, "tableView")
        return table.selectionModel()\
                    .selectedRows()[0]\
                    .row()

    def index_selected_rows(self):
        table = self.find(QTableView, "tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()
        if len(self._data) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

        self.plot.update()

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)
        self.plot.update()

    def sort(self):
        self._table.sort(False)
        self.plot.update()

    def _copy(self):
        rows = self.index_selected_rows()

        table = []
        # table.append(self._data.header)
        table.append(self._trad.get_dict("table_headers"))

        data = self._data.data
        for row in rows:
            table.append(list(data[row]))

        self.copyTableIntoClipboard(table)

    def _paste(self):
        header, data = self.parseClipboardTable()

        logger.debug(f"paste: h:{header}, d:{data}")

        if len(data) == 0:
            return

        row = 0
        rows = self.index_selected_rows()
        if len(rows) != 0:
            row = rows[0]

        self._table.paste(row, data)
        self.plot.update()

    def _undo(self):
        self._table.undo()
        self.update()

    def _redo(self):
        self._table.redo()
        self.update()
