# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Reservoir.Reservoir import Reservoir
from Model.Reservoir.ReservoirList import ReservoirList


class SetNameCommand(QUndoCommand):
    def __init__(self, reservoir_lst, index, new_value):
        QUndoCommand.__init__(self)

        self._reservoir_lst = reservoir_lst
        self._index = index
        self._old = self._reservoir_lst.get(self._index).name
        self._new = str(new_value)

    def undo(self):
        self._reservoir_lst.get(self._index).name = self._old

    def redo(self):
        self._reservoir_lst.get(self._index).name = self._new


class SetNodeCommand(QUndoCommand):
    def __init__(self, reservoir_lst, index, node):
        QUndoCommand.__init__(self)

        self._reservoir_lst = reservoir_lst
        self._index = index
        self._old = self._reservoir_lst.get(self._index).node
        self._new = node
        self._prev_assoc_to_node = self._reservoir_lst.get_assoc_to_node(node)

    def _previous_assoc_node(self, node):
        if self._prev_assoc_to_node is not None:
            self._prev_assoc_to_node.node = node

    def undo(self):
        self._reservoir_lst.get(self._index).node = self._old
        self._previous_assoc_node(self._new)

    def redo(self):
        self._reservoir_lst.get(self._index).node = self._new
        self._previous_assoc_node(None)


class AddCommand(QUndoCommand):
    def __init__(self, reservoir_lst, index):
        QUndoCommand.__init__(self)

        self._reservoir_lst = reservoir_lst

        self._index = index
        self._new = None

    def undo(self):
        self._reservoir_lst.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._reservoir_lst.new(self._index)
        else:
            self._reservoir_lst.insert(self._index, self._new)


class AddAndAssociateCommand(QUndoCommand):
    def __init__(self, reservoir_lst, index, node):
        QUndoCommand.__init__(self)

        self._reservoir_lst = reservoir_lst

        self._new = None
        self._index = index
        self._node = node

    def undo(self):
        self._reservoir_lst.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._reservoir_lst.new(self._index)
        else:
            self._reservoir_lst.insert(self._index, self._new)

        self._reservoir_lst.get(self._index).node = self._node


class DelCommand(QUndoCommand):
    def __init__(self, reservoir_lst, rows):
        QUndoCommand.__init__(self)

        self._reservoir_lst = reservoir_lst

        self._rows = rows

        self._reservoir = []
        for row in rows:
            self._reservoir.append((row, self._reservoir_lst.get(row)))
        self._reservoir.sort()

    def undo(self):
        for row, el in self._reservoir:
            self._reservoir_lst.insert(row, el)

    def redo(self):
        self._reservoir_lst.delete_i(self._rows)


class PasteCommand(QUndoCommand):
    def __init__(self, reservoir_lst, row, reservoir):
        QUndoCommand.__init__(self)

        self._reservoir_lst = reservoir_lst

        self._row = row
        self._reservoir = deepcopy(reservoir)
        self._reservoir.reverse()

    def undo(self):
        self._reservoir_lst.delete_i(
            self._tab,
            range(self._row, self._row + len(self._reservoir))
        )

    def redo(self):
        for r in self._reservoir:
            self._reservoir_lst.insert(self._row, r)
