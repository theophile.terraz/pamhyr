# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.D90AdisTS.D90AdisTS import D90AdisTS
from Model.D90AdisTS.D90AdisTSList import D90AdisTSList


class SetCommand(QUndoCommand):
    def __init__(self, data, row, column, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._row = row
        self._column = column

        if self._column == "name":
            self._old = self._data[self._row].name
        elif self._column == "d90":
            self._old = self._data[self._row].d90

        _type = float
        if column == "name":
            _type = str

        self._new = _type(new_value)

    def undo(self):
        if self._column == "name":
            self._data[self._row].name = self._old
        elif self._column == "d90":
            self._data[self._row].d90 = self._old

    def redo(self):
        if self._column == "name":
            self._data[self._row].name = self._new
        elif self._column == "d90":
            self._data[self._row].d90 = self._new


class SetCommandSpec(QUndoCommand):
    def __init__(self, data, row, column, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._row = row
        self._column = column

        if self._column == "name":
            self._old = self._data[self._row].name
        elif self._column == "reach":
            self._old = self._data[self._row].reach
        elif self._column == "start_rk":
            self._old = self._data[self._row].start_rk
        elif self._column == "end_rk":
            self._old = self._data[self._row].end_rk
        elif self._column == "d90":
            self._old = self._data[self._row].d90

        _type = float
        if column == "name":
            _type = str
        elif column == "reach":
            _type = int

        self._new = _type(new_value)

    def undo(self):
        if self._column == "name":
            self._data[self._row].name = self._old
        elif self._column == "reach":
            self._data[self._row].reach = self._old
        elif self._column == "start_rk":
            self._data[self._row].start_rk = self._old
        elif self._column == "end_rk":
            self._data[self._row].end_rk = self._old
        elif self._column == "d90":
            self._data[self._row].d90 = self._old

    def redo(self):
        if self._column == "name":
            self._data[self._row].name = self._new
        elif self._column == "reach":
            self._data[self._row].reach = self._new
        elif self._column == "start_rk":
            self._data[self._row].start_rk = self._new
        elif self._column == "end_rk":
            self._data[self._row].end_rk = self._new
        elif self._column == "d90":
            self._data[self._row].d90 = self._new


class AddCommand(QUndoCommand):
    def __init__(self, data, ics_spec, index):
        QUndoCommand.__init__(self)

        self._data = data
        self._ics_spec = ics_spec
        self._index = index
        self._new = None

    def undo(self):
        self._data.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._data.new(self._index)
        else:
            self._data.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, data, ics_spec, rows):
        QUndoCommand.__init__(self)

        self._data = data
        self._ics_spec = ics_spec
        self._rows = rows
        # self._data = data

        self._ic = []
        for row in rows:
            self._ic.append((row, self._ics_spec[row]))
        self._ic.sort()

    def undo(self):
        for row, el in self._ic:
            self._data.insert(row, el)

    def redo(self):
        self._data.delete_i(self._rows)
