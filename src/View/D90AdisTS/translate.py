# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class D90AdisTSTranslate(MainTranslate):
    def __init__(self):
        super(D90AdisTSTranslate, self).__init__()

        self._dict["D90 AdisTS"] = _translate(
            "D90AdisTS", "D90 AdisTS")

        self._dict["rk"] = self._dict["unit_rk"]

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "d90": _translate("Unit", "D90"),
        }

        self._sub_dict["table_headers_spec"] = {
            "name": self._dict["name"],
            "reach": self._dict["reach"],
            "start_rk": _translate("Unit", "Start_RK (m)"),
            "end_rk": _translate("Unit", "End_RK (m)"),
            "d90": _translate("Unit", "D90"),
        }
