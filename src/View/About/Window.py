# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import os
import logging

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtCore import QCoreApplication

_translate = QCoreApplication.translate
logger = logging.getLogger()


class AboutWindow(PamhyrDialog):
    _pamhyr_ui = "about"
    _pamhyr_name = "About"

    def _path_file(self, filename):
        return os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                "..", "..", filename
            )
        )

    def __init__(self, study=None, config=None, parent=None):
        super(AboutWindow, self).__init__(
            title=_translate("About", "About"),
            study=study,
            config=config,
            options=[],
            parent=parent
        )

        # Version
        with open(self._path_file("VERSION"), "r") as f:
            version = f.readline().strip()
            logger.info(f"version:  {version}")

            label = self.get_label_text("label_version")
            label = label.replace("@version", version)
            label = label.replace("@codename", "(Adis-TS)")
            self.set_label_text("label_version", label)

        # Authors
        with open(self._path_file("AUTHORS"), "r") as f:
            label = ""
            try:
                while True:
                    author = next(f).strip()
                    logger.info(f"author: {author}")
                    label = f"\n  - {author}" + label
            except StopIteration:
                label = _translate("About", "Contributors: ") + label
                # label = "Copyright © 2022-2024  INRAE\n" + label
                self.set_label_text("label_copyright", label)
