# Worker.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import time
import logging

from PyQt5.QtCore import (
    Qt, QObject, pyqtSlot, pyqtSignal
)

logger = logging.getLogger()


class Worker(QObject):
    signalStatus = pyqtSignal(str)

    def __init__(self, study, checker_list, parent=None):
        super(self.__class__, self).__init__(parent)

        self._study = study
        self._checker_list = checker_list

    def process(self):
        self.signalStatus.emit('start')
        self._compute()
        self.signalStatus.emit('end')

    def _compute(self):
        # The computation loop
        for checker in self._checker_list:
            self.signalStatus.emit(checker.name)

            # Run checker
            checker.run(self._study)

            self.signalStatus.emit("progress")


class TabWorker(QObject):
    signalStatus = pyqtSignal(str)

    def __init__(self, study, checker_q, parent=None):
        super(self.__class__, self).__init__(parent)

        self._study = study
        self._checker_q = checker_q

    @property
    def study(self):
        return self._study

    @study.setter
    def study(self, study):
        self._study = study

    def process(self):
        self._compute()

    def _compute(self):
        while True:
            checker = self._checker_q.get()

            if self._study is None:
                self._checker_q.put(checker)
                time.sleep(5)
            else:
                checker.run(self._study)
                self.signalStatus.emit("progress")
