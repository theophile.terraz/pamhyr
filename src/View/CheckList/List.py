# List.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from functools import reduce
from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant,
)

from PyQt5.QtGui import (
    QColor, QBrush,
)

from Modules import Modules
from Checker.Checker import STATUS
from View.Tools.PamhyrList import PamhyrListModel

logger = logging.getLogger()


class TabListModel(PamhyrListModel):
    def data(self, index, role):
        row = index.row()
        column = index.column()

        checker = self._data[row]

        if role == Qt.ForegroundRole:
            color = Qt.gray
            status = checker._status

            if status is STATUS.OK:
                color = Qt.green
            elif status is STATUS.WARNING:
                color = QColor("orange")
            elif status is STATUS.ERROR:
                color = Qt.red

            return QBrush(color)

        if role == Qt.ItemDataRole.DisplayRole:
            msg = checker.name

            if checker.is_error() or checker.is_warning():
                msg += f": {checker.summary}"

            return msg

        return QVariant()
