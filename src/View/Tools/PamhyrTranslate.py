# PamhyrTranslate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

_translate = QCoreApplication.translate


class PamhyrTranslate(object):
    def __init__(self):
        # Module global dictionnary
        self._dict = {}
        # Module sub dictionnary
        self._sub_dict = {}

        self._dict["pamhyr"] = _translate("Pamhyr", "Pamhyr2")

    def __getitem__(self, index):
        if index not in self._dict:
            return None
        return self._dict[index]

    def _get_in_sub_dict(self, dictionary, index):
        if dictionary not in self._sub_dict:
            return None
        return self._sub_dict[index]

    def get(self, dictionary, index):
        if dictionary == "global":
            return self[index]

        return self._get_in_sub_dict(dictionary, index)

    def get_dict(self, dictionary):
        _dict = None

        if dictionary == "global":
            _dict = self._dict

        if dictionary in self._sub_dict:
            _dict = self._sub_dict[dictionary]

        return _dict
