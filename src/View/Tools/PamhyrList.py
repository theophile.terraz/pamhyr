# PamhyrList.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging
import traceback

from tools import trace, timer

from Model.Except import NotImplementedMethodeError

from PyQt5.QtCore import (
    Qt, QVariant,
    QAbstractListModel,
    QModelIndex,
)

from PyQt5.QtWidgets import (
    QAbstractItemView,
)

logger = logging.getLogger()


class PamhyrListModel(QAbstractListModel):
    def __init__(self,
                 list_view=None,
                 trad=None,
                 data=None,
                 undo=None,
                 opt_data=None,
                 options=[],
                 parent=None):
        super(PamhyrListModel, self).__init__()

        self._list_view = list_view

        self._trad = trad
        self._parent = parent

        self._data = data
        self._opt_data = opt_data
        self._options = options
        self._undo = undo

        self._list_view_configure()

    def _list_view_configure(self):
        self._list_view.setModel(self)

    def rowCount(self, parent=QModelIndex()):
        return len(self._data)

    def columnCount(self, parent=QModelIndex()):
        return 1

    def data(self, index, role):
        raise NotImplementedMethodeError(self, self.data)

    def undo(self):
        self._undo.undo()
        self.update()

    def redo(self):
        self._undo.redo()
        self.update()

    def update(self):
        self.layoutChanged.emit()
