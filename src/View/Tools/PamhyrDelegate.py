# PamhyrDelegate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timestamp

from PyQt5.QtCore import (
    Qt, QRect, QTime, QDateTime, pyqtSlot,
)

from PyQt5.QtWidgets import (
    QSpinBox, QTimeEdit, QDateTimeEdit, QItemDelegate,
)

from View.Tools.PamhyrWidget import (
    PamhyrExtendedTimeEdit, PamhyrExtendedDateTimeEdit
)

logger = logging.getLogger()


class PamhyrExTimeDelegate(QItemDelegate):
    def __init__(self, data=None, mode="time", parent=None):
        super(PamhyrExTimeDelegate, self).__init__(parent)

        self._data = data
        self._mode = mode

    def createEditor(self, parent, option, index):
        if self._mode == "time":
            self.editor = PamhyrExtendedTimeEdit(parent=parent)
        else:
            self.editor = PamhyrExtendedDateTimeEdit(parent=parent)
        value = index.data(Qt.DisplayRole)
        self.editor.set_time(value)
        logger.debug(str(value))
        return self.editor

    def setModelData(self, editor, model, index):
        time = editor.get_time()
        if self._mode == "time":
            model.setData(index, int(time.total_seconds()))
        else:
            logger.debug(str(timestamp(time)))
            model.setData(index, int(timestamp(time)))
        editor.close()
        editor.deleteLater()

    def updateEditorGeometry(self, editor, option, index):
        r = QRect(option.rect)
        if self.editor.windowFlags() & Qt.Popup:
            if editor.parent() is not None:
                r.setTopLeft(self.editor.parent().mapToGlobal(r.topLeft()))
        editor.setGeometry(r)

    @pyqtSlot()
    def currentItemChanged(self):
        self.commitData.emit(self.sender())
