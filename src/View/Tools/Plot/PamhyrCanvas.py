# MplCanvas.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure


class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, width=5, height=4, dpi=100):
        fig = Figure(
            figsize=(width, height),
            dpi=dpi,
            layout='constrained',
        )
        super(MplCanvas, self).__init__(fig)

        self.axes = fig.add_subplot(111)
        self.axes.format_coord = lambda x, y: (
            '(x = ' + format(x, '1.4f') + ', \t' +
            ' y = ' + format(y, '1.4f') + ')'
        )
        self.axes.grid(color='black', linestyle='--', linewidth=0.5)
        self.axes.yaxis.tick_left()
        self.axes.xaxis.tick_bottom()
        self.axes.spines[['top', 'right']].set_color('none')
        self.add_arrows()

    def add_arrows(self):
        al = 8.
        arrowprops = dict(
            clip_on=True,
            # frac=1.,
            headwidth=5.,
            facecolor='k'
        )
        kwargs = dict(
            xycoords='axes fraction',
            textcoords='offset points',
            arrowprops=arrowprops,
        )
        self.axes.annotate("", (1, 0), xytext=(-al, 0), **kwargs)
        self.axes.annotate("", (0, 1), xytext=(0, -al), **kwargs)
