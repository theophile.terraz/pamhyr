# APlot.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.Except import NotImplementedMethodeError


class APlot(object):
    def __init__(self, data=None):
        super(APlot, self).__init__()

        self._init = False
        self._data = data

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    def draw(self):
        """Draw plot

        Returns:
            Nothing
        """
        raise NotImplementedMethodeError(self, self.draw)

    def update(self, ind=None):
        """Update plot

        Update the plot, update for data index IND if define, else
        update all the plot

        Args:
            ind: Data index to update

        Returns:
            Nothing
        """
        raise NotImplementedMethodeError(self, self.update)
