# Table.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging
import traceback
from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox,
)

from View.Tools.PamhyrTable import PamhyrTableModel

from View.DIFAdisTS.UndoCommand import (
    SetCommand, AddCommand, SetCommandSpec,
    DelCommand,
)

logger = logging.getLogger()

_translate = QCoreApplication.translate


class ComboBoxDelegate(QItemDelegate):
    def __init__(self, data=None, ic_spec_lst=None,
                 trad=None, parent=None, mode="reaches"):
        super(ComboBoxDelegate, self).__init__(parent)

        self._data = data
        self._mode = mode
        self._trad = trad
        self._ic_spec_lst = ic_spec_lst

    def createEditor(self, parent, option, index):
        self.editor = QComboBox(parent)

        val = []
        if self._mode == "rk":
            reach_id = self._ic_spec_lst[index.row()].reach

            reach = next(filter(
                lambda edge: edge.id == reach_id, self._data.edges()
            ))

            if reach_id is not None:
                val = list(
                    map(
                        lambda rk: str(rk), reach.reach.get_rk()
                    )
                )
        elif self._mode == "method":
            val = self._data.dif_adists.lst[0].types
        else:
            val = list(
                map(
                    lambda n: n.name, self._data.edges()
                )
            )

        self.editor.addItems(
            [self._trad['not_associated']] +
            val
        )

        self.editor.setCurrentText(str(index.data(Qt.DisplayRole)))
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(Qt.DisplayRole)
        self.editor.currentTextChanged.connect(self.currentItemChanged)

    def setModelData(self, editor, model, index):
        text = str(editor.currentText())
        model.setData(index, text)
        editor.close()
        editor.deleteLater()

    def updateEditorGeometry(self, editor, option, index):
        r = QRect(option.rect)
        if self.editor.windowFlags() & Qt.Popup:
            if editor.parent() is not None:
                r.setTopLeft(self.editor.parent().mapToGlobal(r.topLeft()))
        editor.setGeometry(r)

    @pyqtSlot()
    def currentItemChanged(self):
        self.commitData.emit(self.sender())


class DIFTableModel(PamhyrTableModel):
    def __init__(self, river=None, data=None, **kwargs):
        self._river = river

        super(DIFTableModel, self).__init__(data=data, **kwargs)

        self._data = data

    def _setup_lst(self):
        self._lst = self._data._data

    def rowCount(self, parent):
        return len(self._lst)

    def data(self, index, role):
        if role != Qt.ItemDataRole.DisplayRole:
            return QVariant()

        row = index.row()
        column = index.column()

        if self._headers[column] is "method":
            n = self._lst[row].method
            if n is None or n == "":
                return self._trad['not_associated']
            return n
        elif self._headers[column] is "reach":
            n = self._lst[row].reach
            if n is None:
                return self._trad['not_associated']
            return next(filter(
                lambda edge: edge.id == n, self._river.edges()
            )).name
        elif self._headers[column] is "start_rk":
            n = self._lst[row].start_rk
            if n is None:
                return self._trad['not_associated']
            return n
        elif self._headers[column] is "end_rk":
            n = self._lst[row].end_rk
            if n is None:
                return self._trad['not_associated']
            return n
        elif self._headers[column] is "dif":
            n = self._lst[row].dif
            if n is None:
                return self._trad['not_associated']
            return n
        elif self._headers[column] is "b":
            n = self._lst[row].b
            if n is None:
                return self._trad['not_associated']
            return n
        elif self._headers[column] is "c":
            n = self._lst[row].c
            if n is None:
                return self._trad['not_associated']
            return n

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or role != Qt.EditRole:
            return False

        row = index.row()
        column = index.column()

        try:
            if self._headers[column] != "reach":
                self._undo.push(
                    SetCommandSpec(
                        self._lst, row, self._headers[column], value
                    )
                )
            elif self._headers[column] == "reach":
                self._undo.push(
                    SetCommandSpec(
                        self._lst,
                        row,
                        self._headers[column],
                        self._river.edge(value).id
                    )
                )
        except Exception as e:
            logger.info(e)
            logger.debug(traceback.format_exc())

        self.dataChanged.emit(index, index)
        return True

    def add(self, row, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row - 1)

        self._undo.push(
            AddCommand(
                self._data, self._lst, row
            )
        )

        self.endInsertRows()
        self.layoutChanged.emit()

    def delete(self, rows, parent=QModelIndex()):
        self.beginRemoveRows(parent, rows[0], rows[-1])

        self._undo.push(
            DelCommand(
                self._data, self._lst, rows
            )
        )

        self.endRemoveRows()
        self.layoutChanged.emit()

    def undo(self):
        self._undo.undo()
        self.layoutChanged.emit()

    def redo(self):
        self._undo.redo()
        self.layoutChanged.emit()
