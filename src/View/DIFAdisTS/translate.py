# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class DIFAdisTSTranslate(MainTranslate):
    def __init__(self):
        super(DIFAdisTSTranslate, self).__init__()

        self._dict["DIF AdisTS"] = _translate(
            "DIFAdisTS", "DIF AdisTS")

        self._dict["rk"] = self._dict["unit_rk"]

        self._sub_dict["table_headers"] = {
            "method": self._dict["method"],
            "dif": _translate("Unit", "DIF"),
            "b": _translate("Unit", "Coeff b"),
            "c": _translate("Unit", "Coeff c"),
        }

        self._sub_dict["table_headers_spec"] = {
            "method": self._dict["method"],
            "reach": self._dict["reach"],
            "start_rk": _translate("Unit", "Start_RK (m)"),
            "end_rk": _translate("Unit", "End_RK (m)"),
            "dif": _translate("Unit", "DIF"),
            "b": _translate("Unit", "Coeff b"),
            "c": _translate("Unit", "Coeff c"),
        }
