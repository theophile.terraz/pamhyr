# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
)

from View.LateralContributionsAdisTS.Table import (
    TableModel, ComboBoxDelegate
)

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.Geometry.PlotXY import PlotXY
from View.LateralContributionsAdisTS.translate \
    import LCTranslate
from View.LateralContributionsAdisTS.Edit.Window \
    import EditLateralContributionAdisTSWindow

logger = logging.getLogger()


class LateralContributionAdisTSWindow(PamhyrWindow):
    _pamhyr_ui = "LateralContributionsAdisTS"
    _pamhyr_name = "Lateral contribution AdisTS"

    def __init__(self, study=None, pollutant=None, config=None, parent=None):
        trad = LCTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        self._pollutant = pollutant

        super(LateralContributionAdisTSWindow, self).__init__(
            title=name,
            study=study,
            trad=trad,
            config=config,
            parent=parent
        )

        self._lcs = self._study.river.lateral_contributions_adists

        self.setup_table()
        self.setup_graph()
        self.setup_connections()

    def setup_table(self):
        self._table = {}

        self._delegate_rk = []

        delegate_rk = ComboBoxDelegate(
            data=None,
            mode="rk",
            trad=self._trad,
            parent=self
        )
        self._delegate_rk.append(delegate_rk)

        self._delegate_edge = ComboBoxDelegate(
            data=self._study.river,
            mode="edge",
            trad=self._trad,
            parent=self
        )

        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=self._trad.get_dict("table_headers"),
            delegates={
                "edge": self._delegate_edge,
                "begin_rk": delegate_rk,
                "end_rk": delegate_rk,
            },
            data=self._study.river,
            undo=self._undo_stack,
            trad=self._trad,
            opt_data="liquid",
            pollutant=self._pollutant,
            lcs_list=self._lcs,
        )
        table.setModel(self._table)
        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        table.setAlternatingRowColors(True)

    def setup_graph(self):
        self.canvas = MplCanvas(width=5, height=4, dpi=100)
        self.canvas.setObjectName("canvas")
        self.plot_layout = self.find(QVBoxLayout, "verticalLayout_2")
        self.plot_layout.addWidget(self.canvas)

        self.plot = PlotXY(
            canvas=self.canvas,
            data=None,
            trad=self._trad,
            toolbar=None,
        )

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_del").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)

        table = self.find(QTableView, f"tableView")
        table.selectionModel().selectionChanged.connect(
            self._set_current_reach
        )

        self._table.dataChanged.connect(self._set_current_reach)

    def index_selected_row(self):
        table = self.find(QTableView, f"tableView")
        return table.selectionModel()\
                    .selectedRows()[0]\
                    .row()

    def index_selected_rows(self):
        table = self.find(QTableView, f"tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def _set_current_reach(self):
        rows = self.index_selected_rows()

        data = None
        highlight = None

        tab = "liquid"

        if len(rows) > 0:
            edge_id = self._study.river\
                       .lateral_contributions_adists.lst[rows[0]]\
                       .edge

            if edge_id:
                edge = next(filter(lambda e: e.id == edge_id,
                                   self._study.river.edges()))
                data = edge.reach
                lc = self._lcs.lst[rows[0]]
                highlight = (lc.begin_rk, lc.end_rk)

                for delegate in self._delegate_rk:
                    delegate.data = edge

        self.plot = PlotXY(
            canvas=self.canvas,
            data=data,
            trad=self._trad,
            toolbar=None,
        )
        self.plot.highlight = highlight
        self.plot.update()

    def add(self):
        rows = self.index_selected_rows()
        if len(self._lcs) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

        self._set_current_reach()

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)
        self._set_current_reach()

    def sort(self):
        self._table.sort(False)
        self._set_current_reach()

    def _copy(self):
        logger.info("TODO: copy")
        self._set_current_reach()

    def _paste(self):
        logger.info("TODO: paste")
        self._set_current_reach()

    def _undo(self):
        self._table.undo()
        self._set_current_reach()

    def _redo(self):
        self._table.redo()
        self._set_current_reach()

    def edit(self):
        rows = self.index_selected_rows()
        for row in rows:
            data = self._lcs.lst[row]

            if self.sub_window_exists(
                EditLateralContributionAdisTSWindow,
                data=[self._study, None, data]
            ):
                continue

            win = EditLateralContributionAdisTSWindow(
                data=data,
                study=self._study,
                parent=self
            )
            win.show()
