# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.LateralContributionsAdisTS.LateralContributionAdisTS \
    import LateralContributionAdisTS
from Model.LateralContributionsAdisTS.LateralContributionsAdisTSList \
    import LateralContributionsAdisTSList


class SetBeginCommand(QUndoCommand):
    def __init__(self, lcs, lcs_lst, index, new_value):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._lcs_lst = lcs_lst
        self._index = index
        self._old = self._lcs_lst[self._index].begin_rk
        self._new = float(new_value)

    def undo(self):
        self._lcs_lst[self._index].begin_rk = float(self._old)

    def redo(self):
        self._lcs_lst[self._index].begin_rk = float(self._new)


class SetEndCommand(QUndoCommand):
    def __init__(self, lcs, lcs_lst, index, new_value):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._lcs_lst = lcs_lst
        self._index = index
        self._old = self._lcs_lst[self._index].end_rk
        self._new = float(new_value)

    def undo(self):
        self._lcs_lst[self._index].end_rk = float(self._old)

    def redo(self):
        self._lcs_lst[self._index].end_rk = float(self._new)


class SetEdgeCommand(QUndoCommand):
    def __init__(self, lcs, lcs_lst, index, edge):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._lcs_lst = lcs_lst
        self._index = index
        self._old = self._lcs_lst[self._index].edge
        self._new = edge

    def undo(self):
        self._lcs_lst[self._index].edge = self._old

    def redo(self):
        self._lcs_lst[self._index].edge = self._new


class AddCommand(QUndoCommand):
    def __init__(self, pollutant, lcs, lcs_lst, index):
        QUndoCommand.__init__(self)

        self._pollutant = pollutant
        self._lcs = lcs
        self._lcs_lst = lcs_lst
        self._index = index
        self._new = None

    def undo(self):
        del self._lcs[self._index]

    def redo(self):
        if self._new is None:
            self._new = self._lcs.new(self._index, self._pollutant)
        else:
            self._lcs_lst.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, lcs, rows):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._rows = rows

        self._bc = []
        for row in rows:
            self._bc.append((row, self._lcs[row]))
        self._bc.sort()

    def undo(self):
        for row, el in self._bc:
            self._lcs.insert(row, el)

    def redo(self):
        for row in self._rows:
            del self._lcs[row]
