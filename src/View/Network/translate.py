# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class NetworkTranslate(MainTranslate):
    def __init__(self):
        super(NetworkTranslate, self).__init__()

        self._dict["River network"] = _translate(
            "Network", "River network"
        )

        self._dict["node"] = _translate("Network", "Node")
        self._dict["edge"] = _translate("Network", "Reach")

        self._dict["menu_add_node"] = _translate("Network", "Add node")
        self._dict["menu_del_node"] = _translate("Network", "Delete the node")
        self._dict["menu_edit_res_node"] = _translate(
            "Network", "Edit node reservoir"
        )
        self._dict["menu_add_res_node"] = _translate(
            "Network", "Add node reservoir"
        )
        self._dict["menu_del_res_node"] = _translate(
            "Network", "Delete node reservoir"
        )

        self._dict["menu_del_edge"] = _translate("Network", "Delete the reach")
        self._dict["menu_ena_edge"] = _translate("Network", "Enable the reach")
        self._dict["menu_dis_edge"] = _translate("Network",
                                                 "Disable the reach")
        self._dict["menu_rev_edge"] = _translate(
            "Network", "Reverse the reach orientation"
        )

        self._sub_dict["table_headers_node"] = {
            "name": self._dict['name'],
            "type": self._dict['type'],
            "id": _translate("Network", "Index"),
        }

        self._sub_dict["table_headers_edge"] = {
            "name": self._dict['name'],
            "node1": _translate("Network", "Source node"),
            "node2": _translate("Network", "Destination node"),
            "id": _translate("Network", "Index"),
        }

    def node_name(self, node):
        if node.name == "":
            return f"{self['node']} #{node.id}"

        return node.name

    def edge_name(self, edge):
        if edge.name == "":
            return f"{self['edge']} #{edge.id}"

        return edge.name
