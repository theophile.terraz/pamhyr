# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtCore import (
    QPointF,
)

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Network.Node import Node
from Model.Network.Edge import Edge
from Model.Network.Graph import Graph


class AddNodeCommand(QUndoCommand):
    def __init__(self, graph, node):
        QUndoCommand.__init__(self)

        self._graph = graph
        self._node = node

    def undo(self):
        self._graph.remove_node(self._node)

    def redo(self):
        self._graph.insert_node(self._node)


class SetNodePosCommand(QUndoCommand):
    def __init__(self, node, new, old):
        QUndoCommand.__init__(self)

        self._node = node
        self._new = new
        self._old = old

    def undo(self):
        self._node.node.setPos(self._old[0], self._old[1])

    def redo(self):
        self._node.node.setPos(self._new[0], self._new[1])


class DelNodeCommand(QUndoCommand):
    def __init__(self, graph, node):
        QUndoCommand.__init__(self)

        self._graph = graph
        self._node = node
        self._edges = []

    def undo(self):
        for edge in self._edges:
            self._graph.insert_edge(edge)

        self._graph.insert_node(self._node)

    def redo(self):
        self._edges = list(
            filter(
                lambda e: (e.node1 == self._node or
                           e.node2 == self._node),
                self._graph.edges()
            )
        )

        for edge in self._edges:
            self._graph.remove_edge(edge)

        self._graph.remove_node(self._node)


class AddEdgeCommand(QUndoCommand):
    def __init__(self, graph, edge):
        QUndoCommand.__init__(self)

        self._graph = graph
        self._edge = edge

    def undo(self):
        self._graph.remove_edge(self._edge)

    def redo(self):
        self._graph.insert_edge(self._edge)


class DelEdgeCommand(QUndoCommand):
    def __init__(self, graph, edge):
        QUndoCommand.__init__(self)

        self._graph = graph
        self._edge = edge

    def undo(self):
        self._graph.insert_edge(self._edge)

    def redo(self):
        self._graph.remove_edge(self._edge)


class SetCommand(QUndoCommand):
    def __init__(self, element, column, new_value):
        QUndoCommand.__init__(self)

        self._el = element
        self._column = column
        self._old = self._el[self._column]
        self._new = new_value

    def undo(self):
        self._el[self._column] = self._old

    def redo(self):
        self._el[self._column] = self._new


class SetNodeCommand(QUndoCommand):
    def __init__(self, graph, element, column, new_value):
        QUndoCommand.__init__(self)

        self._el = element
        self._column = column
        self._old = graph.node(self._el[self._column])
        self._new = new_value

    def undo(self):
        self._el[self._column] = self._old

    def redo(self):
        self._el[self._column] = self._new


class EnableEdgeCommand(QUndoCommand):
    def __init__(self, edge, enable):
        QUndoCommand.__init__(self)

        self._edge = edge
        self._old = self._edge._enable
        self._enable = enable

    def undo(self):
        self._edge.enable(enable=self._old)

    def redo(self):
        self._edge.enable(enable=self._enable)


class ReverseEdgeCommand(QUndoCommand):
    def __init__(self, edge):
        QUndoCommand.__init__(self)

        self._edge = edge

    def undo(self):
        self._edge.reverse()

    def redo(self):
        self._edge.reverse()
