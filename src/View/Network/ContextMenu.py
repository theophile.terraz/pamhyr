# ContextMenu.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import (
    QMenu,
)


class AbstractMenu(object):
    def __init__(self, event=None, pos=None, items=[],
                 graph=None, trad=None, parent=None):
        super(AbstractMenu, self).__init__()

        self._menu = QMenu(parent)
        self._event = event
        self._pos = pos
        self._items = items

        self._graph = graph
        self._trad = trad
        self._parent = parent

    def map_to_global(self):
        return self._parent.mapToGlobal(self._event.pos())

    def _exec(self):
        return self._menu.exec_(self.map_to_global())

    def run(self):
        return


class DefaultMenu(AbstractMenu):
    def run(self):
        add_node = self._menu.addAction(self._trad["menu_add_node"])

        action = self._exec()
        if action == add_node:
            self._parent.add_node(self._pos)


class NodeMenu(AbstractMenu):
    def run(self):
        item = self._items[0]
        node = item.node

        delete = self._menu.addAction(self._trad["menu_del_node"])

        is_internal = self._graph.is_internal_node(node)
        if is_internal:
            res = self._graph.reservoir.get_assoc_to_node(node)
            if res is not None:
                edt_res = self._menu.addAction(
                    self._trad["menu_edit_res_node"]
                )
                del_res = self._menu.addAction(
                    self._trad["menu_del_res_node"]
                )
                res_edit = True
            else:
                add_res = self._menu.addAction(self._trad["menu_add_res_node"])
                res_edit = False

        action = self._exec()
        if action == delete:
            self._parent.del_node(item)
        elif is_internal:
            if res_edit:
                if action == edt_res:
                    self._parent.edit_node_reservoir(node)
                elif action == del_res:
                    self._parent.del_node_reservoir(node)
            else:
                if action == add_res:
                    self._parent.add_node_reservoir(node)


class EdgeMenu(AbstractMenu):
    def run(self):
        it = self._items[0]

        reverse = self._menu.addAction(self._trad["menu_rev_edge"])
        delete = self._menu.addAction(self._trad["menu_del_edge"])
        if self._parent.graph.is_enable_edge(it.edge):
            enable = self._menu.addAction(self._trad["menu_dis_edge"])
            is_enable = True
        else:
            enable = self._menu.addAction(self._trad["menu_ena_edge"])
            is_enable = False

        action = self._exec()
        if action == delete:
            self._parent.del_edge(it)
        elif action == enable:
            self._parent.enable_edge(it, is_enable)
        elif action == reverse:
            self._parent.reverse_edge(it)
