# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from PyQt5.QtCore import QCoreApplication

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QRect, QVariant, QAbstractTableModel, pyqtSlot, pyqtSignal,
    QEvent,
)

from PyQt5.QtWidgets import (
    QTableView, QItemDelegate, QComboBox, QLineEdit, QHBoxLayout, QSlider,
    QPushButton, QCheckBox, QStyledItemDelegate, QStyleOptionButton, QStyle,
    QApplication, QToolBar, QAction, QHeaderView, QAbstractItemView,
    QUndoStack, QShortcut,
)

from Modules import Modules
from Model.River import RiverNode, RiverReach, River

from View.Tools.PamhyrWindow import PamhyrWindow
from View.Network.GraphWidget import GraphWidget
from View.Network.UndoCommand import *
from View.Network.translate import NetworkTranslate
from View.Network.Table import (
    ComboBoxDelegate, NodeTableModel, EdgeTableModel,
)

# Reservoir short cut
from View.Reservoir.Edit.Window import EditReservoirWindow
import View.Reservoir.UndoCommand as ResUndoCommand


logger = logging.getLogger()

_translate = QCoreApplication.translate


class NetworkWindow(PamhyrWindow):
    _pamhyr_ui = "Network"
    _pamhyr_name = "River network"

    def __init__(self, study=None, config=None, parent=None):
        trad = NetworkTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(NetworkWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            options=['undo'],
            parent=parent,
        )

        self._graph = study.river
        self._reservoir = study.river.reservoir
        self._table_headers_node = self._trad.get_dict("table_headers_node")
        self._table_headers_edge = self._trad.get_dict("table_headers_edge")

        self.setup_graph()
        self.setup_table()
        self.setup_connections()

    def setup_table(self):
        # Nodes table
        table = self.find(QTableView, "tableView_nodes")
        self._nodes_model = NodeTableModel(
            table_view=table,
            table_headers=self._table_headers_node,
            editable_headers=["name"],
            trad=self._trad,
            data=self._graph,
            undo=self._undo_stack,
        )
        table.setModel(self._nodes_model)

        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        # Edges table
        table = self.find(QTableView, "tableView_reachs")
        self.delegate_combobox = ComboBoxDelegate(
            graph=self._graph,
            parent=self,
        )

        self._reachs_model = EdgeTableModel(
            table_view=table,
            table_headers=self._table_headers_edge,
            editable_headers=["name", "node1", "node2"],
            trad=self._trad,
            delegates={
                "node1": self.delegate_combobox,
                "node2": self.delegate_combobox,
            },
            data=self._graph,
            undo=self._undo_stack,
        )
        table.setModel(self._reachs_model)
        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        # table.resizeColumnsToContents()

    def setup_graph(self):
        self._graph_widget = GraphWidget(
            self._graph, parent=self,
            undo=self._undo_stack,
            trad=self._trad,
        )
        self._graph_layout = self.find(QHBoxLayout, "horizontalLayout_graph")
        self._graph_layout.addWidget(self._graph_widget)

    def setup_connections(self):
        self._nodes_model.dataChanged.connect(self.update)
        self._nodes_model.dataChanged.connect(self._graph_widget.rename_nodes)
        self._reachs_model.dataChanged.connect(
            self._graph_widget.display_update)
        self._reachs_model.dataChanged.connect(self.update)
        self._graph_widget.changeEdge.connect(self.update)
        self._graph_widget.changeNode.connect(self.update)

        self.find(QAction, "action_toolBar_add").setCheckable(True)
        self.find(QAction, "action_toolBar_add").triggered.connect(
            self.clicked_add
        )

        self.find(QAction, "action_toolBar_del").setCheckable(True)
        self.find(QAction, "action_toolBar_del").triggered.connect(
            self.clicked_del
        )

    def clicked_add(self):
        if self.get_action_checkable("action_toolBar_add"):
            self.set_action_checkable("action_toolBar_del", False)
            self._graph_widget.state("add")
        else:
            self._graph_widget.state("move")

    def clicked_del(self):
        if self.get_action_checkable("action_toolBar_del"):
            self.set_action_checkable("action_toolBar_add", False)
            self._graph_widget.state("del")
        else:
            self._graph_widget.state("move")

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_Escape:
            self._graph_widget.reset_selection

    def add_node_reservoir(self, node):
        if self._reservoir.get_assoc_to_node(node) is None:
            self._undo_stack.push(
                ResUndoCommand.AddAndAssociateCommand(
                    self._reservoir, 0, node
                )
            )

    def del_node_reservoir(self, node):
        res = self._reservoir.get_assoc_to_node(node)
        ind = self._reservoir.index(res)

        self._undo_stack.push(
            ResUndoCommand.DelCommand(
                self._reservoir, [ind]
            )
        )

    def edit_node_reservoir(self, node):
        data = self._reservoir.get_assoc_to_node(node)

        if self.sub_window_exists(
            EditReservoirWindow,
            data=[self._study, None, data]
        ):
            return

        win = EditReservoirWindow(
            data=data,
            study=self._study,
            parent=self
        )
        win.show()

    def _undo(self):
        self._undo_stack.undo()
        self.update()

    def _redo(self):
        self._undo_stack.redo()
        self.update()

    def update(self):
        self._reachs_model.update()
        self._nodes_model.update()
        self._graph_widget.display_update()

        self._propagate_update(key=Modules.NETWORK)
