# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class ParamTranslate(MainTranslate):
    def __init__(self):
        super(ParamTranslate, self).__init__()

        self._dict["Solver parameters"] = _translate(
            "SolverParameters", "Solver parameters"
        )

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "value": self._dict["value"],
        }

        self._sub_dict["yes_no"] = {
            "yes": _translate("SolverParameters", "Yes"),
            "no": _translate("SolverParameters", "No"),
            "y": _translate("SolverParameters", "Y"),
            "n": _translate("SolverParameters", "N"),
        }

        self._sub_dict["r_yes_no"] = {
            _translate("SolverParameters", "Yes"): "yes",
            _translate("SolverParameters", "No"): "no",
            _translate("SolverParameters", "Y"): "y",
            _translate("SolverParameters", "N"): "n",
        }

        self._sub_dict["names"] = {
            "all_init_time":
            _translate("SolverParameters",
                       "Initial time (jj:hh:mm:ss)"),
            "all_final_time":
            _translate("SolverParameters",
                       "Final time (jj:hh:mm:ss)"),
            "all_timestep":
            _translate("SolverParameters",
                       "Timestep (second)"),
            "all_command_line_arguments":
            _translate("SolverParameters",
                       "Command line arguments"),
            # Mage specific parameters
            "mage_min_timestep":
            _translate("SolverParameters",
                       "Minimum timestep (second)"),
            "mage_timestep_tra":
            _translate("SolverParameters",
                       "Time step of writing on .TRA"),
            "mage_timestep_bin":
            _translate("SolverParameters",
                       "Time step of writing on .BIN"),
            "mage_implicitation":
            _translate("SolverParameters",
                       "Implicitation parameter"),
            "mage_continuity_discretization":
            _translate("SolverParameters",
                       "Continuity discretization type (S/L)"),
            "mage_qsj_discretization":
            _translate("SolverParameters",
                       "QSJ discretization (A/B)"),
            "mage_stop_criterion_iterations":
            _translate("SolverParameters",
                       "Stop criterion iterations (G/A/R)"),
            "mage_iteration_type":
            _translate("SolverParameters",
                       "Iteration type"),
            "mage_smooth_coef":
            _translate("SolverParameters",
                       "Smoothing coefficient"),
            "mage_cfl_max":
            _translate("SolverParameters",
                       "Maximun accepted number of CFL"),
            "mage_min_height":
            _translate("SolverParameters",
                       "Minimum water height (meter)"),
            "mage_max_niter":
            _translate("SolverParameters",
                       "Maximum number of iterations (< 100)"),
            "mage_timestep_reduction_factor":
            _translate("SolverParameters",
                       "Timestep reduction factor"),
            "mage_precision_reduction_factor_Z":
            _translate("SolverParameters",
                       "Reduction precision factor of Z"),
            "mage_precision_reduction_factor_Q":
            _translate("SolverParameters",
                       "Reduction precision factor of Q"),
            "mage_precision_reduction_factor_r":
            _translate("SolverParameters",
                       "Reduction precision factor of residue"),
            "mage_niter_max_precision":
            _translate("SolverParameters",
                       "Number of iterations at maximum precision"),
            "mage_niter_before_switch":
            _translate("SolverParameters",
                       "Number of iterations before switch"),
            "mage_max_froude":
            _translate("SolverParameters",
                       "Maximum accepted Froude number"),
            "mage_diffluence_node_height_balance":
            _translate("SolverParameters",
                       "Diffluence node height balance"),
            "mage_compute_reach_volume_balance":
            _translate("SolverParameters",
                       "Compute reach volume balance (Y/N)"),
            "mage_max_reach_volume_balance":
            _translate("SolverParameters",
                       "Maximum reach volume balance"),
            "mage_min_reach_volume_to_check":
            _translate("SolverParameters",
                       "Minimum reach volume to check"),
            "mage_init_internal":
            _translate("SolverParameters",
                       "Use Mage internal initialization (Y/N)"),
            # Sediment
            "mage_sediment_masse_volumique":
            _translate("SolverParameters", "Sediment density"),
            "mage_sediment_angle_repos":
            _translate("SolverParameters", "Sediment rest angle"),
            "mage_sediment_porosity":
            _translate("SolverParameters", "Sediment porosity"),
            "mage_distance_Han":
            _translate("SolverParameters", "Han distance"),
            "mage_distance_chargement_d50":
            _translate("SolverParameters", "D50 changing distance"),
            "mage_distance_chargement_sigma":
            _translate("SolverParameters", "Sigma changing distance"),
            "mage_methode_modification_geometrie":
            _translate("SolverParameters", "Geometry modification method"),
            "mage_shields_critique":
            _translate("SolverParameters", "Critical Shields number"),
            "mage_shields_correction":
            _translate("SolverParameters", "Shields correction"),
            "mage_capacite_solide":
            _translate("SolverParameters", "Solid capacity"),
            "mage_pas_de_temps_charriage":
            _translate("SolverParameters", "Bed load time step"),
            "mage_facteur_multiplicateur":
            _translate("SolverParameters", "Multiplicator factor"),
        }
