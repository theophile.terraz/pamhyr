# Window.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from Modules import Modules
from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtWidgets import (
    QLabel, QPlainTextEdit, QPushButton,
    QCheckBox,
)

from View.REPLines.Translate import REPLineTranslate
from View.REPLines.UndoCommand import (
    SetCommand
)

logger = logging.getLogger()


class EditREPLineWindow(PamhyrDialog):
    _pamhyr_ui = "REPLineDialog"
    _pamhyr_name = "Edit Mage REP lines"

    def __init__(self, study=None, config=None, rep_line=None,
                 trad=None, undo=None, parent=None):

        name = trad[self._pamhyr_name] + " - " + study.name
        super(EditREPLineWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            options=[],
            parent=parent
        )

        self._rep_line = rep_line
        self._hash_data.append(self._rep_line)

        self._undo = undo

        self.setup_values()

    def setup_values(self):
        self.set_check_box("checkBox_enabled", self._rep_line.enabled)
        self.set_line_edit_text("lineEdit_name", self._rep_line.name)
        self.set_line_edit_text("lineEdit_line", self._rep_line.line)

    def accept(self):
        is_enabled = self.get_check_box("checkBox_enabled")
        name = self.get_line_edit_text("lineEdit_name")
        line = self.get_line_edit_text("lineEdit_line")
        solvers = set()

        self._undo.push(
            SetCommand(
                self._rep_line,
                enabled=is_enabled,
                name=name,
                line=line,
                solvers=solvers,
            )
        )

        self._propagate_update(key=Modules.ADDITIONAL_FILES)
        self.close()
