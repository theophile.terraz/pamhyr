# Window.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import trace, timer

from PyQt5.QtWidgets import (
    QAction, QListView,
)

from View.Tools.PamhyrWindow import PamhyrWindow

from View.REPLines.List import ListModel
from View.REPLines.Translate import REPLineTranslate
from View.REPLines.Edit.Window import EditREPLineWindow


class REPLineListWindow(PamhyrWindow):
    _pamhyr_ui = "REPLineList"
    _pamhyr_name = "Mage REP lines"

    def __init__(self, study=None, config=None,
                 parent=None):
        trad = REPLineTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(REPLineListWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            options=[],
            parent=parent
        )

        self.setup_list()
        self.setup_connections()

    def setup_list(self):
        lst = self.find(QListView, f"listView")
        self._list = ListModel(
            list_view=lst,
            data=self._study.river.rep_lines,
            undo=self._undo_stack,
        )

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)

    def update(self):
        self._list.update()

    def selected_rows(self):
        lst = self.find(QListView, f"listView")
        return list(map(lambda i: i.row(), lst.selectedIndexes()))

    def add(self):
        rows = self.selected_rows()
        if len(rows) > 0:
            row = rows[0]
        else:
            row = 0

        self._list.add(row)

    def delete(self):
        rows = self.selected_rows()
        if len(rows) < 0:
            return

        self._list.delete(rows[0])

    def edit(self):
        rows = self.selected_rows()

        for row in rows:
            rep_line = self._study.river.rep_lines.lines[row]

            if self.sub_window_exists(
                    EditREPLineWindow,
                    data=[self._study, self._config, rep_line]
            ):
                continue

            win = EditREPLineWindow(
                study=self._study,
                config=self._config,
                rep_line=rep_line,
                trad=self._trad,
                undo=self._undo_stack,
                parent=self,
            )
            win.show()

    def _undo(self):
        self._undo_stack.undo()
        self.update()

    def _redo(self):
        self._undo_stack.redo()
        self.update()
