# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class BCAdisTSTranslate(MainTranslate):
    def __init__(self):
        super(BCAdisTSTranslate, self).__init__()

        self._dict["Boundary conditions AdisTS"] = _translate(
            "BoundaryConditionsAdisTS", "Boundary conditions AdisTS"
        )

        self._sub_dict["table_headers"] = {
            "type": self._dict["type"],
            "node": _translate("BoundaryCondition", "Node"),
            "pol": _translate("BoundaryCondition", "Pollutant")
        }
