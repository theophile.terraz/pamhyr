# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer, logger_exception

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
    QWidget,
)

from View.BoundaryConditionsAdisTS.Table import (
    TableModel, ComboBoxDelegate
)

from View.Network.GraphWidget import GraphWidget
from View.BoundaryConditionsAdisTS.translate import BCAdisTSTranslate
from View.BoundaryConditionsAdisTS.Edit.Window \
    import EditBoundaryConditionWindow

_translate = QCoreApplication.translate

logger = logging.getLogger()


class BoundaryConditionAdisTSWindow(PamhyrWindow):
    _pamhyr_ui = "BoundaryConditionsAdisTS"
    _pamhyr_name = "Boundary conditions AdisTS"

    def __init__(self, study=None, config=None, parent=None):
        trad = BCAdisTSTranslate()
        name = (
            trad[self._pamhyr_name] +
            " - " + study.name
        )

        super(BoundaryConditionAdisTSWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._pollutants_lst = self._study._river._Pollutants
        self._bcs = self._study.river.boundary_conditions_adists

        self.setup_graph()
        self.setup_table()
        self.setup_connections()

        self.ui.setWindowTitle(self._title)

    def setup_table(self):
        self._delegate_type = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            mode="type",
            parent=self
        )
        self._delegate_node = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            mode="node",
            parent=self
        )
        self._delegate_pol = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            mode="pol",
            parent=self
        )

        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=["type", "node", "pol"],
            delegates={
                "type": self._delegate_type,
                "node": self._delegate_node,
                "pol": self._delegate_pol,
            },
            trad=self._trad,
            bc_list=self._study.river.boundary_conditions_adists,
            undo=self._undo_stack,
            data=self._study.river
        )
        table.setModel(self._table)
        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        table.setAlternatingRowColors(True)

    def setup_graph(self):
        self.graph_widget = GraphWidget(
            self._study.river,
            min_size=None, size=(200, 200),
            only_display=True,
            parent=self
        )
        self.graph_layout = self.find(QVBoxLayout, "verticalLayout")
        self.graph_layout.addWidget(self.graph_widget)

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_del").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)

    def index_selected_row(self):
        table = self.find(QTableView, f"tableView")
        return table.selectionModel()\
                    .selectedRows()[0]\
                    .row()

    def index_selected_rows(self):
        table = self.find(QTableView, f"tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        self._table.add(len(self._bcs))

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()

    def edit(self):
        rows = self.index_selected_rows()
        for row in rows:
            data = self._bcs.lst[row]

            if self.sub_window_exists(
                EditBoundaryConditionWindow,
                data=[self._study, None, data]
            ):
                continue

            win = EditBoundaryConditionWindow(
                data=data,
                study=self._study,
                parent=self
            )
            win.show()
