# Table.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging
import traceback

from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox,
)

from Model.BoundaryCondition.BoundaryConditionTypes import (
    NotDefined, PonctualContribution,
    TimeOverZ, TimeOverDischarge, ZOverDischarge
)

from View.Tools.PamhyrTable import PamhyrTableModel

from View.BoundaryConditionsAdisTS.UndoCommand import (
    SetNodeCommand, SetTypeCommand,
    AddCommand, DelCommand, SetPolCommand
)
from View.BoundaryCondition.translate import BC_types

logger = logging.getLogger()

_translate = QCoreApplication.translate


class ComboBoxDelegate(QItemDelegate):
    def __init__(self, data=None, mode="type", tab="",
                 trad=None, parent=None):
        super(ComboBoxDelegate, self).__init__(parent)

        self._data = data
        self._mode = mode
        self._tab = tab
        self._trad = trad

        self._long_types = {}
        if self._trad is not None:
            self._long_types = self._trad.get_dict("long_types")

    def createEditor(self, parent, option, index):
        self.editor = QComboBox(parent)

        if self._mode == "type":
            lst = [self._trad["not_associated"], "Concentration", "Mass"]
            self.editor.addItems(
                lst
            )
        elif self._mode == "node":
            self.editor.addItems(
                [self._trad["not_associated"]] +
                self._data.nodes_names()
            )
        elif self._mode == "pol":
            lst = [p.name for p in self._data._Pollutants.Pollutants_List]
            self.editor.addItems(
                [self._trad["not_associated"]] +
                lst
            )

        self.editor.setCurrentText(index.data(Qt.DisplayRole))
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(Qt.DisplayRole)
        self.editor.currentTextChanged.connect(self.currentItemChanged)

    def setModelData(self, editor, model, index):
        text = str(editor.currentText())
        model.setData(index, text)
        editor.close()
        editor.deleteLater()

    def updateEditorGeometry(self, editor, option, index):
        r = QRect(option.rect)
        if self.editor.windowFlags() & Qt.Popup:
            if editor.parent() is not None:
                r.setTopLeft(self.editor.parent().mapToGlobal(r.topLeft()))
        editor.setGeometry(r)

    @pyqtSlot()
    def currentItemChanged(self):
        self.commitData.emit(self.sender())


class TableModel(PamhyrTableModel):
    def __init__(self, pollutant=None, bc_list=None, trad=None, **kwargs):
        self._trad = trad
        self._bc_list = bc_list
        self._pollutant = pollutant

        super(TableModel, self).__init__(trad=trad, **kwargs)

    def _setup_lst(self):
        self._lst = self._bc_list.lst

    def rowCount(self, parent):
        return len(self._lst)

    def data(self, index, role):
        if len(self._lst) != 0:
            data = self._lst
        else:
            data = []

        if role != Qt.ItemDataRole.DisplayRole:
            return QVariant()

        row = index.row()
        column = index.column()

        if self._headers[column] == "type":
            n = data[row].type
            if n is None or n == "":
                return self._trad["not_associated"]
            return n
        elif self._headers[column] == "node":
            n = data[row].node
            if n is None:
                return self._trad["not_associated"]
            tmp = next(filter(lambda x: x.id == n, self._data._nodes), None)
            if tmp is not None:
                return tmp.name
            else:
                return self._trad["not_associated"]
        elif self._headers[column] == "pol":
            n = data[row].pollutant
            if n is None or n == "not_associated" or n == "":
                return self._trad["not_associated"]
            tmp = next(filter(lambda x: x.id == n,
                              self._data._Pollutants.Pollutants_List
                              ),
                       None)
            if tmp is not None:
                return tmp.name
            else:
                return self._trad["not_associated"]

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or role != Qt.EditRole:
            return False

        row = index.row()
        column = index.column()

        try:
            if self._headers[column] == "type":
                self._undo.push(
                    SetTypeCommand(
                        self._lst, row, value
                    )
                )
            elif self._headers[column] == "node":
                self._undo.push(
                    SetNodeCommand(
                        self._lst, row, self._data.node(value)
                    )
                )
            elif self._headers[column] == "pol":
                if value == self._trad["not_associated"]:
                    self._undo.push(
                        SetPolCommand(
                            self._lst, row, None
                        )
                    )
                else:
                    pol = next(filter(lambda x: x.name == value,
                                      self._data._Pollutants.Pollutants_List)
                               )
                    self._undo.push(
                        SetPolCommand(
                            self._lst, row, pol.id
                        )
                    )
        except Exception as e:
            logger.info(e)
            logger.debug(traceback.format_exc())

        self.dataChanged.emit(index, index)
        return True

    def add(self, row, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row - 1)

        self._undo.push(
            AddCommand(
                self._pollutant, self._bc_list, self._lst, row
            )
        )

        self.endInsertRows()
        self.layoutChanged.emit()

    def delete(self, rows, parent=QModelIndex()):
        self.beginRemoveRows(parent, rows[0], rows[-1])

        self._undo.push(
            DelCommand(
                self._lst, rows
            )
        )

        self.endRemoveRows()
        self.layoutChanged.emit()

    def undo(self):
        self._undo.undo()
        self.layoutChanged.emit()

    def redo(self):
        self._undo.redo()
        self.layoutChanged.emit()
