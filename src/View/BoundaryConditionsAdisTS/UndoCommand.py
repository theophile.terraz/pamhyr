# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.BoundaryConditionsAdisTS.BoundaryConditionAdisTS \
    import BoundaryConditionAdisTS
from Model.BoundaryConditionsAdisTS.BoundaryConditionsAdisTSList \
    import BoundaryConditionsAdisTSList


class SetNodeCommand(QUndoCommand):
    def __init__(self, bcs, index, node):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._index = index
        self._old = self._bcs[self._index].node
        self._new = node.id

    def undo(self):
        self._bcs[self._index].node = self._old

    def redo(self):
        self._bcs[self._index].node = self._new


class SetTypeCommand(QUndoCommand):
    def __init__(self, bcs, index, _type):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._index = index
        self._type = _type
        self._old = self._bcs[self._index].type
        self._new = self._type

    def undo(self):
        self._bcs[self._index].type = self._old

    def redo(self):
        self._bcs[self._index].type = self._new


class SetPolCommand(QUndoCommand):
    def __init__(self, bcs, index, pollutant):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._index = index
        self._old = self._bcs[self._index].pollutant
        self._new = pollutant

    def undo(self):
        self._bcs[self._index].pollutant = self._old

    def redo(self):
        self._bcs[self._index].pollutant = self._new


class AddCommand(QUndoCommand):
    def __init__(self, pollutant, bcs_list, bcs, index):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._bc_list = bcs_list
        self._pollutant = pollutant
        self._index = index
        self._new = None

    def undo(self):
        del self._bcs[self._index]

    def redo(self):
        if self._new is None:
            self._new = self._bc_list.new(self._index, self._pollutant)
        else:
            self._bcs.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, bcs, rows):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._rows = rows

        self._bc = []
        for row in rows:
            self._bc.append((row, self._bcs[row]))
        self._bc.sort()

    def undo(self):
        for row, el in self._bc:
            self._bcs.insert(row, el)

    def redo(self):
        for row in self._rows:
            del self._bcs[row]
