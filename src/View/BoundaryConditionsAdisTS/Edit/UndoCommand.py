# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.BoundaryConditionsAdisTS.BoundaryConditionAdisTS \
    import BoundaryConditionAdisTS

logger = logging.getLogger()


class SetDataCommand(QUndoCommand):
    def __init__(self, data, index, column, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._column = column
        self._old = self._data._data[self._index][self._column]
        _type = self._data._types[self._column]
        self._new = _type(new_value)

    def undo(self):
        if self._column == 0:
            self._data._data[self._index] = (
                self._old, self._data._data[self._index][1]
            )
        else:
            self._data._data[self._index] = (
                self._data._data[self._index][0], self._old
            )

    def redo(self):
        if self._column == 0:
            self._data._data[self._index] = (
                self._new, self._data._data[self._index][1]
            )
        else:
            self._data._data[self._index] = (
                self._data._data[self._index][0], self._new
            )


class AddCommand(QUndoCommand):
    def __init__(self, data, index):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._new = None

    def undo(self):
        del self._data._data[self._index]

    def redo(self):
        if self._new is None:
            self._new = self._data._data.insert(self._index, (
                self._data._types[0](0), self._data._types[1](0.0)
            ))
        else:
            self._data._data.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, data, rows):
        QUndoCommand.__init__(self)

        self._data = data
        self._rows = rows

        self._bc = []
        for row in rows:
            self._bc.append((row, self._data._data[row]))
        self._bc.sort()

    def undo(self):
        for row, el in self._bc:
            self._data._data.insert(row, el)

    def redo(self):
        for row in self._rows:
            del self._data._data[row]
