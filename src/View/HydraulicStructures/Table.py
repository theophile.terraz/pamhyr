# Table.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging
import traceback

from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox,
)

from View.Tools.PamhyrTable import PamhyrTableModel

from View.HydraulicStructures.UndoCommand import (
    SetNameCommand, SetReachCommand, SetRkCommand,
    SetEnabledCommand, AddCommand, DelCommand,
)

logger = logging.getLogger()

_translate = QCoreApplication.translate


class ComboBoxDelegate(QItemDelegate):
    def __init__(self, data=None, trad=None, parent=None, mode="reaches"):
        super(ComboBoxDelegate, self).__init__(parent)

        self._data = data
        self._trad = trad
        self._mode = mode

    def createEditor(self, parent, option, index):
        self.editor = QComboBox(parent)

        val = []
        if self._mode == "rk":
            reach = self._data.hydraulic_structures\
                              .get(index.row())\
                              .input_reach
            if reach is not None:
                val = list(
                    map(
                        lambda p: p.display_name(),
                        reach.reach.profiles
                    )
                )
        else:
            val = list(
                map(
                    lambda n: n.name, self._data.edges()
                )
            )

        self.editor.addItems(
            [self._trad['not_associated']] +
            val
        )

        self.editor.setCurrentText(str(index.data(Qt.DisplayRole)))
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(Qt.DisplayRole)
        self.editor.currentTextChanged.connect(self.currentItemChanged)

    def setModelData(self, editor, model, index):
        text = str(editor.currentText())

        if self._mode == "rk":
            reach = self._data.hydraulic_structures\
                              .get(index.row())\
                              .input_reach
            profiles = list(
                filter(
                    lambda p: p.display_name() == text,
                    reach.reach.profiles
                )
            )

            value = profiles[0].rk if len(profiles) > 0 else None
        else:
            value = text

        model.setData(index, value)
        editor.close()
        editor.deleteLater()

    def updateEditorGeometry(self, editor, option, index):
        r = QRect(option.rect)
        if self.editor.windowFlags() & Qt.Popup:
            if editor.parent() is not None:
                r.setTopLeft(self.editor.parent().mapToGlobal(r.topLeft()))
        editor.setGeometry(r)

    @pyqtSlot()
    def currentItemChanged(self):
        self.commitData.emit(self.sender())


class TableModel(PamhyrTableModel):
    def _setup_lst(self):
        self._lst = self._data._hydraulic_structures

    def rowCount(self, parent):
        return len(self._lst)

    def data(self, index, role):
        if role != Qt.ItemDataRole.DisplayRole:
            return QVariant()

        row = index.row()
        column = index.column()

        if self._headers[column] == "name":
            return self._lst.get(row).name
        elif self._headers[column] == "reach":
            n = self._lst.get(row).input_reach
            if n is None:
                return self._trad['not_associated']
            return n.name
        elif self._headers[column] == "rk":
            n = self._lst.get(row).input_rk
            if n is None:
                return self._trad['not_associated']
            return n

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or role != Qt.EditRole:
            return False

        row = index.row()
        column = index.column()
        na = self._trad['not_associated']

        try:
            if self._headers[column] == "name":
                self._undo.push(
                    SetNameCommand(
                        self._lst, row, value
                    )
                )
            elif self._headers[column] == "reach":
                if value == na:
                    value = None

                self._undo.push(
                    SetReachCommand(
                        self._lst, row, self._data.edge(value)
                    )
                )
            elif self._headers[column] == "rk":
                if value == na:
                    value = None

                self._undo.push(
                    SetRkCommand(
                        self._lst, row, value
                    )
                )
        except Exception as e:
            logger.info(e)
            logger.debug(traceback.format_exc())

        self.dataChanged.emit(index, index)
        return True

    def add(self, row, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row - 1)

        self._undo.push(
            AddCommand(
                self._lst, row
            )
        )

        self.endInsertRows()
        self.layoutChanged.emit()

    def delete(self, rows, parent=QModelIndex()):
        self.beginRemoveRows(parent, rows[0], rows[-1])

        self._undo.push(
            DelCommand(
                self._lst, rows
            )
        )

        self.endRemoveRows()
        self.layoutChanged.emit()

    def enabled(self, row, enabled, parent=QModelIndex()):
        self._undo.push(
            SetEnabledCommand(
                self._lst, row, enabled
            )
        )
        self.layoutChanged.emit()

    def undo(self):
        self._undo.undo()
        self.layoutChanged.emit()

    def redo(self):
        self._undo.redo()
        self.layoutChanged.emit()
