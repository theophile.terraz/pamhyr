# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class BasicHydraulicStructuresTranslate(MainTranslate):
    def __init__(self):
        super(BasicHydraulicStructuresTranslate, self).__init__()

        self._dict["Basic Hydraulic Structures"] = _translate(
            "BasicHydraulicStructures", "Basic Hydraulic Structures"
        )

        self._dict['msg_type_change_title'] = _translate(
            "BasicHydraulicStructures",
            "Change hydraulic structure type"
        )

        self._dict['msg_type_change_text'] = _translate(
            "BasicHydraulicStructures",
            "Do you want to change the hydraulic structure type and reset \
hydraulic structure values?"
        )

        # BHSValues translation

        self._dict['width'] = self._dict["unit_width"]
        self._dict['height'] = self._dict["unit_thickness"]
        self._dict['elevation'] = self._dict["unit_elevation"]
        self._dict['diameter'] = self._dict["unit_diameter"]
        self._dict['discharge_coefficient'] = _translate(
            "BasicHydraulicStructures", "Discharge coefficient"
        )
        self._dict['loading_elevation'] = _translate(
            "BasicHydraulicStructures", "Upper elevation (m)"
        )
        self._dict['half-angle_tangent'] = _translate(
            "BasicHydraulicStructures", "Half-angle tangent"
        )
        self._dict['maximal_loading_elevation'] = _translate(
            "BasicHydraulicStructures", "Maximal loading elevation"
        )
        self._dict['siltation_height'] = _translate(
            "BasicHydraulicStructures", "Siltation height (m)"
        )
        self._dict['top_of_the_vault'] = _translate(
            "BasicHydraulicStructures", "Top of the vault (m)"
        )
        self._dict['bottom_of_the_vault'] = _translate(
            "BasicHydraulicStructures", "Bottom of the vault (m)"
        )
        self._dict['opening'] = _translate(
            "BasicHydraulicStructures", "Opening"
        )
        self._dict['maximal_opening'] = _translate(
            "BasicHydraulicStructures", "Maximal opening"
        )
        self._dict['step_space'] = _translate(
            "BasicHydraulicStructures", "Step space"
        )
        self._dict['weir'] = _translate(
            "BasicHydraulicStructures", "Weir"
        )
        self._dict['coefficient'] = _translate(
            "BasicHydraulicStructures", "Coefficient"
        )

        # Dummy parameters

        self._dict['parameter_1'] = _translate(
            "BasicHydraulicStructures", "Parameter 1"
        )
        self._dict['parameter_2'] = _translate(
            "BasicHydraulicStructures", "Parameter 2"
        )
        self._dict['parameter_3'] = _translate(
            "BasicHydraulicStructures", "Parameter 3"
        )
        self._dict['parameter_4'] = _translate(
            "BasicHydraulicStructures", "Parameter 4"
        )
        self._dict['parameter_5'] = _translate(
            "BasicHydraulicStructures", "Parameter 5"
        )

        # BHS types long names

        self._sub_dict["long_types"] = {
            "ND": self._dict["not_defined"],
            "S1": _translate(
                "BasicHydraulicStructures", "Rectangular weir"
            ),
            "S2": _translate(
                "BasicHydraulicStructures", "Trapezoidal weir"
            ),
            "S3": _translate(
                "BasicHydraulicStructures", "Triangular weir"
            ),
            "OR": _translate(
                "BasicHydraulicStructures", "Rectangular orifice"
            ),
            "OC": _translate(
                "BasicHydraulicStructures", "Circular orifice"
            ),
            "OV": _translate(
                "BasicHydraulicStructures", "Vaulted orifice"
            ),
            "V1": _translate(
                "BasicHydraulicStructures", "Rectangular gate"
            ),
            "V2": _translate(
                "BasicHydraulicStructures", "Simplified rectangular gate"
            ),
            "BO": _translate(
                "BasicHydraulicStructures", "Borda-type head loss"
            ),
            "CV": _translate(
                "BasicHydraulicStructures", "Check valve"
            ),
            "UD": _translate(
                "BasicHydraulicStructures", "User-defined"
            ),
        }

        # Tables

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "type": self._dict["type"],
        }

        self._sub_dict["table_headers_parameters"] = {
            "name": self._dict["name"],
            "value": self._dict["value"],
        }
