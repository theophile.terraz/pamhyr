# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)


class SetNameCommand(QUndoCommand):
    def __init__(self, hs, index, new_value):
        QUndoCommand.__init__(self)

        self._hs = hs
        self._index = index
        self._old = self._hs.basic_structure(self._index).name
        self._new = str(new_value)

    def undo(self):
        self._hs.basic_structure(self._index).name = self._old

    def redo(self):
        self._hs.basic_structure(self._index).name = self._new


class SetTypeCommand(QUndoCommand):
    def __init__(self, hs, index, new_type):
        QUndoCommand.__init__(self)

        self._hs = hs
        self._index = index
        self._type = new_type
        self._old = self._hs.basic_structure(self._index)
        self._new = self._hs.basic_structure(self._index)\
            .convert(self._type)

    def undo(self):
        self._hs.delete_i([self._index])
        self._hs.insert(self._index, self._old)

    def redo(self):
        self._hs.delete_i([self._index])
        self._hs.insert(self._index, self._new)


class SetEnabledCommand(QUndoCommand):
    def __init__(self, hs, index, enabled):
        QUndoCommand.__init__(self)

        self._hs = hs
        self._index = index
        self._old = not enabled
        self._new = enabled

    def undo(self):
        self._hs.basic_structure(self._index).enabled = self._old

    def redo(self):
        self._hs.basic_structure(self._index).enabled = self._new


class AddCommand(QUndoCommand):
    def __init__(self, hs, index):
        QUndoCommand.__init__(self)

        self._hs = hs

        self._index = index
        self._new = None

    def undo(self):
        self._hs.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._hs.add(self._index)
        else:
            self._hs.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, hs, rows):
        QUndoCommand.__init__(self)

        self._hs = hs

        self._rows = rows

        self._bhs = []
        for row in rows:
            self._bhs.append((row, self._hs.basic_structure(row)))

    def undo(self):
        for row, el in self._bhs:
            self._hs.insert(row, el)

    def redo(self):
        self._hs.delete_i(self._rows)


class PasteCommand(QUndoCommand):
    def __init__(self, hs, row, h_s):
        QUndoCommand.__init__(self)

        self._hs = hs

        self._row = row
        self._bhs = deepcopy(h_s)
        self._bhs.reverse()

    def undo(self):
        self._hs.delete_i(range(self._row, self._row + len(self._bhs)))

    def redo(self):
        for r in self._bhs:
            self._hs.insert(self._row, r)

####################################
# Basic hydraulic structure values #
####################################


class SetValueCommand(QUndoCommand):
    def __init__(self, bhs, index, value):
        QUndoCommand.__init__(self)

        self._bhs = bhs
        self._index = index
        self._old = self._bhs.parameters[self._index].value
        self._new = self._bhs.parameters[self._index].type(value)

    def undo(self):
        self._bhs.parameters[self._index].value = self._old

    def redo(self):
        self._bhs.parameters[self._index].value = self._new
