# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timer, trace

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5 import QtCore
from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel, QCoreApplication,
    pyqtSlot, pyqtSignal, QItemSelectionModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QHeaderView, QDoubleSpinBox, QVBoxLayout, QCheckBox
)

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.Tools.Plot.PamhyrToolbar import PamhyrPlotToolbar

from View.HydraulicStructures.PlotAC import PlotAC

from View.HydraulicStructures.BasicHydraulicStructures.Table import (
    ComboBoxDelegate, TableModel, ParametersTableModel,
)

from View.Network.GraphWidget import GraphWidget
from View.HydraulicStructures.BasicHydraulicStructures.Translate import (
    BasicHydraulicStructuresTranslate
)

_translate = QCoreApplication.translate

logger = logging.getLogger()


class BasicHydraulicStructuresWindow(PamhyrWindow):
    _pamhyr_ui = "BasicHydraulicStructures"
    _pamhyr_name = "Basic Hydraulic Structures"

    def __init__(self, data=None, study=None, config=None, parent=None):
        trad = BasicHydraulicStructuresTranslate()
        name = " - ".join([
            trad[self._pamhyr_name], data.name, study.name
        ])

        super(BasicHydraulicStructuresWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._hash_data.append(data)

        self._hs = data

        self.setup_table()
        self.setup_checkbox()
        self.setup_plot()
        self.setup_connections()

        self.update()

    def setup_table(self):
        self.setup_table_bhs()
        self.setup_table_bhs_parameters()

    def setup_table_bhs(self):
        self._table = None

        self._delegate_type = ComboBoxDelegate(
            trad=self._trad,
            parent=self
        )

        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=["name", "type"],
            delegates={
                "type": self._delegate_type,
            },
            trad=self._trad,
            data=self._hs,
            undo=self._undo_stack,
            parent=self,
        )

        selectionModel = table.selectionModel()
        index = table.model().index(0, 0)

        selectionModel.select(
            index,
            QItemSelectionModel.Rows |
            QItemSelectionModel.ClearAndSelect |
            QItemSelectionModel.Select
        )
        table.scrollTo(index)

    def setup_table_bhs_parameters(self):
        self._table_parameters = None

        table = self.find(QTableView, f"tableView_2")
        self._table_parameters = ParametersTableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers_parameters"),
            editable_headers=["value"],
            delegates={},
            trad=self._trad,
            data=self._hs,
            undo=self._undo_stack,
            parent=self,
        )

    def setup_checkbox(self):
        self._checkbox = self.find(QCheckBox, f"checkBox")
        self._set_checkbox_state()

    def setup_plot(self):
        self.canvas = MplCanvas(width=5, height=4, dpi=100)
        self.canvas.setObjectName("canvas")
        self.toolbar = PamhyrPlotToolbar(
            self.canvas, self
        )
        self.plot_layout = self.find(QVBoxLayout, "verticalLayout")
        self.plot_layout.addWidget(self.toolbar)
        self.plot_layout.addWidget(self.canvas)

        reach = self._hs.input_reach
        profile_rk = self._hs.input_rk
        if profile_rk is not None:
            profiles = reach.reach.get_profiles_from_rk(float(profile_rk))
        else:
            profiles = None

        if profiles is not None and len(profiles) != 0:
            profile = profiles[0]
        else:
            profile = None

        self.plot_ac = PlotAC(
            canvas=self.canvas,
            river=self._study.river,
            reach=self._hs.input_reach,
            profile=profile,
            trad=self._trad,
            toolbar=self.toolbar
        )
        self.plot_ac.draw()

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self._checkbox.clicked.connect(self._set_basic_structure_state)

        table = self.find(QTableView, "tableView")
        table.selectionModel()\
             .selectionChanged\
             .connect(self.update)

        self._table.dataChanged.connect(self.update)
        self._table.layoutChanged.connect(self.update)

    def index_selected(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0]
        else:
            return None

    def index_selected_row(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0].row()
        else:
            return None

    def index_selected_rows(self):
        table = self.find(QTableView, "tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()

        if len(self._hs) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

    def delete(self):
        rows = self.index_selected_rows()

        if len(rows) == 0:
            return

        self._table.delete(rows)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()

    def _set_checkbox_state(self):
        row = self.index_selected_row()

        if row is None:
            self._checkbox.setEnabled(False)
            self._checkbox.setChecked(True)
        else:
            self._checkbox.setEnabled(True)
            self._checkbox.setChecked(self._hs.basic_structure(row).enabled)

    def _set_basic_structure_state(self):
        rows = self.index_selected_rows()
        if len(rows) != 0:
            for row in rows:
                if row is not None:
                    self._table.enabled(
                        row,
                        self._checkbox.isChecked()
                    )

    def update(self):
        self._set_checkbox_state()
        self._update_parameters_table()

    def _update_parameters_table(self):
        row = self.index_selected_row()
        self._table_parameters.update_hs_index(row)
