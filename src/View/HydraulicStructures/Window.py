# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timer, trace

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5 import QtCore
from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel, QCoreApplication,
    pyqtSlot, pyqtSignal, QItemSelectionModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QHeaderView, QDoubleSpinBox, QVBoxLayout, QCheckBox
)

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.Tools.Plot.PamhyrToolbar import PamhyrPlotToolbar

from View.HydraulicStructures.PlotAC import PlotAC
from View.HydraulicStructures.PlotRKC import PlotRKC

from View.HydraulicStructures.Table import (
    TableModel, ComboBoxDelegate
)

from View.Network.GraphWidget import GraphWidget
from View.HydraulicStructures.Translate import HydraulicStructuresTranslate

from View.HydraulicStructures.BasicHydraulicStructures.Window import (
    BasicHydraulicStructuresWindow
)

logger = logging.getLogger()


class HydraulicStructuresWindow(PamhyrWindow):
    _pamhyr_ui = "HydraulicStructures"
    _pamhyr_name = "Hydraulic Structures"

    def __init__(self, study=None, config=None, parent=None):
        trad = HydraulicStructuresTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(HydraulicStructuresWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._hs_lst = self._study.river._hydraulic_structures

        self.setup_table()
        self.setup_checkbox()
        self.setup_plots()
        self.setup_connections()

        self.update()

    def setup_table(self):
        self._table = None

        self._delegate_reach = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            parent=self,
            mode="reaches"
        )
        self._delegate_rk = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            parent=self,
            mode="rk"
        )

        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=["name", "reach", "rk"],
            delegates={
                "reach": self._delegate_reach,
                "rk": self._delegate_rk,
            },
            trad=self._trad,
            data=self._study.river,
            undo=self._undo_stack,
        )

        selectionModel = table.selectionModel()
        index = table.model().index(0, 0)

        selectionModel.select(
            index,
            QItemSelectionModel.Rows |
            QItemSelectionModel.ClearAndSelect |
            QItemSelectionModel.Select
        )
        table.scrollTo(index)

    def setup_checkbox(self):
        self._checkbox = self.find(QCheckBox, f"checkBox")
        self._set_checkbox_state()

    def setup_plots(self):
        self.canvas = MplCanvas(width=5, height=4, dpi=100)
        self.canvas.setObjectName("canvas")
        self.toolbar = PamhyrPlotToolbar(
            self.canvas, self
        )
        self.plot_layout = self.find(QVBoxLayout, "verticalLayout")
        self.plot_layout.addWidget(self.toolbar)
        self.plot_layout.addWidget(self.canvas)

        self.plot_rkc = PlotRKC(
            canvas=self.canvas,
            river=self._study.river,
            reach=None,
            profile=None,
            trad=self._trad,
            toolbar=self.toolbar,
            parent=self
        )
        self.plot_rkc.draw()

        self.canvas_2 = MplCanvas(width=5, height=4, dpi=100)
        self.canvas_2.setObjectName("canvas_2")
        self.toolbar_2 = PamhyrPlotToolbar(
            self.canvas_2, self
        )
        self.plot_layout_2 = self.find(QVBoxLayout, "verticalLayout_2")
        self.plot_layout_2.addWidget(self.toolbar_2)
        self.plot_layout_2.addWidget(self.canvas_2)

        self.plot_ac = PlotAC(
            canvas=self.canvas_2,
            river=self._study.river,
            reach=None,
            profile=None,
            trad=self._trad,
            toolbar=self.toolbar_2
        )
        self.plot_ac.draw()

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)
        self._checkbox.clicked.connect(self._set_structure_state)

        table = self.find(QTableView, "tableView")
        table.selectionModel()\
             .selectionChanged\
             .connect(self.update)

        self._table.dataChanged.connect(self.update)
        self._table.layoutChanged.connect(self.update)

    def index_selected(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0]
        else:
            return None

    def index_selected_row(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0].row()
        else:
            return None

    def index_selected_rows(self):
        table = self.find(QTableView, "tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()
        if len(self._hs_lst) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()

    def edit(self):
        rows = self.index_selected_rows()
        for row in rows:
            data = self._hs_lst.get(row)

            if self.sub_window_exists(
                BasicHydraulicStructuresWindow,
                data=[self._study, None, data]
            ):
                continue

            win = BasicHydraulicStructuresWindow(
                data=data,
                study=self._study,
                parent=self
            )
            win.show()

    def _set_checkbox_state(self):
        row = self.index_selected_row()
        if row is None:
            self._checkbox.setEnabled(False)
            self._checkbox.setChecked(True)
        else:
            self._checkbox.setEnabled(True)
            self._checkbox.setChecked(self._hs_lst.get(row).enabled)

    def _set_structure_state(self):
        rows = self.index_selected_rows()
        if len(rows) != 0:
            for row in rows:
                if row is not None:
                    self._table.enabled(
                        row,
                        self._checkbox.isChecked()
                    )

    def update(self):
        self._set_checkbox_state()
        self._update_clear_plot()

    def _update_clear_plot(self):
        rows = self.index_selected_rows()

        if len(rows) == 0 or len(self._hs_lst) == 0:
            self._update_clear_all()
            return

        reach = self._hs_lst.get(rows[0]).input_reach
        if reach is not None:
            self.plot_rkc.set_reach(reach)
            self.plot_ac.set_reach(reach)

            profile_rk = self._hs_lst.get(rows[0]).input_rk
            if profile_rk is not None:
                profiles = reach.reach\
                                .get_profiles_from_rk(
                                    float(profile_rk)
                                )

                if len(profiles) != 0 and profiles is not None:
                    profile = profiles[0]

                    self.plot_rkc.set_profile(profile)
                    self.plot_ac.set_profile(profile)
                else:
                    self._update_clear_profile()
            else:
                self._update_clear_profile()
        else:
            self._update_clear_all()

    def _update_clear_all(self):
        self.plot_rkc.clear()
        self.plot_ac.clear()

    def _update_clear_profile(self):
        self.plot_ac.clear()
        self.plot_rkc.clear_profile()
