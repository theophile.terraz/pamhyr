# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

logger = logging.getLogger()


class SetNameCommand(QUndoCommand):
    def __init__(self, h_s_lst, index, new_value):
        QUndoCommand.__init__(self)

        self._h_s_lst = h_s_lst
        self._index = index
        self._old = self._h_s_lst.get(self._index).name
        self._new = str(new_value)

    def undo(self):
        self._h_s_lst.get(self._index).name = self._old

    def redo(self):
        self._h_s_lst.get(self._index).name = self._new


class SetReachCommand(QUndoCommand):
    def __init__(self, h_s_lst, index, reach):
        QUndoCommand.__init__(self)

        self._h_s_lst = h_s_lst
        self._index = index
        self._old = self._h_s_lst.get(self._index).input_reach
        self._new = reach
        self._old_rk = self._h_s_lst.get(self._index).input_rk
        self._new_rk = None

    def undo(self):
        i = self._h_s_lst.get(self._index)
        i.input_reach = self._old
        i.input_rk = self._old_rk

    def redo(self):
        i = self._h_s_lst.get(self._index)
        i.input_reach = self._new
        i.input_rk = self._new_rk


class SetRkCommand(QUndoCommand):
    def __init__(self, h_s_lst, index, rk):
        QUndoCommand.__init__(self)

        self._h_s_lst = h_s_lst
        self._index = index
        self._old = self._h_s_lst.get(self._index).input_rk
        self._new = rk

    def undo(self):
        self._h_s_lst.get(self._index).input_rk = self._old

    def redo(self):
        self._h_s_lst.get(self._index).input_rk = self._new


class SetEnabledCommand(QUndoCommand):
    def __init__(self, h_s_lst, index, enabled):
        QUndoCommand.__init__(self)

        self._h_s_lst = h_s_lst
        self._index = index
        self._old = not enabled
        self._new = enabled

    def undo(self):
        self._h_s_lst.get(self._index).enabled = self._old

    def redo(self):
        self._h_s_lst.get(self._index).enabled = self._new


class AddCommand(QUndoCommand):
    def __init__(self, h_s_lst, index):
        QUndoCommand.__init__(self)

        self._h_s_lst = h_s_lst

        self._index = index
        self._new = None

    def undo(self):
        self._h_s_lst.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._h_s_lst.new(self._h_s_lst, self._index)
        else:
            self._h_s_lst.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, h_s_lst, rows):
        QUndoCommand.__init__(self)

        self._h_s_lst = h_s_lst

        self._rows = rows

        self._h_s = []
        for row in rows:
            self._h_s.append((row, self._h_s_lst.get(row)))
        self._h_s.sort()

    def undo(self):
        for row, el in self._h_s:
            self._h_s_lst.insert(row, el)

    def redo(self):
        self._h_s_lst.delete_i(self._rows)


class PasteCommand(QUndoCommand):
    def __init__(self, h_s_lst, row, h_s):
        QUndoCommand.__init__(self)

        self._h_s_lst = h_s_lst

        self._row = row
        self._h_s = deepcopy(h_s)
        self._h_s.reverse()

    def undo(self):
        self._h_s_lst.delete_i(
            self._tab,
            range(self._row, self._row + len(self._h_s))
        )

    def redo(self):
        for r in self._h_s:
            self._h_s_lst.insert(self._row, r)
