# PlotAC.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import timer
from View.Tools.PamhyrPlot import PamhyrPlot
from matplotlib import pyplot as plt


class PlotAC(PamhyrPlot):
    def __init__(self, canvas=None, trad=None, toolbar=None,
                 river=None, reach=None, profile=None,
                 parent=None):
        super(PlotAC, self).__init__(
            canvas=canvas,
            trad=trad,
            data=river,
            toolbar=toolbar,
            parent=parent
        )

        self._current_reach = reach
        self._current_profile = profile

        self.label_x = self._trad["x"]
        self.label_y = self._trad["unit_elevation"]

        self._isometric_axis = False

        self._auto_relim_update = True
        self._autoscale_update = True

    @property
    def river(self):
        return self.data

    @river.setter
    def river(self, river):
        self.data = river

    @timer
    def draw(self):
        self.init_axes()

        if self.data is None:
            self.line_rk = None
            return

        if self._current_reach is None:
            self.line_rk = None
            return

        self.draw_data()

        self.idle()
        self._init = True

    def draw_data(self):
        reach = self._current_reach

        if self._current_profile is None:
            self.line_rk = None
        else:
            profile = self._current_profile
            x = profile.get_station()
            z = profile.z()

            self.line_rk, = self.canvas.axes.plot(
                x, z,
                color=self.color_plot_river_bottom,
                **self.plot_default_kargs
            )

    def set_reach(self, reach):
        self._current_reach = reach
        self.update()

    def set_profile(self, profile):
        self._current_profile = profile
        self.update()

    def update(self):
        if self.line_rk is None:
            self.draw()
            return

        if self._current_reach is None or self._current_profile is None:
            self.update_clear()
        else:
            self.update_data()

        self.update_idle()

    def update_data(self):
        profile = self._current_profile
        x = profile.get_station()
        z = profile.z()

        self.line_rk.set_data(x, z)

    def clear(self):
        self.update_clear()

    def update_clear(self):
        if self.line_rk is not None:
            self.line_rk.set_data([], [])
        self.update_idle()
