# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog
from Solver.Solvers import solver_type_list
from Solver.GenericSolver import GenericSolver

from View.Configure.Translate import ConfigureTranslate

from PyQt5.QtWidgets import (
    QPushButton,
)


class ConfigureSolverWindow(PamhyrDialog):
    _pamhyr_ui = "ConfigureAddSolverDialog"
    _pamhyr_name = "Add/Edit Solver"

    def __init__(self, data=None, config=None, parent=None):
        trad = ConfigureTranslate()

        if data is not None:
            name = trad["edit_solver"]
            name += f" - {data.name}"
        else:
            name = trad["add_solver"]

        super(ConfigureSolverWindow, self).__init__(
            title=name,
            config=config,
            options=[],
            trad=trad,
            parent=parent
        )

        # Combo box item
        for solver in solver_type_list:
            self.combobox_add_item("comboBox_solver", solver)

        # Data to return
        self.data = data
        if self.data is not None:
            self.copy_data()

        self.setup_connection()

    def copy_data(self):
        self.set_combobox_text("comboBox_solver", self.data.type)
        self.set_line_edit_text("lineEdit_name", self.data.name)
        self.set_line_edit_text("lineEdit_description", self.data.description)
        self.set_line_edit_text("lineEdit_input", self.data._path_input)
        self.set_line_edit_text("lineEdit_input_cmd", self.data._cmd_input)
        self.set_line_edit_text("lineEdit_solver", self.data._path_solver)
        self.set_line_edit_text("lineEdit_solver_cmd", self.data._cmd_solver)
        self.set_line_edit_text("lineEdit_output", self.data._path_output)
        self.set_line_edit_text("lineEdit_output_cmd", self.data._cmd_output)

    def setup_connection(self):
        # File button
        buttons = {
            "pushButton_input": (lambda: self.file_dialog(
                select_file="ExistingFile",
                callback=lambda f: self.set_line_edit_text(
                    "lineEdit_input", f[0])
            )),
            "pushButton_solver": (lambda: self.file_dialog(
                select_file="ExistingFile",
                callback=lambda f: self.set_line_edit_text(
                    "lineEdit_solver", f[0])
            )),
            "pushButton_output": (lambda: self.file_dialog(
                select_file="ExistingFile",
                callback=lambda f: self.set_line_edit_text(
                    "lineEdit_output", f[0])
            )),
        }

        for button in buttons:
            self.find(QPushButton, button).clicked.connect(buttons[button])

    def accept(self):
        if self.get_line_edit_text("lineEdit_name") == "":
            self.message_box(
                "A solver need a name",
                "Please give a name to your solver"
            )
        else:
            # Build new solver from selected type
            stype = self.get_combobox_text("comboBox_solver")
            self.data = solver_type_list[stype](
                self.get_line_edit_text("lineEdit_name"))

            self.data.description = self.get_line_edit_text(
                "lineEdit_description")
            self.data.set_input(
                self.get_line_edit_text("lineEdit_input"),
                self.get_line_edit_text("lineEdit_input_cmd")
            )
            self.data.set_solver(
                self.get_line_edit_text("lineEdit_solver"),
                self.get_line_edit_text("lineEdit_solver_cmd")
            )
            self.data.set_output(
                self.get_line_edit_text("lineEdit_output"),
                self.get_line_edit_text("lineEdit_output_cmd")
            )

            self.done(True)
