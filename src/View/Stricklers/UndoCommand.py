# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.LateralContribution.LateralContribution import LateralContribution
from Model.LateralContribution.LateralContributionList import (
    LateralContributionList
)


class SetNameCommand(QUndoCommand):
    def __init__(self, data, index, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._old = self._data.get(self._index).name
        self._new = str(new_value)

    def undo(self):
        self._data.get(self._index).name = self._old

    def redo(self):
        self._data.get(self._index).name = self._new


class SetCommentCommand(QUndoCommand):
    def __init__(self, data, index, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._old = self._data.get(self._index).comment
        self._new = str(new_value)

    def undo(self):
        self._data.get(self._index).comment = self._old

    def redo(self):
        self._data.get(self._index).comment = self._new


class SetMinorCommand(QUndoCommand):
    def __init__(self, data, index, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._old = self._data.get(self._index).minor
        self._new = float(new_value)

    def undo(self):
        self._data.get(self._index).minor = self._old

    def redo(self):
        self._data.get(self._index).minor = self._new


class SetMediumCommand(QUndoCommand):
    def __init__(self, data, index, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._old = self._data.get(self._index).medium
        self._new = float(new_value)

    def undo(self):
        self._data.get(self._index).medium = self._old

    def redo(self):
        self._data.get(self._index).medium = self._new


class AddCommand(QUndoCommand):
    def __init__(self, data, index):
        QUndoCommand.__init__(self)

        self._data = data
        self._index = index
        self._new = None

    def undo(self):
        self._data.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._data.new(self._index)
        else:
            self._data.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, data, rows):
        QUndoCommand.__init__(self)

        self._data = data
        self._rows = rows

        self._el = []
        for row in rows:
            self._el.append((row, self._data.get(row)))
        self._el.sort()

    def undo(self):
        for row, el in self._el:
            self._data.insert(row, el)

    def redo(self):
        self._data.delete_i(self._rows)


class SortCommand(QUndoCommand):
    def __init__(self, data, _reverse):
        QUndoCommand.__init__(self)

        self._data = data
        self._reverse = _reverse

        self._old = self._data.stricklers
        self._indexes = None

    def undo(self):
        ll = self._data.stricklers
        self._data.sort()

    def redo(self):
        self._data.sort(
            reverse=self._reverse,
        )
        if self._indexes is None:
            self._indexes = list(
                map(
                    lambda p: self._old.index(p),
                    self._data.stricklers
                )
            )
            self._old = None


class PasteCommand(QUndoCommand):
    def __init__(self, data, row, el):
        QUndoCommand.__init__(self)

        self._data = data
        self._row = row
        self._el = deepcopy(el)
        self._el.reverse()

    def undo(self):
        self._data.delete(self._el)

    def redo(self):
        for el in self._el:
            self._data.insert(self._row, el)
