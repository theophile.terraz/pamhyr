# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class StricklersTranslate(MainTranslate):
    def __init__(self):
        super(StricklersTranslate, self).__init__()

        self._dict["Stricklers"] = _translate(
            "Stricklers", "Strickler coefficients"
        )

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "minor": self._dict["main_channel"],
            "medium": self._dict["floodway"],
            "comment": self._dict["comment"],
        }
