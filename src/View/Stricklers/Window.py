# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
)

from Model.Stricklers.Stricklers import Stricklers

from View.Stricklers.UndoCommand import PasteCommand
from View.Stricklers.Table import TableModel
from View.Stricklers.translate import StricklersTranslate

_translate = QCoreApplication.translate

logger = logging.getLogger()


class StricklersWindow(PamhyrWindow):
    _pamhyr_ui = "Stricklers"
    _pamhyr_name = "Stricklers"

    def __init__(self, study=None, config=None, parent=None):
        trad = StricklersTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(StricklersWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self.setup_table()
        self.setup_connections()

    def setup_table(self):
        self._table = {}

        for t in ["app", "study"]:
            table = self.find(QTableView, f"tableView_{t}")
            if t == "study":
                data = self._study.river.stricklers
            else:
                data = self._config.stricklers

            self._table[t] = TableModel(
                table_view=table,
                table_headers=self._trad.get_dict("table_headers"),
                editable_headers=["name", "comment", "minor", "medium"],
                data=data,
                undo=self._undo_stack,
            )

            table.setModel(self._table[t])
            table.setSelectionBehavior(QAbstractItemView.SelectRows)
            table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
            table.setAlternatingRowColors(True)

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_del").triggered.connect(self.delete)
        self.find(QAction, "action_sort").triggered.connect(self.sort)

    def index_selected_rows(self):
        table = self.find(QTableView, f"tableView_study")
        return list(
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            self._table['study'].add(0)
        else:
            self._table['study'].add(rows[0])

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table['study'].delete(rows)

    def sort(self):
        self._table['study'].sort(False)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table['study'].undo()

    def _redo(self):
        self._table['study'].redo()
