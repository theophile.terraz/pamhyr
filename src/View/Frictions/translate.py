# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class FrictionsTranslate(MainTranslate):
    def __init__(self):
        super(FrictionsTranslate, self).__init__()

        self._dict["rk"] = self._dict["unit_rk"]
        self._dict["strickler_plot"] = _translate(
            "Frictions", "Strickler ($m^{1/3}/s$)"
        )
        self._dict["stricklers"] = _translate(
            "Frictions", "Stricklers"
        )

        self._dict["Edit frictions"] = _translate(
            "Frictions", "Edit frictions"
        )

        self._sub_dict["table_headers"] = {
            # "name": self._dict["name"],
            # "edge": self._dict["reach"],
            "begin_rk": _translate("Frictions", "Start (m)"),
            "end_rk": _translate("Frictions", "End (m)"),
            "begin_strickler": _translate("Frictions", "Coefficient"),
            # "end_strickler": _translate("Frictions", "End coefficient"),
        }
