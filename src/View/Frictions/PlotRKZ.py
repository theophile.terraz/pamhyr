# PlotRKC.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication
from View.HydraulicStructures.PlotRKC import PlotRKC

_translate = QCoreApplication.translate


class PlotRKZ(PlotRKC):
    def __init__(self, canvas=None, trad=None, toolbar=None,
                 river=None, reach=None, profile=None,
                 parent=None):
        super(PlotRKC, self).__init__(
            canvas=canvas,
            trad=trad,
            data=river,
            toolbar=toolbar,
            parent=parent
        )

        self._current_reach = reach
        self._current_profile = profile

        self.label_x = self._trad["unit_rk"]
        self.label_y = self._trad["unit_elevation"]

        self._isometric_axis = False

        self._auto_relim_update = True
        self._autoscale_update = True
        self.parent = parent

    def onpick(self, event):
        return

    def draw_hs(self):
        pass
