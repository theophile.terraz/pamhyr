# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Friction.Friction import Friction
from Model.Friction.FrictionList import FrictionList


class SetNameCommand(QUndoCommand):
    def __init__(self, frictions, index, new_value):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._index = index
        self._old = self._frictions.get(self._index).name
        self._new = str(new_value)

    def undo(self):
        self._frictions.get(self._index).name = self._old

    def redo(self):
        self._frictions.get(self._index).name = self._new


class SetBeginCommand(QUndoCommand):
    def __init__(self, frictions, index, new_value):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._index = index
        self._old = self._frictions.get(self._index).begin_rk
        self._new = float(new_value)

    def undo(self):
        self._frictions.get(self._index).begin_rk = float(self._old)

    def redo(self):
        self._frictions.get(self._index).begin_rk = float(self._new)


class SetEndCommand(QUndoCommand):
    def __init__(self, frictions, index, new_value):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._index = index
        self._old = self._frictions.get(self._index).end_rk
        self._new = float(new_value)

    def undo(self):
        self._frictions.get(self._index).end_rk = float(self._old)

    def redo(self):
        self._frictions.get(self._index).end_rk = float(self._new)


class SetBeginStricklerCommand(QUndoCommand):
    def __init__(self, frictions, index, new_value):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._index = index
        self._old = self._frictions.get(self._index).begin_strickler
        self._new = new_value

    def undo(self):
        self._frictions.get(self._index).begin_strickler = self._old

    def redo(self):
        self._frictions.get(self._index).begin_strickler = self._new


class SetEndStricklerCommand(QUndoCommand):
    def __init__(self, frictions, index, new_value):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._index = index
        self._old = self._frictions.get(self._index).end_strickler
        self._new = new_value

    def undo(self):
        self._frictions.get(self._index).end_strickler = self._old

    def redo(self):
        self._frictions.get(self._index).end_strickler = self._new


class SetEdgeCommand(QUndoCommand):
    def __init__(self, frictions, index, edge):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._index = index
        self._old = self._frictions.get(self._index).edge
        self._new = edge

    def undo(self):
        self._frictions.get(self._index).edge = self._old

    def redo(self):
        self._frictions.get(self._index).edge = self._new


class AddCommand(QUndoCommand):
    def __init__(self, frictions, index, reach):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._index = index
        self._reach = reach
        self._new = None

    def undo(self):
        self._frictions.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._frictions.new(self._index)
            self._new.edge = self._reach
        else:
            self._frictions.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, frictions, rows):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._rows = rows

        self._friction = []
        for row in rows:
            self._friction.append((row, self._frictions.get(row)))
        self._friction.sort()

    def undo(self):
        for row, el in self._friction:
            self._frictions.insert(row, el)

    def redo(self):
        self._frictions.delete_i(self._rows)


class SortCommand(QUndoCommand):
    def __init__(self, frictions, _reverse):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._reverse = _reverse

        self._old = self._frictions.frictions
        self._indexes = None

    def undo(self):
        ll = self._frictions.frictions
        self._frictions.sort(
            key=lambda x: self._indexes[ll.index(x)]
        )

    def redo(self):
        self._frictions.sort(
            reverse=self._reverse,
            key=lambda x: x.name
        )
        if self._indexes is None:
            self._indexes = list(
                map(
                    lambda p: self._old.index(p),
                    self._frictions.frictions
                )
            )
            self._old = None


class MoveCommand(QUndoCommand):
    def __init__(self, frictions, up, i):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._frictions.move_up(self._i)
        else:
            self._frictions.move_down(self._i)

    def redo(self):
        if self._up:
            self._frictions.move_up(self._i)
        else:
            self._frictions.move_down(self._i)


class PasteCommand(QUndoCommand):
    def __init__(self, frictions, row, friction):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._row = row
        self._friction = deepcopy(friction)
        self._friction.reverse()

    def undo(self):
        self._frictions.delete(self._friction)

    def redo(self):
        for friction in self._friction:
            self._frictions.insert(self._row, friction)


class DuplicateCommand(QUndoCommand):
    def __init__(self, frictions, rows, friction):
        QUndoCommand.__init__(self)

        self._frictions = frictions
        self._rows = rows
        self._friction = deepcopy(friction)
        self._friction.reverse()

    def undo(self):
        self._frictions.delete(self._friction)

    def redo(self):
        for friction in self._frictions:
            self._frictions.insert(self._rows[0], friction)
