# PlotStricklers.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timer, flatten
from View.Tools.PamhyrPlot import PamhyrPlot

from PyQt5.QtCore import (
    QCoreApplication
)

logger = logging.getLogger()

_translate = QCoreApplication.translate


class PlotStricklers(PamhyrPlot):
    def __init__(self, data=None, canvas=None, trad=None,
                 toolbar=None, parent=None):
        super(PlotStricklers, self).__init__(
            canvas=canvas,
            trad=trad,
            data=data,
            toolbar=toolbar,
            parent=parent
        )

        self.label_x = self._trad["rk"]
        self.label_y = self._trad["strickler_plot"]

        self.line_rk_elevation = [None, None]

        self._isometric_axis = False

        self._auto_relim = False
        self._auto_relim_update = False
        self._autoscale_update = True

    @timer
    def draw(self, highlight=None):
        self.init_axes()

        if self.data is None:
            return

        self.draw_data()

        self.idle()
        self._init = True

    def draw_data(self):
        rk = self.data.reach.get_rk()
        self.canvas.axes.set_xlim(
            left=min(rk), right=max(rk)
        )

        frictions = self.data.frictions
        if len(frictions) != 0:
            lst = frictions.frictions
            self.draw_frictions(lst, self.color_plot)

            # HightLight
            rk_min, rk_max = (-1, -1)
            if self._highlight_data is not None:
                rk_min, rk_max = self._highlight_data

                lst = list(
                    filter(
                        lambda s: (s.begin_rk == rk_min and
                                   s.end_rk == rk_max),
                        frictions.frictions
                    )
                )

                self.draw_frictions(lst, self.color_plot_highlight)

    def draw_frictions(self, frictions, color):
        lst = sorted(
            filter(
                lambda f: f.is_full_defined(),
                frictions
            ),
            key=lambda s: s.begin_rk
        )

        coef = flatten(
            map(
                lambda s: [s.begin_strickler, s.end_strickler],
                lst
            )
        )

        rk = flatten(
            map(
                lambda s: [s.begin_rk, s.end_rk],
                lst
            )
        )

        coef_minor = list(map(lambda s: s.minor, coef))
        coef_medium = list(map(lambda s: s.medium, coef))

        self.line_rk_elevation[0] = self.canvas.axes.plot(
            rk, coef_minor,
            color=color, lw=1.
        )

        self.line_rk_elevation[1] = self.canvas.axes.plot(
            rk, coef_medium,
            color=color, lw=1.
        )

    @timer
    def update(self):
        self.draw()
