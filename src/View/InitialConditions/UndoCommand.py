# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.InitialConditions.InitialConditions import InitialConditions
from Model.InitialConditions.InitialConditionsDict import InitialConditionsDict


class SetCommand(QUndoCommand):
    def __init__(self, ics, row, column, new_value):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._row = row
        self._column = column
        self._old = self._ics.get(self._row)[column]

        _type = float
        if column == "name" or column == "comment":
            _type = str

        self._new = _type(new_value)

    def undo(self):
        self._ics.get(self._row)[self._column] = self._old

    def redo(self):
        self._ics.get(self._row)[self._column] = self._new


class AddCommand(QUndoCommand):
    def __init__(self, ics, index):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._index = index
        self._new = None

    def undo(self):
        self._ics.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._ics.new(self._index)
        else:
            self._ics.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, ics, rows):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._rows = rows

        self._ic = []
        for row in rows:
            self._ic.append((row, self._ics.get(row)))
        self._ic.sort()

    def undo(self):
        for row, el in self._ic:
            self._ics.insert(row, el)

    def redo(self):
        self._ics.delete_i(self._rows)


class SortCommand(QUndoCommand):
    def __init__(self, ics, _reverse):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._reverse = _reverse

        self._old = self._ics.data
        self._indexes = None

    def undo(self):
        ll = self._ics.data
        self._ics.sort(
            key=lambda x: self._indexes[ll.index(x)]
        )

    def redo(self):
        self._ics.sort(
            reverse=self._reverse,
            key=lambda x: x["rk"]
        )
        if self._indexes is None:
            self._indexes = list(
                map(
                    lambda p: self._old.index(p),
                    self._ics.data
                )
            )
            self._old = None


class MoveCommand(QUndoCommand):
    def __init__(self, ics, up, i):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._ics.move_up(self._i)
        else:
            self._ics.move_down(self._i)

    def redo(self):
        if self._up:
            self._ics.move_up(self._i)
        else:
            self._ics.move_down(self._i)


class InsertCommand(QUndoCommand):
    def __init__(self, ics, row, ic):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._row = row
        self._ic = deepcopy(ic)
        self._ic.reverse()

    def undo(self):
        self._ics.delete(self._ic)

    def redo(self):
        for ic in self._ic:
            self._ics.insert(self._row, ic)


class DuplicateCommand(QUndoCommand):
    def __init__(self, ics, rows, ic):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._rows = rows
        self._ic = deepcopy(ic)
        self._ic.reverse()

    def undo(self):
        self._ics.delete(self._ic)

    def redo(self):
        for ic in self._ics:
            self._ics.insert(self._rows[0], ic)


class GenerateCommand(QUndoCommand):
    def __init__(self, ics, generator, param, option):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._param = param
        self._option = option
        self._copy = self._ics.data
        self._generator = generator

    def undo(self):
        self._ics.data = self._copy

    def redo(self):
        if self._generator == "growing":
            self._ics.generate_growing_constant_depth(self._param,
                                                      self._option)
        elif self._generator == "discharge":
            self._ics.generate_discharge(self._param,
                                         self._option)
        elif self._generator == "height":
            self._ics.generate_height(self._param[0],
                                      self._param[1],
                                      self._option,
                                      self._param[2])


class ReplaceDataCommand(QUndoCommand):
    def __init__(self, ics, new_data):
        QUndoCommand.__init__(self)

        self._ics = ics
        self._copy = self._ics.data
        self._new_data = new_data
        self._rows = list(range(len(ics.data)))
        self._new_rows = list(range(len(new_data)))

    def undo(self):
        self._ics.delete_i(self._new_rows)
        for row, el in enumerate(self._copy):
            self._ics.insert(row, el)

    def redo(self):
        self._ics.delete_i(self._rows)
        for row, el in enumerate(self._new_data):
            self._ics.insert(row, el)
