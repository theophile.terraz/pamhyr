# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class ICTranslate(MainTranslate):
    def __init__(self):
        super(ICTranslate, self).__init__()

        self._dict["Initial condition"] = _translate(
            "InitialCondition", "Initial conditions")

        self._dict["elevation"] = self._dict["unit_elevation"]
        self._dict["discharge"] = self._dict["unit_discharge"]
        self._dict["rk"] = self._dict["unit_rk"]

        self._sub_dict["table_headers"] = {
            # "name": _translate("InitialCondition", "Name"),
            "rk": self._dict["unit_rk"],
            "discharge": self._dict["unit_discharge"],
            "elevation": self._dict["unit_elevation"],
            "height": self._dict["unit_depth"],
            "velocity": self._dict["unit_velocity"],
            # "comment": _translate("InitialCondition", "Comment"),
        }
