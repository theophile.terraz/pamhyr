# PlotDischarge.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import timer
from View.Tools.PamhyrPlot import PamhyrPlot


class PlotDischarge(PamhyrPlot):
    def __init__(self, canvas=None, trad=None, toolbar=None,
                 data=None, parent=None):
        super(PlotDischarge, self).__init__(
            canvas=canvas,
            trad=trad,
            data=data,
            toolbar=toolbar,
            parent=parent
        )

        self.label_x = self._trad["rk"]
        self.label_y = self._trad["discharge"]

        self._isometric_axis = False

        self._auto_relim_update = True
        self._autoscale_update = True

    @timer
    def draw(self):
        self.init_axes()

        if self.data is None:
            return

        self.draw_data()

        self.idle()
        self._init = True

    def draw_data(self):
        self.line_discharge = []

        if len(self.data) != 0:
            rk = self.data.get_rk()
            discharge = self.data.get_discharge()

            line, = self.canvas.axes.plot(
                rk, discharge,
                color=self.color_plot,
                **self.plot_default_kargs
            )
            self.line_discharge.append(line)

    @timer
    def update(self, ind=None):
        if not self._init:
            self.draw()

        self.update_data()

        self.update_idle()

    def update_data(self):
        if len(self.data) == len(self.line_discharge):
            rk = self.data.get_rk()
            discharge = self.data.get_discharge()

            line, = self.canvas.axes.plot(
                rk, discharge,
                color=self.color_plot,
                **self.plot_default_kargs
            )
            self.line_discharge.append(line)
        else:
            for line in self.line_discharge:
                line.remove()

            self._draw_data()
