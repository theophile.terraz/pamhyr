# PlotDRK.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timer
from View.Tools.PamhyrPlot import PamhyrPlot

from PyQt5.QtCore import (
    QCoreApplication
)

logger = logging.getLogger()

_translate = QCoreApplication.translate


class PlotDRK(PamhyrPlot):
    def __init__(self, canvas=None, trad=None, toolbar=None,
                 data=None, parent=None):
        super(PlotDRK, self).__init__(
            canvas=canvas,
            trad=trad,
            data=data,
            toolbar=toolbar,
            parent=parent
        )

        self.label_x = self._trad["rk"]
        self.label_y = self._trad["elevation"]

        self._isometric_axis = False

        self._auto_relim_update = True
        self._autoscale_update = True

    @timer
    def draw(self, highlight=None):
        self.init_axes()

        if self.data is None:
            return

        self.draw_river_bottom()
        self.draw_water()

        self.idle()
        self._init = True

    def draw_river_bottom(self):
        rk = self.data.reach.reach.get_rk()
        z_min = self.data.reach.reach.get_z_min()

        self.line_rk_zmin = self.canvas.axes.plot(
            rk, z_min,
            color=self.color_plot_river_bottom,
            lw=1.
        )

    def draw_water(self):
        if len(self.data) != 0:
            rk = self.data.get_rk()
            elevation = self.data.get_elevation()

            sorted_rk, sorted_elevation = zip(
                *sorted(zip(rk, elevation))
            )

            self.line_rk_elevation = self.canvas.axes.plot(
                sorted_rk, sorted_elevation,
                color=self.color_plot_river_water,
                **self.plot_default_kargs
            )

            z_min = self.data.reach.reach.get_z_min()
            geometry_rk = self.data.reach.reach.get_rk()
            sorted_geometry_rk, sorted_z_min = zip(
                *sorted(zip(geometry_rk, z_min), reverse=True)
            )

            poly_x = sorted_rk + sorted_geometry_rk
            poly_y = sorted_elevation + sorted_z_min

            self.collection = self.canvas.axes.fill(
                poly_x, poly_y,
                color=self.color_plot_river_water_zone,
                alpha=0.7,
            )

    @timer
    def update(self, ind=None):
        self.draw()
