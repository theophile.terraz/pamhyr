# DialogHeight.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QComboBox, QUndoStack, QShortcut,
    QDoubleSpinBox, QCheckBox, QLabel
)


class HeightDialog(PamhyrDialog):
    _pamhyr_ui = "InitialConditions_Dialog_Generator_Height"
    _pamhyr_name = "Height"

    def __init__(self, values, option, trad=None, parent=None):
        super(HeightDialog, self).__init__(
            title=trad[self._pamhyr_name],
            options=[],
            trad=trad,
            parent=parent
        )

        self.values = values
        self.option = option
        self.sb1 = self.find(QDoubleSpinBox, "doubleSpinBox_1")
        self.sb1.setValue(self.values[0])
        self.sb2 = self.find(QDoubleSpinBox, "doubleSpinBox_2")
        self.sb2.setValue(self.values[1])
        self.sb3 = self.find(QDoubleSpinBox, "doubleSpinBox_3")
        self.sb3.setValue(self.values[2])
        self.cb = self.find(QCheckBox, "checkBox")
        self.cb.setChecked(self.option)
        self.enable_discharge()
        self.cb.clicked.connect(self.enable_discharge)

    def enable_discharge(self):
        label = self.find(QLabel, "label_3")
        self.sb3.setEnabled(self.cb.isChecked())
        label.setEnabled(self.cb.isChecked())

    def accept(self):
        self.values[0] = self.sb1.value()
        self.values[1] = self.sb2.value()
        self.values[2] = self.sb3.value()
        self.option = self.cb.isChecked()
        super().accept()

    def reject(self):
        self.close()
