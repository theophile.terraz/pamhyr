# DialogDischarge.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QComboBox, QUndoStack, QShortcut,
    QDoubleSpinBox, QCheckBox,
)


class DischargeDialog(PamhyrDialog):
    _pamhyr_ui = "InitialConditions_Dialog_Generator_Discharge"
    _pamhyr_name = "Discharge"

    def __init__(self,
                 value,
                 option,
                 title="Discharge",
                 trad=None,
                 parent=None):
        super(DischargeDialog, self).__init__(
            title=trad[self._pamhyr_name],
            options=[],
            trad=trad,
            parent=parent
        )

        self.value = value
        self.option = option
        self.sb = self.find(QDoubleSpinBox, "doubleSpinBox")
        self.sb.setValue(self.value)
        self.cb = self.find(QCheckBox, "checkBox")
        self.cb.setChecked(self.option)

    def accept(self):
        self.value = self.sb.value()
        self.option = self.cb.isChecked()
        super().accept()

    def reject(self):
        self.close()
