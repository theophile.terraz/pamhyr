# Translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate
from View.Geometry.Translate import GeometryTranslate

_translate = QCoreApplication.translate


class GeometryProfileTranslate(GeometryTranslate):
    def __init__(self):
        super(GeometryProfileTranslate, self).__init__()

        self._dict["Geometry cross-section"] = _translate(
            "Geometry", "Geometry cross-section"
        )

        self._dict["width"] = _translate("Geometry", "Width")
        self._dict["area"] = _translate("Geometry", "Area")
        self._dict["perimeter"] = _translate("Geometry", "Perimeter")

        self._sub_dict["table_headers"] = {
            "x": self._dict["x"],
            "y": self._dict["y"],
            "z": self._dict["z"],
            "name": self._dict["name"],
            "abs": _translate("Geometry", "Traversal abs (m)"),
        }
