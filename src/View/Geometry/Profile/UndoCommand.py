# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Geometry.Profile import Profile


class SetDataCommand(QUndoCommand):
    def __init__(self, profile, index, old_value, new_value):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._index = index
        self._old = old_value
        self._new = self.type(new_value)


class SetXCommand(SetDataCommand):
    def __init__(self, reach, index, old_value, new_value):
        self.type = float
        super(SetXCommand, self).__init__(reach, index, old_value, new_value)

    def undo(self):
        self._profile.point(self._index).x = self._old
        self._profile.modified()

    def redo(self):
        self._profile.point(self._index).x = self._new
        self._profile.modified()


class SetYCommand(SetDataCommand):
    def __init__(self, reach, index, old_value, new_value):
        self.type = float
        super(SetYCommand, self).__init__(reach, index, old_value, new_value)

    def undo(self):
        self._profile.point(self._index).y = self._old
        self._profile.modified()

    def redo(self):
        self._profile.point(self._index).y = self._new
        self._profile.modified()


class SetZCommand(SetDataCommand):
    def __init__(self, reach, index, old_value, new_value):
        self.type = float
        super(SetZCommand, self).__init__(reach, index, old_value, new_value)

    def undo(self):
        self._profile.point(self._index).z = self._old
        self._profile.modified()

    def redo(self):
        self._profile.point(self._index).z = self._new
        self._profile.modified()


class SetNameCommand(SetDataCommand):
    def __init__(self, reach, index, old_value, new_value):
        self.type = str
        super(SetNameCommand, self).__init__(
            reach, index, old_value, new_value)

    def undo(self):
        self._profile.point(self._index).name = self._old

    def redo(self):
        self._profile.point(self._index).name = self._new


class AddCommand(QUndoCommand):
    def __init__(self, profile, index):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._index = index
        self._point = None

    def undo(self):
        self._profile.delete_i([self._index])
        self._profile.modified()

    def redo(self):
        if self._point is None:
            self._point = self._profile.insert(self._index)
        else:
            self._profile.insert_point(self._index, self._point)
        self._profile.modified()


class DelCommand(QUndoCommand):
    def __init__(self, profile, rows):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._rows = rows

        self._points = []
        for row in rows:
            self._points.append((row, self._profile.point(row)))
        self._points.sort()     # Sort by row index

    def undo(self):
        for row, point in self._points:
            self._profile.insert_point(row, point)
        self._profile.modified()

    def redo(self):
        self._profile.delete_i(self._rows)
        self._profile.modified()


class SortCommand(QUndoCommand):
    def __init__(self, profile, column, _reverse):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._column = column
        self._reverse = _reverse

        old = self._profile.points
        self._profile.sort(self._column, self._reverse)
        new = self._profile.points

        self._indexes = list(
            map(
                lambda p: old.index(p),
                new
            )
        )

    def undo(self):
        self._profile.sort_with_indexes(self._indexes)
        self._profile.modified()

    def redo(self):
        self._profile.sort(self._column, self._reverse)
        self._profile.modified()


class MoveCommand(QUndoCommand):
    def __init__(self, profile, up, i):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._profile.move_up_point(self._i)
        else:
            self._profile.move_down_point(self._i)
        self._profile.modified()

    def redo(self):
        if self._up:
            self._profile.move_up_point(self._i)
        else:
            self._profile.move_down_point(self._i)
        self._profile.modified()


class ReverseCommand(QUndoCommand):
    def __init__(self, profile):
        QUndoCommand.__init__(self)

        self._profile = profile

    def undo(self):
        self._profile.reverse()
        self._profile.modified()

    def redo(self):
        self._profile.reverse()
        self._profile.modified()


class PurgeCommand(QUndoCommand):
    def __init__(self, profile, np_purge):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._old = self._profile.points.copy()
        self._np_purge = np_purge

    def undo(self):
        self._profile._points = self._old.copy()
        self._profile.modified()

    def redo(self):
        self._profile.purge(self._np_purge)
        self._profile.modified()


class PasteCommand(QUndoCommand):
    def __init__(self, profile, row, points):
        QUndoCommand.__init__(self)

        self._profile = profile
        self._row = row
        self._points = points

        self._points.reverse()

    def undo(self):
        for ind in range(len(self._points)):
            self._profile.delete_i([self._row])
        self._profile.modified()

    def redo(self):
        for point in self._points:
            self._profile.insert_point(self._row, point)
        self._profile.modified()
