# ShiftDialog.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog


class ShiftDialog(PamhyrDialog):
    _pamhyr_ui = "GeometryReachShift"
    _pamhyr_name = "Shift"

    def __init__(self, trad=None, parent=None):
        super(ShiftDialog, self).__init__(
            title=trad[self._pamhyr_name],
            trad=trad,
            options=[],
            parent=parent
        )

        self._init_default_values()

    def _init_default_values(self):
        self._dx = 0.0
        self._dy = 0.0
        self._dz = 0.0

    @property
    def dx(self):
        return self._dx

    @property
    def dy(self):
        return self._dy

    @property
    def dz(self):
        return self._dz

    def accept(self):
        self._dx = self.get_double_spin_box("doubleSpinBox_X")
        self._dy = self.get_double_spin_box("doubleSpinBox_Y")
        self._dz = self.get_double_spin_box("doubleSpinBox_Z")
        super().accept()

    def reject(self):
        self.close()
