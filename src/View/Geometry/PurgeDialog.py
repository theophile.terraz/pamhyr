# PurgeDialog.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
)

from PyQt5.QtWidgets import (
    QUndoStack, QShortcut, QSpinBox,
)


class PurgeDialog(PamhyrDialog):
    _pamhyr_ui = "PurgeOptions"
    _pamhyr_name = "Purge"

    def __init__(self, trad=None, parent=None):
        super(PurgeDialog, self).__init__(
            title=trad[self._pamhyr_name],
            trad=trad,
            options=[],
            parent=parent
        )

        self._init_default_values()

    def _init_default_values(self):
        self._np_purge = 24

    @property
    def np_purge(self):
        return self._np_purge

    def accept(self):
        self._np_purge = self.get_spin_box("spinBox_np_purge")
        super().accept()

    def reject(self):
        self.close()
