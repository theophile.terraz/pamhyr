# UpdateRKDialog.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QComboBox, QUndoStack, QShortcut,
    QDoubleSpinBox, QButtonGroup,
)


class UpdateRKDialog(PamhyrDialog):
    _pamhyr_ui = "UpdateRKOptions"
    _pamhyr_name = "UpdateRK"

    def __init__(self, reach, trad=None, parent=None):
        super(UpdateRKDialog, self).__init__(
            title=trad[self._pamhyr_name],
            trad=trad,
            options=[],
            parent=parent
        )

        self._reach = reach
        self._profiles = None

        self._init_default_values()

    def _init_default_values(self):
        gl, _ = self._reach.compute_guidelines()
        self._gl = list(gl)

        self._begin_dir = "un"
        self._end_dir = "np"

        lower_gl = list(map(str.lower, self._gl))
        for i, gl in enumerate(lower_gl):
            if gl == "rg":
                self._begin_dir = self._gl[i]
            elif gl == "rd":
                self._end_dir = self._gl[i]

        self._orientation = 0
        self._origin = self._reach.profile(0)
        self._origin_value = self._reach.profile(0).rk

        self._init_default_values_profiles()
        self._init_default_values_guidelines()

    def _init_default_values_profiles(self):
        profiles = self.profiles

        self.combobox_add_items("comboBox_origin", profiles)

        self.find(QComboBox, "comboBox_origin")\
            .currentIndexChanged\
            .connect(
                self.changed_profile
            )

        buttonbox = self.find(QButtonGroup, "buttonGroup_orientation")

        for button in buttonbox.buttons():
            name = button.objectName()
            i = int(name.split('_')[-1])

            buttonbox.setId(button, i)

    def changed_profile(self):
        origin = self.get_combobox_text("comboBox_origin")
        self.set_double_spin_box(
            "doubleSpinBox_origin",
            self._reach.profile(self.profiles.index(origin)).rk
        )

    @property
    def profiles(self):
        if self._profiles is None:
            self._profiles = list(
                map(
                    lambda p: self._profile_name(p),
                    self._reach.profiles
                )
            )

        return self._profiles

    def _profile_name(self, profile):
        name = profile.name

        if name == "":
            name = f"{profile.rk}"
        else:
            name += f" ({profile.rk})"

        return name

    def _init_default_values_guidelines(self):
        bgl = ['un'] + self._gl + ['np']
        egl = ['un'] + self._gl + ['np']

        self.combobox_add_items("comboBox_begin_gl", bgl)
        self.combobox_add_items("comboBox_end_gl", egl)

        self.set_combobox_text("comboBox_begin_gl", self._begin_dir)
        self.set_combobox_text("comboBox_end_gl", self._end_dir)

    @property
    def orientation(self):
        return self._orientation

    @property
    def origin(self):
        return self._origin

    @property
    def origin_value(self):
        return self._origin_value

    @property
    def begin_dir(self):
        return self._begin_dir

    @property
    def end_dir(self):
        return self._end_dir

    def accept(self):
        origin = self.get_combobox_text("comboBox_origin")
        self._origin = self.profiles.index(origin)
        self._origin_value = self.get_double_spin_box("doubleSpinBox_origin")
        self._begin_dir = self.get_combobox_text("comboBox_begin_gl")
        self._end_dir = self.get_combobox_text("comboBox_end_gl")
        self._orientation = self.get_checked_id_button_group(
            "buttonGroup_orientation"
        )

        super().accept()

    def reject(self):
        self.close()
