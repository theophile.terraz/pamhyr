# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer, logger_exception

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Geometry import Reach
from Model.Except import exception_message_box

from Meshing.Mage import MeshingWithMage

logger = logging.getLogger()


class SetDataCommand(QUndoCommand):
    def __init__(self, reach, index, old_value, new_value):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._index = index
        self._old = old_value
        self._new = self.type(new_value)


class SetNameCommand(SetDataCommand):
    def __init__(self, reach, index, old_value, new_value):
        self.type = str
        super(SetNameCommand, self).__init__(
            reach, index, old_value, new_value)

    def undo(self):
        self._reach.profile(self._index).name = self._old

    def redo(self):
        self._reach.profile(self._index).name = self._new


class SetRKCommand(SetDataCommand):
    def __init__(self, reach, index, old_value, new_value):
        self.type = float
        super(SetRKCommand, self).__init__(reach, index, old_value, new_value)

    def undo(self):
        self._reach.profile(self._index).rk = self._old

    def redo(self):
        self._reach.profile(self._index).rk = self._new


class AddCommand(QUndoCommand):
    def __init__(self, reach, index):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._index = index
        self._profile = None

    def undo(self):
        self._reach.delete_profiles([self._profile])

    def redo(self):
        if self._profile is None:
            self._profile = self._reach.insert(self._index)
        else:
            self._reach.insert_profile(self._index, self._profile)


class DelCommand(QUndoCommand):
    def __init__(self, reach, rows):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._rows = rows

        self._profiles = []
        for row in rows:
            self._profiles.append((row, self._reach.profile(row)))
        self._profiles.sort()

    def undo(self):
        for row, profile in self._profiles:
            self._reach.insert_profile(row, profile)

    def redo(self):
        self._reach.delete(self._rows)


class SortCommand(QUndoCommand):
    def __init__(self, reach, _reverse):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._reverse = _reverse

        old = self._reach.profiles
        self._reach.sort(self._reverse)
        new = self._reach.profiles

        self._indexes = list(
            map(
                lambda p: old.index(p),
                new
            )
        )

    def undo(self):
        self._reach.sort_with_indexes(self._indexes)

    def redo(self):
        self._reach.sort(self._reverse)


class MoveCommand(QUndoCommand):
    def __init__(self, reach, up, i):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._reach.move_up_profile(self._i)
        else:
            self._reach.move_down_profile(self._i)

    def redo(self):
        if self._up:
            self._reach.move_up_profile(self._i)
        else:
            self._reach.move_down_profile(self._i)


class PasteCommand(QUndoCommand):
    def __init__(self, reach, row, profiles):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._row = row
        self._profiles = list(
            map(
                lambda p: deepcopy(p),
                profiles
            )
        )
        self._profiles.reverse()

    def undo(self):
        self._reach.delete_profiles(self._profiles)

    def redo(self):
        for profile in self._profiles:
            self._reach.insert_profile(self._row, profile)


class DuplicateCommand(QUndoCommand):
    def __init__(self, reach, rows, profiles):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._rows = rows
        self._profiles = list(
            map(
                lambda p: deepcopy(p),
                profiles
            )
        )
        self._profiles.reverse()

    def undo(self):
        self._reach.delete_profiles(self._profiles)

    def redo(self):
        for profile in self._profiles:
            self._reach.insert_profile(self._rows[0], profile)


class ImportCommand(QUndoCommand):
    def __init__(self, reach, row, filename):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._row = row
        self._filename = filename
        self._profiles = None
        self._old_profiles = []
        for row in range(len(self._reach)):
            self._old_profiles.append((self._reach.profile(row)))
        self._old_profiles.reverse()

    def undo(self):
        self._reach.delete_profiles(self._profiles)
        for profile in self._old_profiles:
            self._reach.insert_profile(self._row, profile)

    def redo(self):
        if self._profiles is None:
            self._reach.delete_profiles(self._old_profiles)
            try:
                self._profiles = self._reach.import_geometry(self._filename)
                self._profiles.reverse()
            except Exception as e:
                for profile in self._old_profiles:
                    self._reach.insert_profile(self._row, profile)
                logger_exception(e)
                exception_message_box(e)
        else:
            self._reach.delete_profiles(self._old_profiles)
            for profile in self._profiles:
                self._reach.insert_profile(self._row, profile)


class MeshingCommand(QUndoCommand):
    def __init__(self, reach, mesher, data, command):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._data = data
        self._mesher = mesher
        self._command = command

        self._profiles = reach.profiles.copy()
        self._profiles.reverse()

        self._new_profiles = None

    def undo(self):
        self._reach.purge()

        for profile in self._profiles:
            self._reach.insert_profile(0, profile)

    def redo(self):
        if self._new_profiles is None:
            if self._command == "update_rk":
                self._mesher.update_rk(
                    self._reach,
                    **self._data
                )
            else:
                self._mesher.meshing(
                    self._reach,
                    **self._data
                )

            self._new_profiles = self._reach.profiles.copy()
            self._new_profiles.reverse()
        else:
            self._reach.purge()

            for profile in self._new_profiles:
                self._reach.insert_profile(0, profile)


class PurgeCommand(QUndoCommand):
    def __init__(self, reach, np_purge):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._np_purge = np_purge

        self._old = []
        for profile in self._reach.profiles:
            self._old.append(profile.points.copy())

    def undo(self):
        for i in range(self._reach.number_profiles):
            self._reach.profiles[i]._points = self._old[i].copy()

    def redo(self):
        for profile in self._reach._profiles:
            profile.purge(self._np_purge)


class ShiftCommand(QUndoCommand):
    def __init__(self, reach, rows, dx, dy, dz):
        QUndoCommand.__init__(self)

        self._reach = reach
        self._rows = rows
        self._dx = dx
        self._dy = dy
        self._dz = dz

        self._old = []
        for profile in self._reach.profiles:
            self._old.append(profile.points.copy())

    def undo(self):
        for i in self._rows:
            profile = self._reach.profiles[i]
            self._reach.profiles[i].shift(-self._dx,
                                          -self._dy,
                                          -self._dz)

    def redo(self):
        for i in self._rows:
            profile = self._reach.profiles[i]
            self._reach.profiles[i].shift(self._dx,
                                          self._dy,
                                          self._dz)
