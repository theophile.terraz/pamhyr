# Translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class GeometryTranslate(MainTranslate):
    def __init__(self):
        super(GeometryTranslate, self).__init__()

        self._dict["Geometry"] = _translate(
            "Geometry", "Geometry"
        )

        self._dict["open_file"] = _translate("Geometry", "Open a file")
        self._dict["file_st"] = _translate(
            "Geometry", "Mage geometry file (*.ST *.st)")
        self._dict["file_m"] = _translate(
            "Geometry", "Mage meshed geometry file (*.M *.m)")
        self._dict["file_shp"] = _translate(
            "Geometry", "Shapefile (*.SHP *.shp)")
        self._dict["file_all"] = _translate("Geometry", "All files (*)")

        self._dict["cross_section"] = _translate("Geometry", "cross-section")
        self._dict["cross_sections"] = _translate("Geometry", "cross-sections")
        self._dict["profile"] = _translate("Geometry", "cross-section")
        self._dict["profiles"] = _translate("Geometry", "cross-sections")

        self._dict["transverse_abscissa"] = _translate(
            "Geometry", "Transverse abscissa (m)"
        )

        self._dict["prev_cs"] = _translate(
            "Geometry", "Previous cross-section"
        )
        self._dict["cs"] = _translate("Geometry", "Cross-section")
        self._dict["next_cs"] = _translate("Geometry", "Next cross-section")

        self._dict["x"] = _translate("Geometry", "X (m)")
        self._dict["y"] = _translate("Geometry", "Y (m)")
        self._dict["z"] = _translate("Geometry", "Z (m)")

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "rk": self._dict["unit_rk"],
            "poins": _translate("Geometry", "Points"),
        }

        self._sub_dict["lm_dict"] = {
            "1": _translate("Geometry", "the first guide-line"),
            "2": _translate("Geometry", "the second guide-line"),
            "3": _translate(
                "Geometry",
                "the mean over the two guide-lines"
            ),
        }

        self._sub_dict["r_lm_dict"] = {}
        for k in self._sub_dict["lm_dict"]:
            v = self._sub_dict["lm_dict"][k]
            self._sub_dict["r_lm_dict"][v] = k

        self._dict["Meshing"] = _translate(
            "Geometry", "Meshing"
        )
        self._dict["UpdateRK"] = _translate(
            "Geometry", "UpdateRK"
        )
        self._dict["Purge"] = _translate(
            "Geometry", "Purge"
        )
        self._dict["Shift"] = _translate(
            "Geometry", "Shift"
        )
