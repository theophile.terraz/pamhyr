# Table.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging
import traceback

from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox,
)

from View.LateralContribution.UndoCommand import (
    SetNameCommand, SetEdgeCommand, SetTypeCommand,
    SetBeginCommand, SetEndCommand,
    AddCommand, DelCommand, SortCommand,
    MoveCommand, PasteCommand, DuplicateCommand,
)

from Model.LateralContribution.LateralContributionTypes import (
    NotDefined, LateralContrib, Rain, Evaporation,
)

from View.Tools.PamhyrTable import PamhyrTableModel
from View.LateralContribution.translate import LC_types

logger = logging.getLogger()

_translate = QCoreApplication.translate


class ComboBoxDelegate(QItemDelegate):
    def __init__(self, data=None, mode="type", tab="",
                 trad=None, parent=None):
        super(ComboBoxDelegate, self).__init__(parent)

        self._data = data
        self._mode = mode
        self._tab = tab
        self._trad = trad

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    def createEditor(self, parent, option, index):
        self.editor = QComboBox(parent)
        long_types = self._trad.get_dict("long_types")

        if self._mode == "type":
            lst = list(
                map(
                    lambda k: long_types[k],
                    filter(
                        lambda k: self._tab in LC_types[k].compatibility(),
                        LC_types.keys()
                    )
                )
            )
            self.editor.addItems(lst)
        elif self._mode == "rk":
            if self._data is None:
                self.editor.addItems(
                    ["0"]
                )
            else:
                self.editor.addItems(
                    list(
                        map(
                            lambda p: p.display_name(),
                            self._data.reach.profiles
                        )
                    )
                )
        else:
            self.editor.addItems(
                [self._trad['not_associated']] +
                self._data.edges_names()
            )

        self.editor.setCurrentText(index.data(Qt.DisplayRole))
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(Qt.DisplayRole)
        self.editor.currentTextChanged.connect(self.currentItemChanged)

    def setModelData(self, editor, model, index):
        text = str(editor.currentText())

        if self._mode == "rk":
            profiles = list(
                filter(
                    lambda p: p.display_name() == text,
                    self._data.reach.profiles
                )
            )

            value = profiles[0].rk if len(profiles) > 0 else None
        else:
            value = text

        model.setData(index, value)
        editor.close()
        editor.deleteLater()

    def updateEditorGeometry(self, editor, option, index):
        r = QRect(option.rect)
        if self.editor.windowFlags() & Qt.Popup:
            if editor.parent() is not None:
                r.setTopLeft(self.editor.parent().mapToGlobal(r.topLeft()))
        editor.setGeometry(r)

    @pyqtSlot()
    def currentItemChanged(self):
        self.commitData.emit(self.sender())


class TableModel(PamhyrTableModel):
    def _setup_lst(self):
        self._lst = self._data.lateral_contribution
        self._tab = self._opt_data
        self._long_types = self._trad.get_dict("long_types")

    def rowCount(self, parent):
        return self._lst.len(self._tab)

    def data(self, index, role):
        if role != Qt.ItemDataRole.DisplayRole:
            return QVariant()

        row = index.row()
        column = index.column()

        if self._headers[column] == "name":
            return self._lst.get(self._tab, row).name
        elif self._headers[column] == "type":
            t = self._lst.get(self._tab, row).lctype
            return self._long_types[t]
        elif self._headers[column] == "edge":
            n = self._lst.get(self._tab, row).edge
            if n is None:
                return self._trad['not_associated']
            return n.name
        elif self._headers[column] == "begin_rk":
            return str(self._lst.get(self._tab, row).begin_rk)
        elif self._headers[column] == "end_rk":
            return str(self._lst.get(self._tab, row).end_rk)

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or role != Qt.EditRole:
            return False

        row = index.row()
        column = index.column()

        try:
            if self._headers[column] == "name":
                self._undo.push(
                    SetNameCommand(
                        self._lst, self._tab, row, value
                    )
                )
            elif self._headers[column] == "type":
                key = next(k for k, v in self._long_types.items()
                           if v == value)
                self._undo.push(
                    SetTypeCommand(
                        self._lst, self._tab, row, LC_types[key]
                    )
                )
            elif self._headers[column] == "edge":
                self._undo.push(
                    SetEdgeCommand(
                        self._lst, self._tab, row, self._data.edge(value)
                    )
                )
            elif self._headers[column] == "begin_rk":
                self._undo.push(
                    SetBeginCommand(
                        self._lst, self._tab, row, value
                    )
                )
            elif self._headers[column] == "end_rk":
                self._undo.push(
                    SetEndCommand(
                        self._lst, self._tab, row, value
                    )
                )
        except Exception as e:
            logger.info(e)
            logger.debug(traceback.format_exc())

        self.dataChanged.emit(index, index)
        return True

    def add(self, row, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row - 1)

        self._undo.push(
            AddCommand(
                self._lst, self._tab, row
            )
        )

        self.endInsertRows()
        self.layoutChanged.emit()

    def delete(self, rows, parent=QModelIndex()):
        self.beginRemoveRows(parent, rows[0], rows[-1])

        self._undo.push(
            DelCommand(
                self._lst, self._tab, rows
            )
        )

        self.endRemoveRows()
        self.layoutChanged.emit()

    def sort(self, _reverse, parent=QModelIndex()):
        self.layoutAboutToBeChanged.emit()

        self._undo.push(
            SortCommand(
                self._lst, self._tab, False
            )
        )

        self.layoutAboutToBeChanged.emit()
        self.layoutChanged.emit()

    def move_up(self, row, parent=QModelIndex()):
        if row <= 0:
            return

        target = row + 2

        self.beginMoveRows(parent, row - 1, row - 1, parent, target)

        self._undo_stack.push(
            MoveCommand(
                self._lst, self._tab, "up", row
            )
        )

        self.endMoveRows()
        self.layoutChanged.emit()

    def move_down(self, index, parent=QModelIndex()):
        if row > len(self._lst):
            return

        target = row

        self.beginMoveRows(parent, row + 1, row + 1, parent, target)

        self._undo_stack.push(
            MoveCommand(
                self._lst, self._tab, "down", row
            )
        )

        self.endMoveRows()
        self.layoutChanged.emit()
