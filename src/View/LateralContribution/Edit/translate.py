# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate
from View.LateralContribution.translate import LCTranslate

_translate = QCoreApplication.translate


class LCETranslate(LCTranslate):
    def __init__(self):
        super(LCETranslate, self).__init__()
        self._dict["Edit lateral contribution"] = _translate(
            "LateralContribution", "Edit lateral sources"
        )

        self._sub_dict["table_headers"] = {
            "x": _translate("LateralContribution", "X"),
            "y": _translate("LateralContribution", "Y"),
            "time": self._dict["time"],
            "date": self._dict["date"],
            "discharge": self._dict["unit_discharge"],
            "z": self._dict["unit_elevation"],
        }
