# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
)

from View.LateralContribution.UndoCommand import (
    SetNameCommand, SetEdgeCommand, SetTypeCommand,
    AddCommand, DelCommand, SortCommand,
    MoveCommand, PasteCommand, DuplicateCommand,
)

from Model.LateralContribution.LateralContributionTypes import (
    NotDefined, LateralContrib, Rain, Evaporation,
)

from View.LateralContribution.Table import (
    TableModel, ComboBoxDelegate
)

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.LateralContribution.PlotXY import PlotXY
from View.LateralContribution.translate import (
    LC_types, LCTranslate,
)
from View.LateralContribution.Edit.Window import EditLateralContributionWindow

logger = logging.getLogger()


class LateralContributionWindow(PamhyrWindow):
    _pamhyr_ui = "LateralContributions"
    _pamhyr_name = "Lateral contribution"

    def __init__(self, study=None, config=None, parent=None):
        trad = LCTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(LateralContributionWindow, self).__init__(
            title=name,
            study=study,
            trad=trad,
            config=config,
            parent=parent
        )

        self._lcs = self._study.river.lateral_contribution

        self.setup_table()
        self.setup_graph()
        self.tabs = self.find(QTabWidget, "tabWidget")
        self.setup_connections()

    def setup_table(self):
        self._table = {}

        self._delegate_rk = []

        for t in ["liquid", "solid", "suspenssion"]:
            self._delegate_type = ComboBoxDelegate(
                data=self._study.river,
                mode="type",
                tab=t,
                trad=self._trad,
                parent=self
            )

            delegate_rk = ComboBoxDelegate(
                data=None,
                mode="rk",
                tab=t,
                trad=self._trad,
                parent=self
            )
            self._delegate_rk.append(delegate_rk)

            self._delegate_edge = ComboBoxDelegate(
                data=self._study.river,
                mode="edge",
                tab=t,
                trad=self._trad,
                parent=self
            )

            table = self.find(QTableView, f"tableView_{t}")
            self._table[t] = TableModel(
                table_view=table,
                table_headers=self._trad.get_dict("table_headers"),
                editable_headers=self._trad.get_dict("table_headers"),
                delegates={
                    "type": self._delegate_type,
                    "edge": self._delegate_edge,
                    "begin_rk": delegate_rk,
                    "end_rk": delegate_rk,
                },
                data=self._study.river,
                undo=self._undo_stack,
                trad=self._trad,
                opt_data=t,
            )
            table.setModel(self._table[t])
            table.setSelectionBehavior(QAbstractItemView.SelectRows)
            table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
            table.setAlternatingRowColors(True)

    def setup_graph(self):
        self.canvas = MplCanvas(width=5, height=4, dpi=100)
        self.canvas.setObjectName("canvas")
        self.plot_layout = self.find(QVBoxLayout, "verticalLayout")
        self.plot_layout.addWidget(self.canvas)

        self.plot = PlotXY(
            canvas=self.canvas,
            data=None,
            trad=self._trad,
            toolbar=None,
            parent=self
        )

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_del").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)
        self.find(QAction, "action_sort").triggered.connect(self.sort)

        self.tabs.currentChanged.connect(self._set_current_reach)

        for t in ["liquid", "solid", "suspenssion"]:
            table = self.find(QTableView, f"tableView_{t}")
            table.selectionModel()\
                 .selectionChanged\
                 .connect(self._set_current_reach)

            self._table[t].dataChanged\
                          .connect(self._set_current_reach)

    def current_tab(self):
        return self.find(QTabWidget, "tabWidget")\
                   .currentWidget()\
                   .objectName()\
                   .replace("tab_", "")

    def index_selected_row(self):
        tab = self.current_tab()
        table = self.find(QTableView, f"tableView_{tab}")
        return table.selectionModel()\
                    .selectedRows()[0]\
                    .row()

    def index_selected_rows(self):
        tab = self.current_tab()
        table = self.find(QTableView, f"tableView_{tab}")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def _set_current_reach(self):
        tab = self.current_tab()
        rows = self.index_selected_rows()

        data = None
        highlight = None

        if len(rows) > 0:
            edge = self._study.river\
                       .lateral_contribution\
                       .get(tab, rows[0])\
                       .edge
            if edge:
                data = edge.reach
                lc = self._lcs.get(tab, rows[0])
                highlight = (lc.begin_rk, lc.end_rk)

            for delegate in self._delegate_rk:
                delegate.data = edge

        self.plot = PlotXY(
            canvas=self.canvas,
            data=data,
            trad=self._trad,
            toolbar=None,
            parent=self
        )
        self.plot.highlight = highlight
        self.plot.update()

    def add(self):
        tab = self.current_tab()
        rows = self.index_selected_rows()
        if self._lcs.len(tab) == 0 or len(rows) == 0:
            self._table[tab].add(0)
        else:
            self._table[tab].add(rows[0])

        self._set_current_reach()

    def delete(self):
        tab = self.current_tab()
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table[tab].delete(rows)
        self._set_current_reach()

    def sort(self):
        tab = self.current_tab()
        self._table[tab].sort(False)
        self._set_current_reach()

    def move_up(self):
        tab = self.current_tab()
        row = self.index_selected_row()
        self._table[tab].move_up(row)
        self._set_current_reach()

    def move_down(self):
        tab = self.current_tab()
        row = self.index_selected_row()
        self._table[tab].move_down(row)
        self._set_current_reach()

    def _copy(self):
        logger.info("TODO: copy")
        self._set_current_reach()

    def _paste(self):
        logger.info("TODO: paste")
        self._set_current_reach()

    def _undo(self):
        tab = self.current_tab()
        self._table[tab].undo()
        self._set_current_reach()

    def _redo(self):
        tab = self.current_tab()
        self._table[tab].redo()
        self._set_current_reach()

    def edit(self):
        tab = self.current_tab()
        rows = self.index_selected_rows()
        for row in rows:
            data = self._lcs.get(tab, row)

            if self.sub_window_exists(
                EditLateralContributionWindow,
                data=[self._study, None, data]
            ):
                continue

            win = EditLateralContributionWindow(
                data=data,
                study=self._study,
                parent=self
            )
            win.show()
