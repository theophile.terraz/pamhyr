# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

from Model.LateralContribution.LateralContributionTypes import (
    NotDefined, LateralContrib, Rain, Evaporation,
)

_translate = QCoreApplication.translate

LC_types = {
    "ND": NotDefined,
    "LC": LateralContrib,
    "RA": Rain,
    "EV": Evaporation,
}


class LCTranslate(MainTranslate):
    def __init__(self):
        super(LCTranslate, self).__init__()

        self._dict["Lateral contribution"] = _translate(
            "LateralContribution", "Lateral sources"
        )

        self._sub_dict["long_types"] = {
            "ND": self._dict["not_defined"],
            "LC": _translate("LateralContribution", "Lateral sources"),
            "RA": _translate("LateralContribution", "Rain"),
            "EV": _translate("LateralContribution", "Evaporation"),
        }

        self._dict["x"] = _translate("Geometry", "X (m)")
        self._dict["y"] = _translate("Geometry", "Y (m)")
        self._dict["z"] = _translate("Geometry", "Z (m)")

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "type": self._dict["type"],
            "edge": self._dict["reach"],
            "begin_rk": _translate("LateralContribution", "Start (m)"),
            "end_rk": _translate("LateralContribution", "End (m)")
        }
