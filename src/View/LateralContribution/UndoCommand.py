# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.LateralContribution.LateralContribution import LateralContribution
from Model.LateralContribution.LateralContributionList import (
    LateralContributionList
)


class SetNameCommand(QUndoCommand):
    def __init__(self, lcs, tab, index, new_value):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._index = index
        self._old = self._lcs.get(self._tab, self._index).name
        self._new = str(new_value)

    def undo(self):
        self._lcs.get(self._tab, self._index).name = self._old

    def redo(self):
        self._lcs.get(self._tab, self._index).name = self._new


class SetBeginCommand(QUndoCommand):
    def __init__(self, lcs, tab, index, new_value):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._index = index
        self._old = self._lcs.get(self._tab, self._index).begin_rk
        self._new = float(new_value)

    def undo(self):
        self._lcs.get(self._tab, self._index).begin_rk = float(self._old)

    def redo(self):
        self._lcs.get(self._tab, self._index).begin_rk = float(self._new)


class SetEndCommand(QUndoCommand):
    def __init__(self, lcs, tab, index, new_value):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._index = index
        self._old = self._lcs.get(self._tab, self._index).end_rk
        self._new = float(new_value)

    def undo(self):
        self._lcs.get(self._tab, self._index).end_rk = float(self._old)

    def redo(self):
        self._lcs.get(self._tab, self._index).end_rk = float(self._new)


class SetEdgeCommand(QUndoCommand):
    def __init__(self, lcs, tab, index, edge):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._index = index
        self._old = self._lcs.get(self._tab, self._index).edge
        self._new = edge

    def undo(self):
        self._lcs.get(self._tab, self._index).edge = self._old

    def redo(self):
        self._lcs.get(self._tab, self._index).edge = self._new


class SetTypeCommand(QUndoCommand):
    def __init__(self, lcs, tab, index, _type):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._index = index
        self._type = _type
        self._old = self._lcs.get(self._tab, self._index)
        self._new = self._lcs.get(self._tab, self._index)\
                             .convert(self._type)

    def undo(self):
        self._lcs.set(self._tab, self._index, self._old)

    def redo(self):
        self._lcs.set(self._tab, self._index, self._new)


class AddCommand(QUndoCommand):
    def __init__(self, lcs, tab, index):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._index = index
        self._new = None

    def undo(self):
        self._lcs.delete_i(self._tab, [self._index])

    def redo(self):
        if self._new is None:
            self._new = self._lcs.new(self._tab, self._index)
        else:
            self._lcs.insert(self._tab, self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, lcs, tab, rows):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._rows = rows

        self._bc = []
        for row in rows:
            self._bc.append((row, self._lcs.get(self._tab, row)))
        self._bc.sort()

    def undo(self):
        for row, el in self._bc:
            self._lcs.insert(self._tab, row, el)

    def redo(self):
        self._lcs.delete_i(self._tab, self._rows)


class SortCommand(QUndoCommand):
    def __init__(self, lcs, tab, _reverse):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._reverse = _reverse

        self._old = self._lcs.get_tab(self._tab)
        self._indexes = None

    def undo(self):
        ll = self._lcs.get_tab(self._tab)
        self._lcs.sort(
            self._tab,
            key=lambda x: self._indexes[ll.index(x)]
        )

    def redo(self):
        self._lcs.sort(
            self._tab,
            reverse=self._reverse,
            key=lambda x: x.name
        )
        if self._indexes is None:
            self._indexes = list(
                map(
                    lambda p: self._old.index(p),
                    self._lcs.get_tab(self._tab)
                )
            )
            self._old = None


class MoveCommand(QUndoCommand):
    def __init__(self, lcs, tab, up, i):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._lcs.move_up(self._tab, self._i)
        else:
            self._lcs.move_down(self._tab, self._i)

    def redo(self):
        if self._up:
            self._lcs.move_up(self._tab, self._i)
        else:
            self._lcs.move_down(self._tab, self._i)


class PasteCommand(QUndoCommand):
    def __init__(self, lcs, tab, row, bc):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._row = row
        self._bc = deepcopy(bc)
        self._bc.reverse()

    def undo(self):
        self._lcs.delete(self._tab, self._bc)

    def redo(self):
        for bc in self._bc:
            self._lcs.insert(self._tab, self._row, bc)


class DuplicateCommand(QUndoCommand):
    def __init__(self, lcs, tab, rows, bc):
        QUndoCommand.__init__(self)

        self._lcs = lcs
        self._tab = tab
        self._rows = rows
        self._bc = deepcopy(bc)
        self._bc.reverse()

    def undo(self):
        self._lcs.delete(self._tab, self._bc)

    def redo(self):
        for bc in self._lcs:
            self._lcs.insert(self._tab, self._rows[0], bc)
