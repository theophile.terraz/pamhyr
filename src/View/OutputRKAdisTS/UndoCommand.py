# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

logger = logging.getLogger()


class SetTitleCommand(QUndoCommand):
    def __init__(self, outputrk_lst, index, new_value):
        QUndoCommand.__init__(self)

        self._outputrk_lst = outputrk_lst
        self._index = index
        self._old = self._outputrk_lst.get(self._index).title
        self._new = str(new_value)

    def undo(self):
        self._outputrk_lst.get(self._index).title = self._old

    def redo(self):
        self._outputrk_lst.get(self._index).title = self._new


class SetReachCommand(QUndoCommand):
    def __init__(self, outputrk_lst, index, reach):
        QUndoCommand.__init__(self)

        self._outputrk_lst = outputrk_lst
        self._index = index
        self._old = self._outputrk_lst.get(self._index).reach
        self._new = reach.id

    def undo(self):
        i = self._outputrk_lst.get(self._index)
        i.reach = self._old

    def redo(self):
        i = self._outputrk_lst.get(self._index)
        i.reach = self._new


class SetRKCommand(QUndoCommand):
    def __init__(self, outputrk_lst, index, rk):
        QUndoCommand.__init__(self)

        self._outputrk_lst = outputrk_lst
        self._index = index
        self._old = self._outputrk_lst.get(self._index).rk
        self._new = rk

    def undo(self):
        self._outputrk_lst.get(self._index).rk = self._old

    def redo(self):
        self._outputrk_lst.get(self._index).rk = self._new


class SetEnabledCommand(QUndoCommand):
    def __init__(self, outputrk_lst, index, enabled):
        QUndoCommand.__init__(self)

        self._outputrk_lst = outputrk_lst
        self._index = index
        self._old = not enabled
        self._new = enabled

    def undo(self):
        self._outputrk_lst.get(self._index).enabled = self._old

    def redo(self):
        self._outputrk_lst.get(self._index).enabled = self._new


class AddCommand(QUndoCommand):
    def __init__(self, outputrk_lst, index):
        QUndoCommand.__init__(self)

        self._outputrk_lst = outputrk_lst

        self._index = index
        self._new = None

    def undo(self):
        self._outputrk_lst.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._outputrk_lst.new(self._outputrk_lst, self._index)
        else:
            self._outputrk_lst.insert(self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, outputrk_lst, rows):
        QUndoCommand.__init__(self)

        self._outputrk_lst = outputrk_lst

        self._rows = rows

        self._outputrk = []
        for row in rows:
            self._outputrk.append((row, self._outputrk_lst.get(row)))
        self._outputrk.sort()

    def undo(self):
        for row, el in self._outputrk:
            self._outputrk_lst.insert(row, el)

    def redo(self):
        self._outputrk_lst.delete_i(self._rows)


class PasteCommand(QUndoCommand):
    def __init__(self, outputrk_lst, row, outputrk):
        QUndoCommand.__init__(self)

        self._outputrk_lst = outputrk_lst

        self._row = row
        self._outputrk = deepcopy(outputrk)
        self._outputrk.reverse()

    def undo(self):
        self._outputrk_lst.delete_i(
            self._tab,
            range(self._row, self._row + len(self._outputrk))
        )

    def redo(self):
        for r in self._outputrk:
            self._outputrk_lst.insert(self._row, r)
