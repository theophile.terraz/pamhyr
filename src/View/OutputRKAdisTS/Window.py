# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timer, trace

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5 import QtCore
from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel, QCoreApplication,
    pyqtSlot, pyqtSignal, QItemSelectionModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QHeaderView, QDoubleSpinBox, QVBoxLayout, QCheckBox
)

from View.Tools.Plot.PamhyrCanvas import MplCanvas
from View.Tools.Plot.PamhyrToolbar import PamhyrPlotToolbar

from View.OutputRKAdisTS.Table import (
    TableModel, ComboBoxDelegate,
)

from View.Network.GraphWidget import GraphWidget
from View.OutputRKAdisTS.Translate import OutputRKAdisTSTranslate

logger = logging.getLogger()


class OutputRKAdisTSWindow(PamhyrWindow):
    _pamhyr_ui = "OutputRKAdisTS"
    _pamhyr_name = "Output RK"

    def __init__(self, study=None, config=None, parent=None):
        trad = OutputRKAdisTSTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(OutputRKAdisTSWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._outputrk_lst = self._study.river._Output_rk_adists

        self.setup_table()
        self.setup_checkbox()
        self.setup_connections()

        self.update()

    def setup_table(self):
        self._table = None

        self._delegate_reach = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            parent=self,
            mode="reaches"
        )
        self._delegate_rk = ComboBoxDelegate(
            trad=self._trad,
            data=self._study.river,
            parent=self,
            mode="rk"
        )

        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=["title", "reach", "rk"],
            delegates={
                "reach": self._delegate_reach,
                "rk": self._delegate_rk,
            },
            trad=self._trad,
            data=self._study.river,
            undo=self._undo_stack,
        )

        selectionModel = table.selectionModel()
        index = table.model().index(0, 0)

        selectionModel.select(
            index,
            QItemSelectionModel.Rows |
            QItemSelectionModel.ClearAndSelect |
            QItemSelectionModel.Select
        )
        table.scrollTo(index)

    def setup_checkbox(self):
        self._checkbox = self.find(QCheckBox, f"checkBox")
        self._set_checkbox_state()

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self._checkbox.clicked.connect(self._set_outputrk_state)

        table = self.find(QTableView, "tableView")
        table.selectionModel()\
             .selectionChanged\
             .connect(self.update)

        self._table.dataChanged.connect(self.update)
        self._table.layoutChanged.connect(self.update)

    def index_selected(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0]
        else:
            return None

    def index_selected_row(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0].row()
        else:
            return None

    def index_selected_rows(self):
        table = self.find(QTableView, "tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        rows = self.index_selected_rows()
        if len(self._outputrk_lst) == 0 or len(rows) == 0:
            self._table.add(0)
        else:
            self._table.add(rows[0])

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()

    def _set_checkbox_state(self):
        row = self.index_selected_row()
        if row is None:
            self._checkbox.setEnabled(False)
            self._checkbox.setChecked(True)
        else:
            self._checkbox.setEnabled(True)
            self._checkbox.setChecked(self._outputrk_lst.get(row).enabled)

    def _set_outputrk_state(self):
        rows = self.index_selected_rows()
        if len(rows) != 0:
            for row in rows:
                if row is not None:
                    self._table.enabled(
                        row,
                        self._checkbox.isChecked()
                    )

    def update(self):
        self._set_checkbox_state()
