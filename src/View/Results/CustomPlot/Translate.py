# Translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Results.translate import ResultsTranslate

_translate = QCoreApplication.translate


class CustomPlotTranslate(ResultsTranslate):
    def __init__(self):
        super(CustomPlotTranslate, self).__init__()

        self._dict["Custom Plot Selection"] = _translate(
            "CustomPlot", "Custom plot selection"
        )

        # Value type

        self._dict['time'] = self._dict["unit_time_s"]
        self._dict['rk'] = self._dict["unit_rk"]
        self._dict['water_elevation'] = self._dict["unit_water_elevation"]
        self._dict['water_elevation_envelop'] = self._dict[
            "unit_water_elevation_envelop"
        ]
        self._dict['discharge'] = self._dict["unit_discharge"]
        self._dict['discharge_envelop'] = self._dict["unit_discharge_envelop"]
        self._dict['bed_elevation'] = self._dict["unit_bed_elevation"]
        self._dict['bed_elevation_envelop'] = self._dict[
            "unit_bed_elevation_envelop"
        ]
        self._dict['velocity'] = self._dict["unit_velocity"]
        self._dict['width'] = self._dict["unit_width"]
        self._dict['velocity_envelop'] = self._dict["unit_velocity_envelop"]
        self._dict['depth'] = self._dict["unit_depth"]
        self._dict['depth_envelop'] = self._dict["unit_depth_envelop"]
        self._dict['mean_depth'] = self._dict["unit_mean_depth"]
        self._dict['wet_area'] = self._dict["unit_wet_area"]
        self._dict['wet_perimeter'] = self._dict["unit_wet_perimeter"]
        self._dict['hydraulic_radius'] = self._dict["unit_hydraulic_radius"]
        self._dict['froude'] = self._dict["unit_froude"]

        # Unit corresponding long name (plot axes display)

        self._dict['0-meter'] = _translate(
            "CustomPlot", "Elevation (m)"
        )
        self._dict['1-m3s'] = self._dict["unit_discharge"]
        self._dict['2-ms'] = self._dict["unit_velocity"]
        self._dict['3-meter'] = self._dict["unit_depth"]
        self._dict['4-dimensionless'] = self._dict["unit_froude"]
        self._dict['5-m2'] = self._dict["wet_area"]
