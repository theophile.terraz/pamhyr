# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

_translate = QCoreApplication.translate


class ResultsTranslate(MainTranslate):
    def __init__(self, pollutants=None):
        if pollutants is not None:
            self.pollutants = pollutants
            if "total_sediment" in self.pollutants:
                self.pollutants.remove("total_sediment")
        else:
            self.pollutants = pollutants

        super(ResultsTranslate, self).__init__()

        self._dict["Results"] = _translate("Results", "Results")
        self._dict["Reading results"] = _translate(
            "Results", "Reading results"
        )

        self._dict['day'] = _translate("Results", "day")
        self._dict['days'] = _translate("Results", "days")

        self._dict['envelop'] = _translate("Results", "Envelop")
        self._dict['solver'] = _translate("Results", "Solver")

        self._dict['x'] = _translate("Results", "X (m)")

        self._dict['label_bottom'] = _translate("Results", "Bottom")
        self._dict['label_water'] = _translate("Results", "Water elevation")
        self._dict['label_water_max'] = _translate(
            "Results",
            "Max water elevation"
        )

        self._sub_dict["table_headers_reach"] = {
            "name": _translate("Results", "Reach name"),
        }

        self._sub_dict["table_headers_phys_var"] = {
            "name": _translate("Results", "Variables names"),
        }

        self._sub_dict["table_headers_profile"] = {
            "name": _translate("Results", "Profile name"),
            "rk": self._dict["unit_rk"],
        }

        self._sub_dict["table_headers_solver"] = {
            "solver": self._dict['solver'],
        }

        self._sub_dict["table_headers_pollutants"] = {
            "name": _translate("Results", "Pollutant name"),
        }

        self._sub_dict["table_headers_raw_data"] = {
            "name": _translate("Results", "Profile"),
            "water_elevation": self._dict["unit_water_elevation"],
            "discharge": self._dict["unit_discharge"],
            "velocity": self._dict["unit_velocity"],
            "width": self._dict["unit_width"],
            "depth": self._dict["unit_depth"],
            "mean_depth": self._dict["unit_mean_depth"],
            "wet_area": self._dict["unit_wet_area"],
            "wet_perimeter": self._dict["unit_wet_perimeter"],
            "hydraulic_radius": self._dict["unit_hydraulic_radius"],
            "froude": self._dict["unit_froude"],
        }

        self._sub_dict["values_x"] = {
            "rk": self._dict["unit_rk"],
            "time": self._dict["unit_time_s"],
        }

        self._sub_dict["values_y"] = {
            "bed_elevation": self._dict["unit_bed_elevation"],
            "water_elevation": self._dict["unit_water_elevation"],
            "discharge": self._dict["unit_discharge"],
            "velocity": self._dict["unit_velocity"],
            "depth": self._dict["unit_depth"],
            "mean_depth": self._dict["unit_mean_depth"],
            "froude": self._dict["unit_froude"],
            "wet_area": self._dict["unit_wet_area"],
        }

        if self.pollutants is not None:
            self._sub_dict["table_headers_raw_data"] = {
                "name": _translate("Results", "Profile"),
            }
            tmp = self._sub_dict["table_headers_raw_data"]
            for pol in self.pollutants:
                tmp[pol + " Concentration"] = pol + "\n Concentration"
                tmp[pol + " Mass"] = pol + "\n Mass"
        else:
            self._sub_dict["table_headers_raw_data"] = {
                "name": _translate("Results", "Profile"),
                "water_elevation": self._dict["unit_water_elevation"],
                "discharge": self._dict["unit_discharge"],
                "discharge": self._dict["unit_discharge"],
            }

        self._sub_dict["values_y_envelop"] = {
            "min_bed_elevation": self._dict["unit_min_bed_elevation"],
            "max_bed_elevation": self._dict["unit_max_bed_elevation"],
            "min_water_elevation": self._dict["unit_min_water_elevation"],
            "max_water_elevation": self._dict["unit_max_water_elevation"],
            "min_discharge": self._dict["unit_min_discharge"],
            "max_discharge": self._dict["unit_max_discharge"],
            "min_velocity": self._dict["unit_min_velocity"],
            "max_velocity": self._dict["unit_max_velocity"],
            "min_depth": self._dict["unit_min_depth"],
            "max_depth": self._dict["unit_max_depth"],
            }

        self._sub_dict["values_y_pol"] = {
            "unit_C": self._dict["unit_concentration"],
            "unit_M": self._dict["unit_mass"],
            "unit_thickness": self._dict["unit_thickness"],
            }


class CompareResultsTranslate(ResultsTranslate):
    def __init__(self):
        super(CompareResultsTranslate, self).__init__()

        self._dict['label_water'] = u"Δ "+_translate(
            "Results", "Water elevation"
            )
        self._dict['unit_elevation'] = u"Δ "+self._dict["unit_elevation"]

        self._sub_dict["table_headers_raw_data"] = {
            "name": _translate("Results", "Profile"),
            "water_elevation": u"Δ "+self._dict["unit_water_elevation"],
            "discharge": u"Δ "+self._dict["unit_discharge"],
            "velocity": u"Δ "+self._dict["unit_velocity"],
            "width": u"Δ "+self._dict["unit_width"],
            "depth": u"Δ "+self._dict["unit_depth"],
            "mean_depth": u"Δ "+self._dict["unit_mean_depth"],
            "wet_area": u"Δ "+self._dict["unit_wet_area"],
            "wet_perimeter": u"Δ "+self._dict["unit_wet_perimeter"],
            "hydraulic_radius": u"Δ "+self._dict["unit_hydraulic_radius"],
            "froude": u"Δ "+self._dict["unit_froude"],
        }

        self._sub_dict["values_y"] = {
            "bed_elevation": u"Δ "+self._dict["unit_bed_elevation"],
            "water_elevation": u"Δ "+self._dict["unit_water_elevation"],
            "discharge": u"Δ "+self._dict["unit_discharge"],
            "velocity": u"Δ "+self._dict["unit_velocity"],
            "depth": u"Δ "+self._dict["unit_depth"],
            "mean_depth": u"Δ "+self._dict["unit_mean_depth"],
            "froude": u"Δ "+self._dict["unit_froude"],
            "wet_area": u"Δ "+self._dict["unit_wet_area"],
        }
