# CustomExport.py -- Pamhyr
# Copyright (C) 2023-2025  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtWidgets import (
    QRadioButton, QCheckBox, QVBoxLayout, QLabel,
)

from View.Results.translate import ResultsTranslate


class CustomExportDialog(PamhyrDialog):
    _pamhyr_ui = "CustomExportAdisDialog"
    _pamhyr_name = "Custom Plot Selection"

    def __init__(self, parent=None):
        trad = ResultsTranslate()
        super(CustomExportDialog, self).__init__(
            title=trad[self._pamhyr_name],
            options=[],
            trad=trad,
            parent=parent
        )

        self._available_values_x = self._trad.get_dict("values_x")
        self._available_values_y = self._trad.get_dict("values_y")

        self.setup_radio_buttons_x()
        self.setup_radio_buttons_res()
        self.setup_envelop_box()
        self.setup_check_boxes()

        self.value = None

    def setup_radio_buttons_x(self):
        self._radio = []
        layout = self.find(QVBoxLayout, "verticalLayout_x")

        for value in self._available_values_x:
            btn = QRadioButton(
                self._available_values_x[value],
                parent=self
            )
            self._radio.append((value, btn))
            layout.addWidget(btn)

        self._radio[0][1].setChecked(True)
        layout.addStretch()

    def setup_radio_buttons_res(self):
        self._radio2 = []
        layout = self.find(QVBoxLayout, "verticalLayout_pol")
        label = self.find(QLabel, "label_3")
        label.setText(self._trad["solver"])

        for solver in self.parent._solvers:
            btn = QRadioButton(
                solver.name,
                parent=self
            )
            self._radio2.append((solver.name, btn))
            layout.addWidget(btn)

        self._radio2[0][1].setChecked(True)
        if len(self.parent._results) == 1:
            self._radio2[0][1].setEnabled(False)
            label.setEnabled(False)
        else:
            self._radio2[0][1].setEnabled(True)
            label.setEnabled(True)

        layout.addStretch()

    def setup_envelop_box(self):
        layout = self.find(QVBoxLayout, "verticalLayout_x")
        self._envelop = QCheckBox(
            self._trad["envelop"],
            parent=self
        )
        layout.addWidget(self._envelop)
        self._envelop.setChecked(True)
        for r in self._radio:
            r[1].clicked.connect(self.envelop_box_status)

    def envelop_box_status(self):
        if self._radio[0][1].isChecked():
            if len(self.parent._results) > 1:
                self._envelop.setEnabled(False)
            else:
                self._envelop.setEnabled(True)
        else:
            self._envelop.setEnabled(False)

    def setup_check_boxes(self):
        self._check = []
        layout = self.find(QVBoxLayout, "verticalLayout_y")

        for value in self._available_values_y:
            btn = QCheckBox(
                self._available_values_y[value],
                parent=self
            )
            self._check.append((value, btn))
            layout.addWidget(btn)

        self._check[0][1].setChecked(True)
        layout.addStretch()

    def accept(self):
        x = next(
            filter(
                lambda r: r[1].isChecked(),
                self._radio
            )
        )[0]

        y = list(
            map(
                lambda b: b[0],
                filter(
                    lambda b: b[1].isChecked(),
                    self._check
                )
            )
        )

        res = 0
        for i, b in enumerate(self._radio2):
            if b[1].isChecked():
                res = i
                break

        self.value = x, y, self._envelop.isChecked(), res

        super().accept()
