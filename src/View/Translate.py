# Translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Tools.PamhyrTranslate import PamhyrTranslate

_translate = QCoreApplication.translate


class CommonWordTranslate(PamhyrTranslate):
    def __init__(self):
        super(CommonWordTranslate, self).__init__()

        self._dict["name"] = _translate("CommonWord", "Name")
        self._dict["title"] = _translate("CommonWord", "Title")
        self._dict["type"] = _translate("CommonWord", "Type")
        self._dict["value"] = _translate("CommonWord", "Value")
        self._dict["comment"] = _translate("CommonWord", "Comment")
        self._dict["description"] = _translate("CommonWord", "Description")

        self._dict["time"] = _translate("CommonWord", "Time")
        self._dict["date"] = _translate("CommonWord", "Date")

        self._dict["reach"] = _translate("CommonWord", "Reach")
        self._dict["reaches"] = _translate("CommonWord", "Reaches")
        self._dict["cross_section"] = _translate("CommonWord", "Cross-section")
        self._dict["main_channel"] = _translate("CommonWord", "Main channel")
        self._dict["floodway"] = _translate("CommonWord", "Floodway")

        self._dict["not_defined"] = _translate("CommonWord", "Not defined")
        self._dict["not_associated"] = _translate(
            "CommonWord", "Not associated"
        )

        self._dict["method"] = _translate("CommonWord", "Method")


class UnitTranslate(CommonWordTranslate):
    def __init__(self):
        super(UnitTranslate, self).__init__()

        self._dict["unit_rk"] = _translate("Unit", "River Kilometer (m)")
        self._dict["unit_width"] = _translate("Unit", "Width (m)")
        self._dict["unit_width_envelop"] = _translate(
            "Unit", "Width Envelop (m)"
        )
        self._dict["unit_max_width"] = _translate("Unit", "Max Width (m)")
        self._dict["unit_min_width"] = _translate("Unit", "Min Width (m)")
        self._dict["unit_depth"] = _translate("Unit", "Depth (m)")
        self._dict["unit_max_depth"] = _translate("Unit", "Max Depth (m)")
        self._dict["unit_min_depth"] = _translate("Unit", "Min Depth (m)")
        self._dict["unit_depth_envelop"] = _translate(
            "Unit", "Depth Envelop (m)"
        )
        self._dict["unit_mean_depth"] = _translate("Unit", "Mean Depth (m)")
        self._dict["unit_diameter"] = _translate("Unit", "Diameter (m)")
        self._dict["unit_thickness"] = _translate("Unit", "Thickness (m)")
        self._dict["unit_elevation"] = _translate("Unit", "Elevation (m)")
        self._dict["unit_bed_elevation"] = _translate(
            "Unit", "Bed Elevation (m)"
        )
        self._dict["unit_bed_elevation_envelop"] = _translate(
            "Unit", "Bed Elevation Envelop (m)"
        )
        self._dict["unit_max_bed_elevation"] = _translate(
            "Unit", "Max Bed Elevation (m)"
        )
        self._dict["unit_min_bed_elevation"] = _translate(
            "Unit", "Min Bed Elevation (m)"
        )
        self._dict["unit_water_elevation"] = _translate(
            "Unit", "Water Elevation (m)"
        )
        self._dict["unit_water_elevation_envelop"] = _translate(
            "Unit", "Water Elevation Envelop (m)"
        )
        self._dict["unit_max_water_elevation"] = _translate(
            "Unit", "Max Water Elevation (m)"
        )
        self._dict["unit_min_water_elevation"] = _translate(
            "Unit", "Min Water Elevation (m)"
        )
        self._dict["unit_velocity"] = _translate("Unit", "Velocity (m/s)")
        self._dict["unit_velocity_envelop"] = _translate(
            "Unit", "Velocity Envelop (m/s)"
            )
        self._dict["unit_max_velocity"] = _translate(
            "Unit", "Max Velocity (m/s)"
        )
        self._dict["unit_min_velocity"] = _translate(
            "Unit", "Min Velocity (m/s)"
        )
        self._dict["unit_discharge"] = _translate(
                "Unit", "Discharge") + f" (m³/s)"
        self._dict["unit_discharge_envelop"] = _translate(
            "Unit", "Discharge Envelop") + " (m³/s)"
        self._dict["unit_max_discharge"] = _translate(
            "Unit", "Max Discharge") + " (m³/s)"
        self._dict["unit_min_discharge"] = _translate(
            "Unit", "Min Discharge") + " (m³/s)"
        self._dict["unit_area_he"] = _translate("Unit", "Area (hectare)")

        self._dict["unit_time_s"] = _translate("Unit", "Time (sec)")
        self._dict["unit_time_p"] = _translate("Unit", "Time (JJJ:HH:MM:SS)")

        self._dict["unit_date_s"] = _translate("Unit", "Date (sec)")
        self._dict["unit_date_iso"] = _translate("Unit", "Date (ISO format)")

        self._dict["unit_area"] = _translate("Unit", "Area")
        self._dict["unit_rho"] = _translate("Unit", "Rho")
        self._dict["unit_porosity"] = _translate("Unit", "Porosity")
        self._dict["unit_cdc_riv"] = _translate("Unit", "CDC_RIV")
        self._dict["unit_cdc_cas"] = _translate("Unit", "CDC_CAS")
        self._dict["unit_apd"] = _translate("Unit", "APD")
        self._dict["unit_ac"] = _translate("Unit", "AC")
        self._dict["unit_bc"] = _translate("Unit", "BC")

        self._dict["unit_concentration"] = _translate(
            "Unit", "Concentration (g/l)"
        )
        self._dict["unit_wet_area"] = _translate("Unit", "Wet Area") + " (m²)"
        self._dict["unit_wet_perimeter"] = _translate(
            "Unit", "Wet Perimeter (m)"
        )
        self._dict["unit_hydraulic_radius"] = _translate(
            "Unit", "Hydraulic Radius (m)"
        )
        self._dict["unit_froude"] = _translate("Unit", "Froude number")
        self._dict["unit_mass"] = _translate("Unit", "Mass (kg)")
        self._dict["unit_concentration"] = _translate(
            "Unit", "Concentration") + " (kg/m³)"


class MainTranslate(UnitTranslate):
    def __init__(self):
        super(MainTranslate, self).__init__()

        self._dict["tab_info_name"] = _translate(
            "MainWindow", "Summary"
        )
        self._dict["tab_checker_name"] = _translate(
            "MainWindow", "Checks"
        )

        self._dict["open_debug"] = _translate(
            "MainWindow", "Open debug window"
        )
        self._dict["open_debug_sql"] = _translate(
            "MainWindow", "Open SQLite debuging tool ('sqlitebrowser')"
        )
        self._dict["active_window"] = _translate(
            "MainWindow", "Enable this window"
        )

        # Message box
        self._dict["Warning"] = _translate(
            "MainWindow", "Warning"
        )
        self._dict["mb_select_reach_title"] = _translate(
            "MainWindow", "Please select a reach"
        )
        self._dict["mb_select_reach_msg"] = _translate(
            "MainWindow",
            "This edition window need a reach selected "
            "into the river network to work on it"
        )

        self._dict["mb_last_open_title"] = _translate(
            "MainWindow", "Last open study"
        )
        self._dict["mb_last_open_msg"] = _translate(
            "MainWindow",
            "Do you want to open again the last open study?"
        )

        self._dict["mb_close_title"] = _translate(
            "MainWindow", "Close without saving study"
        )
        self._dict["mb_close_msg"] = _translate(
            "MainWindow",
            "Do you want to save current study before closing it?"
        )
        self._dict["x"] = _translate("MainWindow", "X (m)")
        self._dict["y"] = _translate("MainWindow", "Y (m)")
        self._dict["Yes"] = _translate("MainWindow", "Yes")
        self._dict["No"] = _translate("MainWindow", "No")
        self._dict["Cancel"] = _translate("MainWindow", "Cancel")
        self._dict["Save"] = _translate("MainWindow", "Save")
        self._dict["Close"] = _translate("MainWindow", "Close")
