# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import timer, trace

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5 import QtCore
from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel, QCoreApplication,
    pyqtSlot, pyqtSignal, QItemSelectionModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QHeaderView, QDoubleSpinBox, QVBoxLayout, QCheckBox
)

from View.Pollutants.Table import (
    TableModel
)

from View.Pollutants.Translate import PollutantsTranslate

from View.Pollutants.Edit.Window import EditPolluantWindow

from View.InitialConditionsAdisTS.Window import InitialConditionsAdisTSWindow
from View.BoundaryConditionsAdisTS.Window import BoundaryConditionAdisTSWindow
from View.LateralContributionsAdisTS.Window \
    import LateralContributionAdisTSWindow

logger = logging.getLogger()


class PollutantsWindow(PamhyrWindow):
    _pamhyr_ui = "Pollutants"
    _pamhyr_name = "Pollutants"

    def __init__(self, study=None, config=None, parent=None):
        trad = PollutantsTranslate()
        name = trad[self._pamhyr_name] + " - " + study.name

        super(PollutantsWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._pollutants_lst = self._study._river._Pollutants

        self.setup_table()
        self.setup_checkbox()
        self.setup_connections()

        self.update()

    def setup_table(self):
        self._table = None

        table = self.find(QTableView, f"tableView")
        self._table = TableModel(
            table_view=table,
            table_headers=self._trad.get_dict("table_headers"),
            editable_headers=["name"],
            trad=self._trad,
            data=self._study.river,
            undo=self._undo_stack,
        )

        selectionModel = table.selectionModel()
        index = table.model().index(0, 0)

        selectionModel.select(
            index,
            QItemSelectionModel.Rows |
            QItemSelectionModel.ClearAndSelect |
            QItemSelectionModel.Select
        )
        table.scrollTo(index)

    def setup_checkbox(self):
        self._checkbox = self.find(QCheckBox, f"checkBox")
        self._set_checkbox_state()

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_delete").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)
        self.find(QAction, "action_initial_conditions"
                  ).triggered.connect(self.initial_conditions)
        self.find(QAction, "action_boundary_conditions"
                  ).triggered.connect(self.boundary_conditions)
        self.find(QAction, "action_lateral_contributions"
                  ).triggered.connect(self.lateral_contrib)
        self._checkbox.clicked.connect(self._set_structure_state)

        table = self.find(QTableView, "tableView")
        table.selectionModel()\
             .selectionChanged\
             .connect(self.update)

        self._table.dataChanged.connect(self.update)
        self._table.layoutChanged.connect(self.update)

    def index_selected(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0]
        else:
            return None

    def index_selected_row(self):
        table = self.find(QTableView, "tableView")
        r = table.selectionModel().selectedRows()

        if len(r) > 0:
            return r[0].row()
        else:
            return None

    def index_selected_rows(self):
        table = self.find(QTableView, "tableView")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def add(self):
        self._table.add(len(self._pollutants_lst))

    def delete(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table.delete(rows)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        self._table.undo()

    def _redo(self):
        self._table.redo()

    def edit(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        for row in rows:
            data = self._pollutants_lst.get(row)

            if self.sub_window_exists(
                EditPolluantWindow,
                data=[self._study, None, data]
            ):
                continue

            win = EditPolluantWindow(
                data=data,
                study=self._study,
                parent=self
            )
            win.show()

    def initial_conditions(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        for row in rows:
            pollutant_id = self._pollutants_lst.get(row).id

            iclist = self._study.river.ic_adists.Initial_Conditions_List
            ics_adists = next(filter(lambda x: x.pollutant == pollutant_id,
                                     iclist))

            if self.sub_window_exists(
                InitialConditionsAdisTSWindow,
                data=[self._study, None, ics_adists]
            ):
                return

            initial = InitialConditionsAdisTSWindow(
                study=self._study,
                parent=self,
                data=ics_adists
            )
            initial.show()

    def boundary_conditions(self):

        if self.sub_window_exists(
            BoundaryConditionAdisTSWindow,
            data=[self._study, None]
        ):
            bound = self.get_sub_window(
                BoundaryConditionAdisTSWindow,
                data=[self._study, None]
            )
            return

        bound = BoundaryConditionAdisTSWindow(
            study=self._study, parent=self
        )
        bound.show()

    def lateral_contrib(self):
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        pollutant_id = self._pollutants_lst.get(rows[0]).id

        if self.sub_window_exists(
            LateralContributionAdisTSWindow,
            data=[self._study, pollutant_id, None]
        ):
            return

        lateral = LateralContributionAdisTSWindow(
            study=self._study,
            pollutant=pollutant_id,
            parent=self
        )
        lateral.show()

    def _set_checkbox_state(self):
        row = self.index_selected_row()
        if row is None:
            self._checkbox.setEnabled(False)
            self._checkbox.setChecked(True)
        else:
            self._checkbox.setEnabled(True)
            self._checkbox.setChecked(self._pollutants_lst.get(row).enabled)

    def _set_structure_state(self):
        rows = self.index_selected_rows()
        if len(rows) != 0:
            for row in rows:
                if row is not None:
                    self._table.enabled(
                        row,
                        self._checkbox.isChecked()
                    )

    def update(self):
        self._set_checkbox_state()
