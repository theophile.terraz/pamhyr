# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.Pollutants.Pollutants import Pollutants

logger = logging.getLogger()


class SetDataCommand(QUndoCommand):
    def __init__(self, data, row, column, new_value):
        QUndoCommand.__init__(self)

        self._data = data
        self._row = row
        self._column = column
        self._old = self._data.data[self._row][self._column]
        self._new = new_value

    def undo(self):
        self._data.data[self._row][self._column] = self._old

    def redo(self):
        self._data.data[self._row][self._column] = self._new


class PasteCommand(QUndoCommand):
    def __init__(self, data, row, new_data):
        QUndoCommand.__init__(self)

        self._data = data
        self._row = row
        self._new = [n for n in new_data[row]]
        self._old = [o for o in self._data.data[row]]

    def undo(self):
        for i in range(9):
            self._data.data[self._row][i] = self._old[i]

    def redo(self):
        for i in range(9):
            self._data.data[self._row][i] = self._new[i]
