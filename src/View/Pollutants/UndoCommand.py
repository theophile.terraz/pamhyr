# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

logger = logging.getLogger()


class SetNameCommand(QUndoCommand):
    def __init__(self, pollutants_lst, index, new_value):
        QUndoCommand.__init__(self)

        self._pollutants_lst = pollutants_lst
        self._index = index
        self._old = self._pollutants_lst.get(self._index).name
        self._new = str(new_value)

    def undo(self):
        self._pollutants_lst.get(self._index).name = self._old

    def redo(self):
        self._pollutants_lst.get(self._index).name = self._new


class SetEnabledCommand(QUndoCommand):
    def __init__(self, pollutants_lst, index, enabled):
        QUndoCommand.__init__(self)

        self._pollutants_lst = pollutants_lst
        self._index = index
        self._old = not enabled
        self._new = enabled

    def undo(self):
        self._pollutants_lst.get(self._index).enabled = self._old

    def redo(self):
        self._pollutants_lst.get(self._index).enabled = self._new


class AddCommand(QUndoCommand):
    def __init__(self, pollutants_lst, index, data):
        QUndoCommand.__init__(self)

        self._pollutants_lst = pollutants_lst

        self._index = index
        self._new = None
        self._new_ic_adists = None
        self._data = data

    def undo(self):
        self._pollutants_lst.delete_i([self._index])
        self._data.delete_i([self._index])

    def redo(self):
        if self._new is None:
            self._new = self._pollutants_lst.new(
                self._pollutants_lst, self._index
            )
            self._new._data = [[0, 0., 0., 0., 0., 0., 0., 0., 0.]]
            self._new_ic_adists = self._data.new(self._index, self._new.id)
        else:
            self._pollutants_lst.insert(self._index, self._new)
            self._data.insert(self._index, self._new_ic_adists)


class DelCommand(QUndoCommand):
    def __init__(self, pollutants_lst, rows, data):
        QUndoCommand.__init__(self)

        self._pollutants_lst = pollutants_lst
        self._inc_pollutants_lst = data

        self._rows = rows

        self._pollutants = []
        self._inc_pollutants = []

        for row in rows:
            self._pollutants.append((row, self._pollutants_lst.get(row)))
            self._inc_pollutants.append(
                    (row, self._inc_pollutants_lst.get(row))
                )
        self._pollutants.sort()
        self._inc_pollutants.sort()

    def undo(self):
        for row, el in self._pollutants:
            self._pollutants_lst.insert(row, el)

        for row, el in self._inc_pollutants:
            self._inc_pollutants_lst.insert(row, el)

    def redo(self):
        self._pollutants_lst.delete_i(self._rows)
        self._inc_pollutants_lst.delete_i(self._rows)


class PasteCommand(QUndoCommand):
    def __init__(self, pollutants_lst, row, pollutant):
        QUndoCommand.__init__(self)

        self._pollutants_lst = pollutants_lst

        self._row = row
        self._pollutant = deepcopy(pollutant)
        self._pollutant.reverse()

    def undo(self):
        self._pollutants_lst.delete_i(
            self._tab,
            range(self._row, self._row + len(self._pollutant))
        )

    def redo(self):
        for r in self._pollutant:
            self._pollutants_lst.insert(self._row, r)
