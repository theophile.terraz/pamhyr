# UndoCommand.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import deepcopy
from tools import trace, timer

from PyQt5.QtWidgets import (
    QMessageBox, QUndoCommand, QUndoStack,
)

from Model.BoundaryCondition.BoundaryCondition import BoundaryCondition
from Model.BoundaryCondition.BoundaryConditionList import BoundaryConditionList


class SetNameCommand(QUndoCommand):
    def __init__(self, bcs, tab, index, new_value):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._index = index
        self._old = self._bcs.get(self._tab, self._index).name
        self._new = str(new_value)

    def undo(self):
        self._bcs.get(self._tab, self._index).name = self._old

    def redo(self):
        self._bcs.get(self._tab, self._index).name = self._new


class SetNodeCommand(QUndoCommand):
    def __init__(self, bcs, tab, index, node):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._index = index
        self._old = self._bcs.get(self._tab, self._index).node
        self._new = node
        self._prev_assoc_to_node = self._bcs.get_assoc_to_node(tab, node)

    def _previous_assoc_node(self, node):
        if self._prev_assoc_to_node is not None:
            self._prev_assoc_to_node.node = node

    def undo(self):
        self._bcs.get(self._tab, self._index).node = self._old
        self._previous_assoc_node(self._new)

    def redo(self):
        self._bcs.get(self._tab, self._index).node = self._new
        self._previous_assoc_node(None)


class SetTypeCommand(QUndoCommand):
    def __init__(self, bcs, tab, index, _type):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._index = index
        self._type = _type
        self._old = self._bcs.get(self._tab, self._index)
        self._new = self._bcs.get(self._tab, self._index)\
                             .convert(self._type)

    def undo(self):
        self._bcs.set(self._tab, self._index, self._old)

    def redo(self):
        self._bcs.set(self._tab, self._index, self._new)


class AddCommand(QUndoCommand):
    def __init__(self, bcs, tab, index):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._index = index
        self._new = None

    def undo(self):
        self._bcs.delete_i(self._tab, [self._index])

    def redo(self):
        if self._new is None:
            self._new = self._bcs.new(self._tab, self._index)
        else:
            self._bcs.insert(self._tab, self._index, self._new)


class DelCommand(QUndoCommand):
    def __init__(self, bcs, tab, rows):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._rows = rows

        self._bc = []
        for row in rows:
            self._bc.append((row, self._bcs.get(self._tab, row)))
        self._bc.sort()

    def undo(self):
        for row, el in self._bc:
            self._bcs.insert(self._tab, row, el)

    def redo(self):
        self._bcs.delete_i(self._tab, self._rows)


class SortCommand(QUndoCommand):
    def __init__(self, bcs, tab, _reverse):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._reverse = _reverse

        self._old = self._bcs.get_tab(self._tab)
        self._indexes = None

    def undo(self):
        ll = self._bcs.get_tab(self._tab)
        self._bcs.sort(
            self._tab,
            key=lambda x: self._indexes[ll.index(x)]
        )

    def redo(self):
        self._bcs.sort(
            self._tab,
            reverse=self._reverse,
            key=lambda x: x.name
        )
        if self._indexes is None:
            self._indexes = list(
                map(
                    lambda p: self._old.index(p),
                    self._bcs.get_tab(self._tab)
                )
            )
            self._old = None


class MoveCommand(QUndoCommand):
    def __init__(self, bcs, tab, up, i):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._up = up == "up"
        self._i = i

    def undo(self):
        if self._up:
            self._bcs.move_up(self._tab, self._i)
        else:
            self._bcs.move_down(self._tab, self._i)

    def redo(self):
        if self._up:
            self._bcs.move_up(self._tab, self._i)
        else:
            self._bcs.move_down(self._tab, self._i)


class PasteCommand(QUndoCommand):
    def __init__(self, bcs, tab, row, bc):
        QUndoCommand.__init__(self)

        self._bcs = bcs
        self._tab = tab
        self._row = row
        self._bc = deepcopy(bc)
        self._bc.reverse()

    def undo(self):
        self._bcs.delete_i(
            self._tab,
            range(self._row, self._row + len(self._bc))
        )

    def redo(self):
        for bc in self._bc:
            self._bcs.insert(self._tab, self._row, bc)
