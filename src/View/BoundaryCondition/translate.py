# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

from Model.BoundaryCondition.BoundaryConditionTypes import (
    NotDefined, PonctualContribution,
    TimeOverZ, TimeOverDischarge, ZOverDischarge,
    Solid
)

_translate = QCoreApplication.translate

BC_types = {
    "ND": NotDefined,
    "PC": PonctualContribution,
    "TZ": TimeOverZ,
    "TD": TimeOverDischarge,
    "ZD": ZOverDischarge,
    "SL": Solid,
}


class BCTranslate(MainTranslate):
    def __init__(self):
        super(BCTranslate, self).__init__()

        self._dict["Boundary conditions"] = _translate(
            "BoundaryConditions", "Boundary conditions"
        )

        self._sub_dict["long_types"] = {
            "ND": self._dict["not_defined"],
            "PC": _translate("BoundaryCondition", "Point sources"),
            "TZ": _translate("BoundaryCondition", "Z(t)"),
            "TD": _translate("BoundaryCondition", "Q(t)"),
            "ZD": _translate("BoundaryCondition", "Q(Z)"),
            "SL": _translate("BoundaryCondition", "Solid"),
        }

        self._sub_dict["table_headers"] = {
            "name": self._dict["name"],
            "type": self._dict["type"],
            "node": _translate("BoundaryCondition", "Node")
        }
