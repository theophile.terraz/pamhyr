# translate.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from View.Translate import MainTranslate

from View.BoundaryCondition.translate import BCTranslate

_translate = QCoreApplication.translate


class BCETranslate(BCTranslate):
    def __init__(self):
        super(BCETranslate, self).__init__()

        self._dict["Edit Boundary Conditions"] = _translate(
            "BoundaryCondition", "Edit boundary conditions"
        )
        self._dict["Boundary Condition Options"] = _translate(
            "BoundaryCondition", "Boundary Condition Options")

        self._sub_dict["table_headers"] = {
            "x": _translate("BoundaryCondition", "X"),
            "y": _translate("BoundaryCondition", "Y"),
            "time": self._dict["time"],
            "date": self._dict["date"],
            "discharge": self._dict["unit_discharge"],
            "z": self._dict["unit_elevation"],
            "solid": _translate("BoundaryCondition", "Solid (kg/s)"),
        }

        self._dict["title_need_geometry"] = _translate(
            "BoundaryCondition", "No geometry"
        )
        self._dict["msg_need_geometry"] = _translate(
            "BoundaryCondition",
            "No geometry found for this reach.\n"
            "This feature requires a reach with a geometry."
        )
        self._dict["Warning"] = _translate("BoundaryCondition",
                                           "Warning")
