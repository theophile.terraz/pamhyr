# GenerateDialog.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from View.Tools.PamhyrWindow import PamhyrDialog

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QComboBox, QUndoStack, QShortcut,
    QDoubleSpinBox, QCheckBox, QPushButton
)


class GenerateDialog(PamhyrDialog):
    _pamhyr_ui = "BoundaryConditionsDialogGenerator"
    _pamhyr_name = "Boundary Condition Options"

    def __init__(self,
                 value,
                 reach,
                 title="Boundary Condition Options",
                 trad=None,
                 parent=None):
        super(GenerateDialog, self).__init__(
            title=trad[self._pamhyr_name],
            options=[],
            trad=trad,
            parent=parent
        )

        self.value = value
        self.find(QDoubleSpinBox, "doubleSpinBox").setValue(self.value)
        self.reach = reach
        self.find(QPushButton, "EstimateButton").clicked.connect(
            self.estimate
            )

    def accept(self):
        self.value = self.find(QDoubleSpinBox, "doubleSpinBox").value()
        super().accept()

    def reject(self):
        self.close()

    def estimate(self):
        self.value = abs(self.reach.get_incline_median_mean())
        self.find(QDoubleSpinBox, "doubleSpinBox").setValue(self.value)
