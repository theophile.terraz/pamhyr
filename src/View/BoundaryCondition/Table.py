# Table.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging
import traceback

from tools import trace, timer

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox,
)

from Model.BoundaryCondition.BoundaryConditionTypes import (
    NotDefined, PonctualContribution,
    TimeOverZ, TimeOverDischarge, ZOverDischarge
)

from View.Tools.PamhyrTable import PamhyrTableModel

from View.BoundaryCondition.UndoCommand import (
    SetNameCommand, SetNodeCommand, SetTypeCommand,
    AddCommand, DelCommand, SortCommand,
    MoveCommand, PasteCommand,
)
from View.BoundaryCondition.translate import BC_types

logger = logging.getLogger()

_translate = QCoreApplication.translate


class ComboBoxDelegate(QItemDelegate):
    def __init__(self, data=None, mode="type", tab="",
                 trad=None, parent=None):
        super(ComboBoxDelegate, self).__init__(parent)

        self._data = data
        self._mode = mode
        self._tab = tab
        self._trad = trad

        self._long_types = {}
        if self._trad is not None:
            self._long_types = self._trad.get_dict("long_types")

    def createEditor(self, parent, option, index):
        self.editor = QComboBox(parent)

        if self._mode == "type":
            lst = list(
                map(
                    lambda k: self._long_types[k],
                    filter(
                        lambda k: self._tab in BC_types[k].compatibility(),
                        BC_types.keys()
                    )
                )
            )
            self.editor.addItems(
                lst
            )
        else:
            self.editor.addItems(
                [self._trad["not_associated"]] +
                self._data.nodes_names()
            )

        self.editor.setCurrentText(index.data(Qt.DisplayRole))
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(Qt.DisplayRole)
        self.editor.currentTextChanged.connect(self.currentItemChanged)

    def setModelData(self, editor, model, index):
        text = str(editor.currentText())
        model.setData(index, text)
        editor.close()
        editor.deleteLater()

    def updateEditorGeometry(self, editor, option, index):
        r = QRect(option.rect)
        if self.editor.windowFlags() & Qt.Popup:
            if editor.parent() is not None:
                r.setTopLeft(self.editor.parent().mapToGlobal(r.topLeft()))
        editor.setGeometry(r)

    @pyqtSlot()
    def currentItemChanged(self):
        self.commitData.emit(self.sender())


class TableModel(PamhyrTableModel):
    def __init__(self, trad=None, **kwargs):
        self._trad = trad
        self._long_types = {}
        if self._trad is not None:
            self._long_types = self._trad.get_dict("long_types")

        super(TableModel, self).__init__(trad=trad, **kwargs)

    def _setup_lst(self):
        self._lst = self._data.boundary_condition
        self._tab = self._opt_data

    def rowCount(self, parent):
        return self._lst.len(self._tab)

    def data(self, index, role):
        if role != Qt.ItemDataRole.DisplayRole:
            return QVariant()

        row = index.row()
        column = index.column()

        if self._headers[column] == "name":
            return self._lst.get(self._tab, row).name
        elif self._headers[column] == "type":
            t = self._lst.get(self._tab, row).bctype
            return self._long_types[t]
        elif self._headers[column] == "node":
            n = self._lst.get(self._tab, row).node
            if n is None:
                return self._trad["not_associated"]
            return n.name

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid() or role != Qt.EditRole:
            return False

        row = index.row()
        column = index.column()

        try:
            if self._headers[column] == "name":
                self._undo.push(
                    SetNameCommand(
                        self._lst, self._tab, row, value
                    )
                )
            elif self._headers[column] == "type":
                key = next(k for k, v in self._long_types.items()
                           if v == value)
                self._undo.push(
                    SetTypeCommand(
                        self._lst, self._tab, row, BC_types[key]
                    )
                )
            elif self._headers[column] == "node":
                self._undo.push(
                    SetNodeCommand(
                        self._lst, self._tab, row, self._data.node(value)
                    )
                )
        except Exception as e:
            logger.info(e)
            logger.debug(traceback.format_exc())

        self.dataChanged.emit(index, index)
        return True

    def add(self, row, parent=QModelIndex()):
        self.beginInsertRows(parent, row, row - 1)

        self._undo.push(
            AddCommand(
                self._lst, self._tab, row
            )
        )

        self.endInsertRows()
        self.layoutChanged.emit()

    def delete(self, rows, parent=QModelIndex()):
        self.beginRemoveRows(parent, rows[0], rows[-1])

        self._undo.push(
            DelCommand(
                self._lst, self._tab, rows
            )
        )

        self.endRemoveRows()
        self.layoutChanged.emit()

    def sort(self, _reverse, parent=QModelIndex()):
        self.layoutAboutToBeChanged.emit()

        self._undo.push(
            SortCommand(
                self._lst, self._tab, False
            )
        )

        self.layoutAboutToBeChanged.emit()
        self.layoutChanged.emit()

    def move_up(self, row, parent=QModelIndex()):
        if row <= 0:
            return

        target = row + 2

        self.beginMoveRows(parent, row - 1, row - 1, parent, target)

        self._undo_stack.push(
            MoveCommand(
                self._lst, self._tab, "up", row
            )
        )

        self.endMoveRows()
        self.layoutChanged.emit()

    def move_down(self, index, parent=QModelIndex()):
        if row > len(self._lst):
            return

        target = row

        self.beginMoveRows(parent, row + 1, row + 1, parent, target)

        self._undo_stack.push(
            MoveCommand(
                self._lst, self._tab, "down", row
            )
        )

        self.endMoveRows()
        self.layoutChanged.emit()

    def undo(self):
        self._undo.undo()
        self.layoutChanged.emit()

    def redo(self):
        self._undo.redo()
        self.layoutChanged.emit()
