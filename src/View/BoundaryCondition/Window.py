# Window.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer, logger_exception

from View.Tools.PamhyrWindow import PamhyrWindow

from PyQt5.QtGui import (
    QKeySequence,
)

from PyQt5.QtCore import (
    Qt, QVariant, QAbstractTableModel,
    QCoreApplication, QModelIndex, pyqtSlot,
    QRect,
)

from PyQt5.QtWidgets import (
    QDialogButtonBox, QPushButton, QLineEdit,
    QFileDialog, QTableView, QAbstractItemView,
    QUndoStack, QShortcut, QAction, QItemDelegate,
    QComboBox, QVBoxLayout, QHeaderView, QTabWidget,
    QWidget,
)

from Model.BoundaryCondition.BoundaryConditionTypes import (
    NotDefined, PonctualContribution,
    TimeOverZ, TimeOverDischarge, ZOverDischarge
)

from View.BoundaryCondition.UndoCommand import (
    SetNameCommand, SetNodeCommand, SetTypeCommand,
    AddCommand, DelCommand, SortCommand,
    MoveCommand, PasteCommand,
)

from View.BoundaryCondition.Table import (
    TableModel, ComboBoxDelegate
)

from View.Network.GraphWidget import GraphWidget
from View.BoundaryCondition.translate import BCTranslate
from View.BoundaryCondition.Edit.Window import EditBoundaryConditionWindow

_translate = QCoreApplication.translate

logger = logging.getLogger()


class BoundaryConditionWindow(PamhyrWindow):
    _pamhyr_ui = "BoundaryConditions"
    _pamhyr_name = "Boundary conditions"

    def __init__(self, study=None, config=None, parent=None):
        trad = BCTranslate()
        name = (
            trad[self._pamhyr_name] +
            " - " + study.name
        )

        super(BoundaryConditionWindow, self).__init__(
            title=name,
            study=study,
            config=config,
            trad=trad,
            parent=parent
        )

        self._bcs = self._study.river.boundary_condition

        self.setup_table()
        self.setup_graph()
        self.setup_connections()

        self.ui.setWindowTitle(self._title)

    def setup_table(self):
        self._table = {}

        for t in ["liquid", "solid"]:
            self._delegate_type = ComboBoxDelegate(
                trad=self._trad,
                data=self._study.river,
                mode="type",
                tab=t,
                parent=self
            )
            self._delegate_node = ComboBoxDelegate(
                trad=self._trad,
                data=self._study.river,
                mode="node",
                tab=t,
                parent=self
            )

            table = self.find(QTableView, f"tableView_{t}")
            self._table[t] = TableModel(
                table_view=table,
                table_headers=self._trad.get_dict("table_headers"),
                editable_headers=["name", "type", "node"],
                delegates={
                    "type": self._delegate_type,
                    "node": self._delegate_node,
                },
                trad=self._trad,
                data=self._study.river,
                undo=self._undo_stack,
                opt_data=t,
            )
            table.setModel(self._table[t])
            table.setSelectionBehavior(QAbstractItemView.SelectRows)
            table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
            table.setAlternatingRowColors(True)

    def setup_graph(self):
        self.graph_widget = GraphWidget(
            self._study.river,
            min_size=None, size=(200, 200),
            only_display=True,
            parent=self
        )
        self.graph_layout = self.find(QVBoxLayout, "verticalLayout")
        self.graph_layout.addWidget(self.graph_widget)

    def setup_connections(self):
        self.find(QAction, "action_add").triggered.connect(self.add)
        self.find(QAction, "action_del").triggered.connect(self.delete)
        self.find(QAction, "action_edit").triggered.connect(self.edit)
        self.find(QAction, "action_sort").triggered.connect(self.sort)

    def current_tab(self):
        return self.find(QTabWidget, "tabWidget")\
                   .currentWidget()\
                   .objectName()\
                   .replace("tab_", "")

    def index_selected_row(self):
        tab = self.current_tab()
        table = self.find(QTableView, f"tableView_{tab}")
        return table.selectionModel()\
                    .selectedRows()[0]\
                    .row()

    def index_selected_rows(self):
        tab = self.current_tab()
        table = self.find(QTableView, f"tableView_{tab}")
        return list(
            # Delete duplicate
            set(
                map(
                    lambda i: i.row(),
                    table.selectedIndexes()
                )
            )
        )

    def set_active_tab(self, tab_id=0):
        try:
            tab_widget = self.find(QTabWidget, "tabWidget")
            tab_widget.setCurrentIndex(tab_id)
        except Exception as e:
            logger_exception(e)

    def add(self):
        tab = self.current_tab()
        rows = self.index_selected_rows()
        if self._bcs.len(tab) == 0 or len(rows) == 0:
            self._table[tab].add(0)
        else:
            self._table[tab].add(rows[0])

    def delete(self):
        tab = self.current_tab()
        rows = self.index_selected_rows()
        if len(rows) == 0:
            return

        self._table[tab].delete(rows)

    def sort(self):
        tab = self.current_tab()
        self._table[tab].sort(False)

    def move_up(self):
        tab = self.current_tab()
        row = self.index_selected_row()
        self._table[tab].move_up(row)

    def move_down(self):
        tab = self.current_tab()
        row = self.index_selected_row()
        self._table[tab].move_down(row)

    def _copy(self):
        logger.info("TODO: copy")

    def _paste(self):
        logger.info("TODO: paste")

    def _undo(self):
        tab = self.current_tab()
        self._table[tab].undo()

    def _redo(self):
        tab = self.current_tab()
        self._table[tab].redo()

    def edit(self):
        tab = self.current_tab()
        rows = self.index_selected_rows()
        for row in rows:
            data = self._bcs.get(tab, row)

            if self.sub_window_exists(
                EditBoundaryConditionWindow,
                data=[self._study, None, data]
            ):
                continue

            win = EditBoundaryConditionWindow(
                data=data,
                study=self._study,
                parent=self
            )
            win.show()
