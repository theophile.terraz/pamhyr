# LateralContributionsAdisTSList.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import copy
from tools import trace, timer

from Model.Tools.PamhyrList import PamhyrModelList
from Model.Except import NotImplementedMethodeError

from Model.LateralContributionsAdisTS.LateralContributionAdisTS \
    import LateralContributionAdisTS


class LateralContributionsAdisTSList(PamhyrModelList):
    _sub_classes = [
        LateralContributionAdisTS,
    ]

    @classmethod
    def _db_load(cls, execute, data=None):
        new = cls(status=data['status'])

        if data is None:
            data = {}

        new._lst = LateralContributionAdisTS._db_load(
            execute, data
        )

        return new

    def _db_save(self, execute, data=None):
        execute("DELETE FROM lateral_contribution_adists")
        execute("DELETE FROM lateral_condition_data_adists")

        if data is None:
            data = {}

        for lc in self._lst:
            lc._db_save(execute, data=data)

        return True

    def new(self, index, pollutant):
        n = LateralContributionAdisTS(pollutant=pollutant, status=self._status)
        self._lst.insert(index, n)
        self._status.modified()
        return n

    @property
    def Lat_Cont_List(self):
        return self.lst
