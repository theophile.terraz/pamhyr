# OutputRKAdists.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import (
    trace, timer,
    old_pamhyr_date_to_timestamp,
    date_iso_to_timestamp,
    date_dmy_to_timestamp,
)

from Model.Tools.PamhyrDB import SQLSubModel
from Model.Except import NotImplementedMethodeError

logger = logging.getLogger()


class OutputRKAdists(SQLSubModel):
    _sub_classes = []
    _id_cnt = 0

    def __init__(self, id: int = -1, reach=None,
                 rk=None, title: str = "",  status=None):
        super(OutputRKAdists, self).__init__()

        self._status = status

        if id == -1:
            self.id = OutputRKAdists._id_cnt
        else:
            self.id = id

        self._reach = reach
        self._rk = rk
        self._title = str(title)
        self._enabled = True

        OutputRKAdists._id_cnt = max(
            OutputRKAdists._id_cnt + 1, self.id)

    @property
    def reach(self):
        return self._reach

    @reach.setter
    def reach(self, reach_id):
        self._reach = reach_id
        self._status.modified()

    @property
    def rk(self):
        return self._rk

    @rk.setter
    def rk(self, profile_id):
        self._rk = profile_id
        self._status.modified()

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title
        self._status.modified()

    @classmethod
    def _db_create(cls, execute):

        sql = (
            "CREATE TABLE OutputRKAdists(" +
            "id INTEGER NOT NULL PRIMARY KEY, " +
            "reach INTEGER NOT NULL, " +
            "rk REAL NOT NULL, " +
            "title TEXT NOT NULL, " +
            "FOREIGN KEY(reach) REFERENCES river_reach(id)" +
            ")"
        )

        execute(sql)

        return cls._create_submodel(execute)

    @classmethod
    def _db_update(cls, execute, version):
        return True

    @classmethod
    def _db_load(cls, execute, data=None):
        new = []

        # reach   = data["reach"]
        # profile = data["profile"]
        status = data["status"]

        table = execute(
            "SELECT id, reach, rk, title " +
            f"FROM OutputRKAdists"
        )

        if table is not None:
            for row in table:
                id = row[0]
                id_reach = row[1]
                id_rk = row[2]
                title = row[3]

                new_output = cls(
                    id=id, reach=id_reach,
                    rk=id_rk, title=title,
                    status=status
                )

                new.append(new_output)

        return new

    def _db_save(self, execute, data=None):

        execute(f"DELETE FROM OutputRKAdists WHERE id = {self.id}")

        sql = (
            "INSERT INTO " +
            "OutputRKAdists(id, reach, rk, title) " +
            "VALUES (" +
            f"{self.id}, {self._reach}, {self._rk}, " +
            f"'{self._db_format(self._title)}'" +
            ")"
        )

        execute(sql)

        return True

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled
        self._status.modified()
