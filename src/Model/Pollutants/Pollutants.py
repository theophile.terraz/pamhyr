# Pollutants.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import (
    trace, timer,
    old_pamhyr_date_to_timestamp,
    date_iso_to_timestamp,
    date_dmy_to_timestamp,
)

from Model.Tools.PamhyrDB import SQLSubModel
from Model.Except import NotImplementedMethodeError

logger = logging.getLogger()


class Pollutants(SQLSubModel):
    _sub_classes = []
    _id_cnt = 0

    def __init__(self, id: int = -1, name: str = "",  status=None):
        super(Pollutants, self).__init__()

        self._status = status

        if id == -1:
            self.id = Pollutants._id_cnt
        else:
            self.id = id

        self._name = str(name)
        self._enabled = True

        self._data = []

        Pollutants._id_cnt = max(
            Pollutants._id_cnt + 1, self.id)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name
        self._status.modified()

    @property
    def data(self):
        return self._data.copy()

    @classmethod
    def _db_create(cls, execute):
        execute("""
          CREATE TABLE Pollutants(
            id INTEGER NOT NULL PRIMARY KEY,
            name TEXT NOT NULL UNIQUE
          )
        """)

        execute("""
          CREATE TABLE Pollutants_characteristics(
            id INTEGER NOT NULL PRIMARY KEY,
            type INTEGER NOT NULL,
            diametre REAL NOT NULL,
            rho REAL NOT NULL,
            porosity REAL NOT NULL,
            cdc_riv REAL NOT NULL,
            cdc_cas REAL NOT NULL,
            apd REAL NOT NULL,
            ac REAL NOT NULL,
            bc REAL NOT NULL,
            pollutant INTEGER NOT NULL,
            FOREIGN KEY(pollutant) REFERENCES Pollutants(id)
          )
        """)

        return cls._create_submodel(execute)

    @classmethod
    def _db_update(cls, execute, version):
        return True

    @classmethod
    def _db_load(cls, execute, data=None):
        new = []

        status = data["status"]

        table = execute(
            "SELECT id, name " +
            f"FROM Pollutants"
        )

        if table is not None:
            for row in table:
                id = row[0]
                name = row[1]

                new_pollutant = cls(
                    id=id, name=name,
                    status=status
                )

                new_data = []
                table = execute(
                    "SELECT * " +
                    "FROM Pollutants_characteristics " +
                    f"WHERE pollutant = {id}"
                )

                if table is not None:
                    for t in table:
                        new_data = t[1:-1]

                new_pollutant._data.append(new_data)

                new.append(new_pollutant)

        return new

    def _db_save(self, execute, data=None):

        execute(f"DELETE FROM Pollutants WHERE id = {self.id}")
        execute(f"DELETE FROM Pollutants_characteristics" +
                f" WHERE pollutant = {self.id}")

        sql = (
            "INSERT INTO " +
            "Pollutants(id, name) " +
            "VALUES (" +
            f"{self.id}, " +
            f"'{self._db_format(self._name)}'" +
            ")"
        )

        execute(sql)

        for d in self._data:
            sql = (
                "INSERT INTO " +
                "Pollutants_characteristics(type, diametre, rho, porosity, " +
                "cdc_riv, cdc_cas, apd, ac, bc, pollutant) " +
                f"VALUES ({d[0]}, {d[1]}, {d[2]},{d[3]}, {d[4]}, "
                f"{d[5]}, {d[6]}, {d[7]}, {d[8]}, {self.id})"
            )
            execute(sql)

        return True

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled
        self._status.modified()

    def is_define(self):
        return len(self._data) != 0

    def new_from_data(self, data):

        print("from_data before : ", data)
        try:
            new = [int(data[0])]
            new += [float(d) for d in data[1:]]
        except Exception as e:
            logger.error(e)
            new = None

        print("from_data after : ", new)

        return new

    def __len__(self):
        return len(self._data)
