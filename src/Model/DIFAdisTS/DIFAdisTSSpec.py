# DIFAdisTSSpec.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from Model.Tools.PamhyrDB import SQLSubModel
from Model.Except import NotImplementedMethodeError

logger = logging.getLogger()


class DIFAdisTSSpec(SQLSubModel):
    _sub_classes = [
    ]
    _id_cnt = 0

    def __init__(self, id: int = -1, method: str = "",
                 status=None):
        super(DIFAdisTSSpec, self).__init__()

        self._status = status

        if id == -1:
            self.id = DIFAdisTSSpec._id_cnt
        else:
            self.id = id

        self._method = method
        self._reach = None
        self._start_rk = None
        self._end_rk = None
        self._dif = None
        self._b = None
        self._c = None
        self._enabled = True

        DIFAdisTSSpec._id_cnt = max(DIFAdisTSSpec._id_cnt + 1, self.id)

    @classmethod
    def _db_create(cls, execute):
        execute("""
              CREATE TABLE dif_spec(
                id INTEGER NOT NULL PRIMARY KEY,
                dif_default INTEGER NOT NULL,
                method TEXT NOT NULL,
                reach INTEGER NOT NULL,
                start_rk REAL NOT NULL,
                end_rk REAL NOT NULL,
                dif REAL NOT NULL,
                b REAL,
                c REAL,
                enabled BOOLEAN NOT NULL,
                FOREIGN KEY(dif_default) REFERENCES dif_adists(id),
                FOREIGN KEY(reach) REFERENCES river_reach(id)
              )
            """)

        return cls._create_submodel(execute)

    @classmethod
    def _db_update(cls, execute, version):
        major, minor, release = version.strip().split(".")
        if major == 0 and minor < 1:
            if int(release) < 6:
                cls._db_create(execute)

        return True

    @classmethod
    def _db_load(cls, execute, data=None):
        new = []

        table = execute(
            "SELECT id, dif_default, method, reach, start_rk, end_rk, " +
            "dif, b, c, enabled " +
            "FROM dif_spec " +
            f"WHERE dif_default = {data['dif_default_id']} "
        )

        if table is not None:
            for row in table:
                id = row[0]
                method = row[2]
                reach = row[3]
                start_rk = row[4]
                end_rk = row[5]
                dif = row[6]
                b = row[7]
                c = row[8]
                enabled = (row[9] == 1)

                new_spec = cls(
                    id=id,
                    method=method,
                    status=data['status']
                )

                new_spec.reach = reach
                new_spec.start_rk = start_rk
                new_spec.end_rk = end_rk
                new_spec.dif = dif
                new_spec.b = b
                new_spec.c = c
                new_spec.enabled = enabled

                new.append(new_spec)

        return new

    def _db_save(self, execute, data=None):
        dif_default = data['dif_default_id']

        sql = (
            "INSERT INTO " +
            "dif_spec(id, dif_default, method, reach, " +
            "start_rk, end_rk, dif, b, c, enabled) " +
            "VALUES (" +
            f"{self.id}, " +
            f"{dif_default}, " +
            f"'{self._db_format(self._method)}', " +
            f"{self._reach}, " +
            f"{self._start_rk}, " +
            f"{self._end_rk}, " +
            f"{self._dif}, " +
            f"{self._b}, " +
            f"{self._c}, " +
            f"{self._enabled}" +
            ")"
        )
        execute(sql)

        return True

    @property
    def method(self):
        return self._method

    @method.setter
    def method(self, method):
        self._method = method
        self._status.modified()

    @property
    def reach(self):
        return self._reach

    @reach.setter
    def reach(self, reach):
        self._reach = reach
        self._status.modified()

    @property
    def start_rk(self):
        return self._start_rk

    @start_rk.setter
    def start_rk(self, start_rk):
        self._start_rk = start_rk
        self._status.modified()

    @property
    def end_rk(self):
        return self._end_rk

    @end_rk.setter
    def end_rk(self, end_rk):
        self._end_rk = end_rk
        self._status.modified()

    @property
    def dif(self):
        return self._dif

    @dif.setter
    def dif(self, dif):
        self._dif = dif
        self._status.modified()

    @property
    def b(self):
        return self._b

    @b.setter
    def b(self, b):
        self._b = b
        self._status.modified()

    @property
    def c(self):
        return self._c

    @c.setter
    def c(self, c):
        self._c = c
        self._status.modified()

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled
        self._status.modified()
