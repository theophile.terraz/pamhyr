# InitialConditionsAdisTSSpec.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

from tools import trace, timer

from Model.Tools.PamhyrDB import SQLSubModel
from Model.Except import NotImplementedMethodeError

logger = logging.getLogger()


class ICAdisTSSpec(SQLSubModel):
    _sub_classes = [
    ]
    _id_cnt = 0

    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(ICAdisTSSpec, self).__init__()

        self._status = status

        if id == -1:
            self.id = ICAdisTSSpec._id_cnt
        else:
            self.id = id

        self._name_section = name
        self._reach = None
        self._start_rk = None
        self._end_rk = None
        self._concentration = None
        self._eg = None
        self._em = None
        self._ed = None
        self._rate = None
        self._enabled = True

        ICAdisTSSpec._id_cnt = max(ICAdisTSSpec._id_cnt + 1, self.id)

    @classmethod
    def _db_create(cls, execute):
        execute("""
            CREATE TABLE initial_conditions_spec(
              id INTEGER NOT NULL PRIMARY KEY,
              ic_default INTEGER NOT NULL,
              name TEXT NOT NULL,
              reach INTEGER NOT NULL,
              start_rk REAL NOT NULL,
              end_rk REAL NOT NULL,
              concentration REAL NOT NULL,
              eg REAL NOT NULL,
              em REAL NOT NULL,
              ed REAL NOT NULL,
              rate REAL NOT NULL,
              enabled BOOLEAN NOT NULL,
              FOREIGN KEY(ic_default) REFERENCES initial_conditions_adists(id),
              FOREIGN KEY(reach) REFERENCES river_reach(id)
            )
            """)

        return cls._create_submodel(execute)

    @classmethod
    def _db_update(cls, execute, version):
        major, minor, release = version.strip().split(".")
        if major == 0 and minor < 1:
            if int(release) < 6:
                cls._db_create(execute)

        return True

    @classmethod
    def _db_load(cls, execute, data=None):
        new = []

        table = execute(
            "SELECT id, ic_default, name, reach, start_rk, end_rk, " +
            "concentration, eg, em, ed, rate, enabled " +
            "FROM initial_conditions_spec " +
            f"WHERE ic_default = {data['ic_default_id']} "
        )

        if table is not None:
            for row in table:
                id = row[0]
                name = row[2]
                reach = row[3]
                start_rk = row[4]
                end_rk = row[5]
                concentration = row[6]
                eg = row[7]
                em = row[8]
                ed = row[9]
                rate = row[10]
                enabled = (row[11] == 1)

                # new_spec = [name, reach, start_rk, end_rk,
                #     concentration, eg, em, ed, rate, enabled]

                new_spec = cls(
                    id=id,
                    name=name,
                    status=data['status']
                )

                new_spec.reach = reach
                new_spec.start_rk = start_rk
                new_spec.end_rk = end_rk
                new_spec.concentration = concentration
                new_spec.eg = eg
                new_spec.em = em
                new_spec.ed = ed
                new_spec.rate = rate
                new_spec.enabled = enabled

                new.append(new_spec)

        return new

    def _db_save(self, execute, data=None):
        ic_default = data['ic_default_id']

        sql = (
            "INSERT INTO " +
            "initial_conditions_spec(id, ic_default, name, reach, " +
            "start_rk, end_rk, concentration, eg, em, ed, rate, enabled) " +
            "VALUES (" +
            f"{self.id}, " +
            f"{ic_default}, " +
            f"'{self._db_format(self._name_section)}', " +
            f"{self._reach}, " +
            f"{self._start_rk}, " +
            f"{self._end_rk}, " +
            f"{self._concentration}, " +
            f"{self._eg}, " +
            f"{self._em}, " +
            f"{self._ed}, " +
            f"{self._rate}, " +
            f"{self._enabled}" +
            ")"
        )
        execute(sql)

        return True

    @property
    def name(self):
        return self._name_section

    @name.setter
    def name(self, name):
        self._name_section = name
        self._status.modified()

    @property
    def reach(self):
        return self._reach

    @reach.setter
    def reach(self, reach):
        self._reach = reach
        self._status.modified()

    @property
    def start_rk(self):
        return self._start_rk

    @start_rk.setter
    def start_rk(self, start_rk):
        self._start_rk = start_rk
        self._status.modified()

    @property
    def end_rk(self):
        return self._end_rk

    @end_rk.setter
    def end_rk(self, end_rk):
        self._end_rk = end_rk
        self._status.modified()

    @property
    def concentration(self):
        return self._concentration

    @concentration.setter
    def concentration(self, concentration):
        self._concentration = concentration
        self._status.modified()

    @property
    def eg(self):
        return self._eg

    @eg.setter
    def eg(self, eg):
        self._eg = eg
        self._status.modified()

    @property
    def em(self):
        return self._em

    @em.setter
    def em(self, em):
        self._em = em
        self._status.modified()

    @property
    def ed(self):
        return self._ed

    @ed.setter
    def ed(self, ed):
        self._ed = ed
        self._status.modified()

    @property
    def rate(self):
        return self._rate

    @rate.setter
    def rate(self, rate):
        self._rate = rate
        self._status.modified()

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled
        self._status.modified()
