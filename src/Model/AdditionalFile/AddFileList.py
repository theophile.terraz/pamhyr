# AddFileList.py -- Pamhyr
# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import trace, timer

from Model.Except import NotImplementedMethodeError
from Model.Tools.PamhyrList import PamhyrModelList
from Model.AdditionalFile.AddFile import AddFile


class AddFileList(PamhyrModelList):
    _sub_classes = [AddFile]

    @classmethod
    def _db_load(cls, execute, data=None):
        new = cls(status=data["status"])

        new._lst = AddFile._db_load(execute, data)

        return new

    def _db_save(self, execute, data=None):
        ok = True

        # Delete previous data
        execute("DELETE FROM additional_files")

        for af in self._lst:
            ok &= af._db_save(execute, data)

        return ok

    @property
    def files(self):
        return self.lst

    def new(self, index):
        n = AddFile(status=self._status)
        self.insert(index, n)
        self._status.modified()
        return n
