# Results.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import numpy as np

from copy import deepcopy
from datetime import datetime

from Model.Results.River.River import River

logger = logging.getLogger()


class Results(object):
    def __init__(self, study=None, solver=None,
                 repertory="", name="0"):
        self._study = study
        self._solver = solver
        self._repertory = repertory
        self._name = name

        self._river = River(self._study)

        self._meta_data = {
            # Keep results creation date
            "creation_date": datetime.now(),
        }

    @property
    def date(self):
        date = self._meta_data["creation_date"]
        return f"{date.isoformat(sep=' ')}"

    @property
    def river(self):
        return self._river

    @property
    def study(self):
        return self._study

    def set(self, key, value):
        self._meta_data[key] = value

    @property
    def is_valid(self):
        return ("timestamps" in self._meta_data)

    def get(self, key):
        return self._meta_data[key]

    def reload(self):
        return self._solver.results(
            self._study,
            self._repertory,
            qlog=None,
        )
