# PointXY.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from math import dist

from Model.Geometry.Point import Point


class PointAC(Point):
    def __init__(self, a: float = 0.0, c: float = 0.0,
                 name: str = "", status=None):
        super(PointXY, self).__init__(name=name, status=status)

        self._a = float(a)
        self._c = float(c)

    def __repr__(self):
        return f"[{self._a}, {self._c}, {self._name}]"

    @property
    def a(self):
        return self._a

    @a.setter
    def a(self, value):
        self._a = float(value)
        self._status.modified()

    @property
    def c(self):
        return self._c

    @c.setter
    def c(self, value):
        self._c = float(value)
        self._status.modified()

    def dist(self, p2):
        return PointAC.distance(self, p2)

    @staticmethod
    def distance(p1, p2):
        """Euclidean distance between p1 and p2.

        Args:
            p1: A AC Point
            p2: A AC Point

        Returns:
            Euclidean distance between the two points
        """
        return dist((p1.a, p1.c), (p2.a, p2.c))
