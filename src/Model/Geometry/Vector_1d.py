# Vector_1d.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import numpy as np
from Model.Geometry.PointXYZ import PointXYZ


class Vector1d:
    def __init__(self, a: PointXYZ, b: PointXYZ):
        self.A = a
        self.B = b

    def __repr__(self):
        return "vecteur AB = ({}, {}, {})".format(
            self.B.x - self.A.x,
            self.B.y - self.A.y,
            self.B.z - self.A.z
        )

    def vector1d(self):
        return np.array([
            self.B.x - self.A.x,
            self.B.y - self.A.y,
            self.B.z - self.A.z
        ])

    def direction_vector(self):
        return np.array([
            self.B.x - self.A.x,
            self.B.y - self.A.y,
            0
        ])

    def norm_vector(self):
        return np.linalg.norm(self.vector1d())

    def norm_direction_vector(self):
        return np.linalg.norm(self.direction_vector())

    def normalized_direction_vector(self):
        return self.direction_vector() / self.norm_direction_vector()
