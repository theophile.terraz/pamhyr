# Point.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.Except import NotImplementedMethodeError


class Point(object):
    def __init__(self, name: str = "", profile=None, status=None):
        super(Point, self).__init__()

        self._status = status

        self._name = name
        self._profile = profile
        self._sl = None

    @property
    def sl(self):
        if self._sl is None:
            return self._profile.sl

        return self._sl

    @sl.setter
    def sl(self, sl):
        if sl == self._profile.sl:
            self._sl = None
        else:
            self._sl = sl

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name
        self._status.modified()

    def point_is_named(self):
        """
        Returns:
            True if the point is named.
        """
        return self._name.strip() != ""

    def is_nan(self):
        raise NotImplementedMethodeError(self, self.is_nan)

    def dist(self, p2):
        raise NotImplementedMethodeError(self, self.dist)
