# PamhyrDict.py -- Pamhyr abstract dict for model classes
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import types
import logging

from copy import copy
from tools import trace, timer

from Model.Tools.PamhyrDB import SQLSubModel
from Model.Except import NotImplementedMethodeError

logger = logging.getLogger()


class PamhyrModelDict(SQLSubModel):
    _sub_classes = []

    def __init__(self, status=None):
        super(PamhyrModelDict, self).__init__()

        self._status = status

        self._dict = {}

    @classmethod
    def _db_create(cls, execute):
        return cls._create_submodel(execute)

    @classmethod
    def _db_update(cls, execute, version):
        return cls._update_submodel(execute, version)

    @classmethod
    def _db_load(cls, execute, data=None):
        raise NotImplementedMethodeError(cls, cls._db_load)

    def _db_save(self, execute, data=None):
        raise NotImplementedMethodeError(self, self._db_save)

    def __len__(self):
        return len(self._dict)

    def __contains__(self, key):
        return key in self._dict

    def set(self, key, new):
        self._dict[key] = new
        self._status.modified()

    def get(self, key):
        if key in self._dict:
            v = self._dict[key]

            if isinstance(v, types.GeneratorType):
                return list(v)

            return v

        new = self.new(key)
        self.set(key, new)
        return new

    def new(self, key):
        raise NotImplementedMethodeError(self, self.new)
