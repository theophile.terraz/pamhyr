# Serializable.py -- Pamhyr pickle abstract class
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import pickle


class Serializable():
    def __init__(self):
        return

    @classmethod
    def open(cls, filename):
        with open(filename, 'rb') as in_file:
            me = pickle.load(in_file)
        return me

    def _save(self):
        with open(self.filename, 'wb') as out_file:
            pickle.dump(self, out_file)
