# test_Model.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import os
import unittest
import tempfile

from Model.Saved import SavedStatus
from Model.Study import Study
from Model.River import River


class StudyTestCase(unittest.TestCase):
    def test_create_study(self):
        study = Study.new("foo", "bar")
        self.assertEqual(study.name, "foo")
        self.assertEqual(study.description, "bar")

    def test_open_study(self):
        study = Study.open("../tests_cases/Enlargement/Enlargement.pamhyr")
        self.assertNotEqual(study, None)
        self.assertEqual(study.name, "Enlargement")

    def test_save_open_study(self):
        study = Study.new("foo", "bar")
        dir = tempfile.mkdtemp()
        f = os.path.join(dir, "foo.pamhyr")

        # Save study
        study.filename = f
        study.save()
        study.close()

        # Reopen study
        study = Study.open(f)

        # Check
        self.assertNotEqual(study, None)
        self.assertEqual(study.name, "foo")
        self.assertEqual(study.description, "bar")

    def test_create_study_river(self):
        study = Study.new("foo", "bar")
        self.assertNotEqual(study.river, None)


class RiverTestCase(unittest.TestCase):
    def test_create_river(self):
        status = SavedStatus()
        river = River(status=status)

        self.assertNotEqual(river, None)

    def test_create_river_nodes(self):
        status = SavedStatus()
        river = River(status=status)

        self.assertNotEqual(river, None)

        # Add nodes
        n0 = river.add_node()
        n1 = river.add_node(x=1.0, y=0.0)
        n2 = river.add_node(x=0.0, y=1.0)

        # Checks
        self.assertEqual(river.nodes_counts(), 3)

        nodes = river.nodes()
        self.assertEqual(nodes[0], n0)
        self.assertEqual(nodes[1], n1)
        self.assertEqual(nodes[2], n2)

    def test_create_river_edges(self):
        status = SavedStatus()
        river = River(status=status)

        self.assertNotEqual(river, None)

        # Add nodes
        n0 = river.add_node()
        n1 = river.add_node(x=1.0, y=0.0)
        n2 = river.add_node(x=0.0, y=1.0)

        self.assertEqual(river.nodes_counts(), 3)

        # Add edges
        e0 = river.add_edge(n0, n1)
        e1 = river.add_edge(n1, n2)

        # Checks
        self.assertEqual(river.edges_counts(), 2)

        edges = river.edges()
        self.assertEqual(edges[0], e0)
        self.assertEqual(edges[1], e1)
