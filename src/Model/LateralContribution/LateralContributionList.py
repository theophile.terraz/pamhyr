# LateralContributionList.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import copy
from tools import trace, timer

from Model.Tools.PamhyrList import PamhyrModelListWithTab
from Model.Except import NotImplementedMethodeError

from Model.LateralContribution.LateralContribution import LateralContribution
from Model.LateralContribution.LateralContributionTypes import (
    NotDefined, LateralContrib, Rain, Evaporation,
)


class LateralContributionList(PamhyrModelListWithTab):
    _tabs_list = ["liquid", "solid", "suspenssion"]
    _sub_classes = [
        LateralContribution,
    ]

    @classmethod
    def _db_load(cls, execute, data=None):
        new = cls(status=data['status'])

        if data is None:
            data = {}

        for tab in new._tabs:
            data["tab"] = tab
            new._tabs[tab] = LateralContribution._db_load(
                execute, data
            )

        return new

    def _db_save(self, execute, data=None):
        execute("DELETE FROM lateral_contribution")

        if data is None:
            data = {}

        for tab in self._tabs:
            data["tab"] = tab
            for lc in self._tabs[tab]:
                lc._db_save(execute, data=data)

        return True

    def new(self, lst, index):
        n = NotDefined(status=self._status)
        self._tabs[lst].insert(index, n)
        self._status.modified()
        return n

    def __copy__(self):
        new = LateralContributionList()

        for lst in self._tabs:
            new.tabs[lst] = self._tabs[lst].copy()

        return new

    def __deepcopy__(self):
        new = LateralContributionList()

        for lst in self._tabs:
            new.tabs[lst] = self._tabs[lst].deepcopy()

        return new

    def copy(self):
        return copy(self)
