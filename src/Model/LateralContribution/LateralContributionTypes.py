# LateralContributionTypes.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.Except import NotImplementedMethodeError

from Model.LateralContribution.LateralContribution import LateralContribution


class NotDefined(LateralContribution):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(NotDefined, self).__init__(id=id, name=name, status=status)

        self._type = "ND"
        self._header = ["x", "y"]

    @property
    def _default_0(self):
        return 0.0


class LateralContrib(LateralContribution):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(LateralContrib, self).__init__(id=id, name=name, status=status)

        self._type = "LC"
        self._header = ["time", "discharge"]
        self._types = [LateralContrib.time_convert, float]

    @classmethod
    def compatibility(cls):
        return ["liquid"]


class Rain(LateralContribution):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(Rain, self).__init__(id=id, name=name, status=status)

        self._type = "RA"
        self._header = ["time", "discharge"]
        self._types = [Rain.time_convert, float]

    @classmethod
    def compatibility(cls):
        return ["liquid"]


class Evaporation(LateralContribution):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(Evaporation, self).__init__(id=id, name=name, status=status)

        self._type = "EV"
        self._header = ["time", "discharge"]
        self._types = [Evaporation.time_convert, float]

    @classmethod
    def compatibility(cls):
        return ["liquid"]
