# BoundaryConditionTypes.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.Except import NotImplementedMethodeError

from Model.BoundaryCondition.BoundaryCondition import BoundaryCondition


class NotDefined(BoundaryCondition):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(NotDefined, self).__init__(id=id, name=name, status=status)

        self._type = "ND"
        self._header = ["x", "y"]

    @property
    def _default_0(self):
        return 0.0


class PonctualContribution(BoundaryCondition):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(PonctualContribution, self).__init__(
            id=id, name=name, status=status)

        self._type = "PC"
        self._header = ["time", "discharge"]
        self._types = [PonctualContribution.time_convert, float]

    @classmethod
    def compatibility(cls):
        return ["liquid"]


class TimeOverZ(BoundaryCondition):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(TimeOverZ, self).__init__(id=id, name=name, status=status)

        self._type = "TZ"
        self._header = ["time", "z"]
        self._types = [TimeOverZ.time_convert, float]

    @classmethod
    def compatibility(cls):
        return ["liquid"]


class TimeOverDischarge(BoundaryCondition):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(TimeOverDischarge, self).__init__(
            id=id, name=name, status=status)

        self._type = "TD"
        self._header = ["time", "discharge"]
        self._types = [TimeOverDischarge.time_convert, float]

    @classmethod
    def compatibility(cls):
        return ["liquid"]


class ZOverDischarge(BoundaryCondition):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(ZOverDischarge, self).__init__(id=id, name=name, status=status)

        self._type = "ZD"
        self._header = ["z", "discharge"]
        self._types = [float, float]

    @classmethod
    def compatibility(cls):
        return ["liquid"]

    @property
    def _default_0(self):
        return 0.0


class Solid(BoundaryCondition):
    def __init__(self, id: int = -1, name: str = "", status=None):
        super(Solid, self).__init__(id=id, name=name, status=status)

        self.d50 = 0.002
        self.sigma = 1

        self._type = "SL"
        self._header = ["time", "solid"]
        self._types = [TimeOverDischarge.time_convert, float]

    @classmethod
    def compatibility(cls):
        return ["solid"]
