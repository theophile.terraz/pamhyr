# ReservoirList.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from copy import copy
from tools import trace, timer

from Model.Tools.PamhyrList import PamhyrModelList
from Model.Reservoir.Reservoir import Reservoir


class ReservoirList(PamhyrModelList):
    _sub_classes = [
        Reservoir,
    ]

    @classmethod
    def _db_load(cls, execute, data=None):
        new = cls(status=data['status'])

        new._lst = Reservoir._db_load(
            execute, data
        )

        return new

    def _db_save(self, execute, data=None):
        execute("DELETE FROM reservoir")
        execute("DELETE FROM reservoir_data")

        if data is None:
            data = {}

        for reservoir in self._lst:
            reservoir._db_save(execute, data=data)

        return True

    def new(self, index):
        r = Reservoir(status=self._status)
        self._lst.insert(index, r)
        self._status.modified()
        return r

    def __copy__(self):
        new = ReservoirList()

        new._lst = self._lst.copy()

        return new

    def __deepcopy__(self):
        new = ReservoirList()

        new._lst = self._lst.deepcopy()

        return new

    def copy(self):
        return copy(self)

    def get_assoc_to_node(self, node):
        assoc = list(
            filter(
                lambda i: i.node is node,
                self._lst
            )
        )

        if len(assoc) > 0:
            return assoc[0]

        return None
