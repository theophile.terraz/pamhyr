# Types.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.HydraulicStructures.Basic.HydraulicStructures import (
    BasicHS
)

from Model.HydraulicStructures.Basic.Value import (
    BHSValue
)


class NotDefined(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(NotDefined, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "ND"
        self._data = []


class DischargeWeir(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(DischargeWeir, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "S1"
        self._data = [
            BHSValue("width", float, 1.0, status=status),
            BHSValue("elevation", float, 1.0, status=status),
            BHSValue("discharge_coefficient", float, 0.4, status=status),
        ]


class TrapezoidalWeir(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(TrapezoidalWeir, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "S2"
        self._data = [
            BHSValue("width", float, 1.0, status=status),
            BHSValue("elevation", float, 1.0, status=status),
            BHSValue("loading_elevation", float, 9999.999, status=status),
            BHSValue("discharge_coefficient", float, 0.4, status=status),
            BHSValue("half-angle_tangent", float, 0.0, status=status),
        ]


class TriangularWeir(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(TriangularWeir, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "S3"
        self._data = [
            BHSValue("elevation", float, 1.0, status=status),
            BHSValue("loading_elevation", float, 9999.999, status=status),
            BHSValue("discharge_coefficient", float, 0.4, status=status),
            BHSValue("half-angle_tangent", float, 0.0, status=status),
        ]


class RectangularOrifice(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(RectangularOrifice, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "OR"
        self._data = [
            BHSValue("width", float, 0.0, status=status),
            BHSValue("elevation", float, 0.0, status=status),
            BHSValue("loading_elevation", float, 9999.999,
                     status=status),
            BHSValue("discharge_coefficient", float, 0.4, status=status),
            BHSValue("maximal_loading_elevation", float, 9999.999,
                     status=status),
        ]


class CircularOrifice(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(CircularOrifice, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "OC"
        self._data = [
            BHSValue("diameter", float, 0.0, status=status),
            BHSValue("elevation", float, 0.0, status=status),
            BHSValue("siltation_height", float, 0.0, status=status),
            BHSValue("discharge_coefficient", float, 0.4, status=status),
        ]


class VaultedOrifice(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(VaultedOrifice, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "OV"
        self._data = [
            BHSValue("elevation", float, 1.0, status=status),
            BHSValue("width", float, 1.0, status=status),
            BHSValue("top_of_the_vault", float, 0.0, status=status),
            BHSValue("bottom_of_the_vault", float, 0.0, status=status),
            BHSValue("discharge_coefficient", float, 0.4, status=status),
        ]


class RectangularGate(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(RectangularGate, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "V1"
        self._data = [
            BHSValue("width", float, 1.0, status=status),
            BHSValue("elevation", float, 0.0, status=status),
            BHSValue("discharge coefficient", float, 0.4, status=status),
            BHSValue("opening", float, 1.0, status=status),
            BHSValue("maximal_opening", float, 1.0, status=status),
        ]


class SimplifiedRectangularGate(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(SimplifiedRectangularGate, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "V2"
        self._data = [
            BHSValue("width", float, 1.0, status=status),
            BHSValue("elevation", float, 0.0, status=status),
            BHSValue("discharge coefficient", float, 0.4, status=status),
            BHSValue("opening", float, 1.0, status=status),
            BHSValue("maximal_opening", float, 1.0, status=status),
        ]


class Borda(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(Borda, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "BO"
        self._data = [
            BHSValue("step_space", float, 0.1, status=status),
            BHSValue("weir", float, 0.15, status=status),
            BHSValue("coefficient", float, 0.4, status=status),
        ]


class CheckValve(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(CheckValve, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "CV"
        self._data = [
            BHSValue("width", float, 0.0, status=status),
            BHSValue("elevation", float, 0.0, status=status),
            BHSValue("loading_elevation", float, 9999.999,
                     status=status),
            BHSValue("discharge_coefficient", float, 0.4, status=status),
            BHSValue("maximal_loading_elevation", float, 9999.999,
                     status=status),
        ]


class UserDefined(BasicHS):
    def __init__(self, id: int = -1, name: str = "",
                 status=None):
        super(UserDefined, self).__init__(
            id=id, name=name,
            status=status
        )

        self._type = "UD"
        self._data = [
            BHSValue("parameter_1", float, 0.0, status=status),
            BHSValue("parameter_2", float, 0.0, status=status),
            BHSValue("parameter_3", float, 0.0, status=status),
            BHSValue("parameter_4", float, 0.0, status=status),
            BHSValue("parameter_5", float, 0.0, status=status),
        ]


BHS_types = {
    "ND": NotDefined,
    "S1": DischargeWeir,
    "S2": TrapezoidalWeir,
    "S3": TriangularWeir,
    "OR": RectangularOrifice,
    "OC": CircularOrifice,
    # "OV": VaultedOrifice,
    "V1": RectangularGate,
    "V2": SimplifiedRectangularGate,
    "BO": Borda,
    "CV": CheckValve,
    "UD": UserDefined
}
