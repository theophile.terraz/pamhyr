# Node.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.Network.Point import Point


class Node(object):
    _id_cnt = 0

    def __init__(self, id: int, name: str,
                 x: float = 0.0, y: float = 0.0,
                 status=None):
        super(Node, self).__init__()

        self._status = status

        if id == -1:
            type(self)._id_cnt += 1
            self.id = type(self)._id_cnt
        else:
            self.id = id

        self._name = name
        self.pos = Point(x, y)

    def __getitem__(self, key):
        ret = None

        if key == "name":
            ret = self._name
        elif key == "id":
            ret = self.id
        elif key == "pos":
            ret = f"({self.pos.x},{self.pos.y})"

        return ret

    def __setitem__(self, key, value):
        if key == "name":
            self._name = value
        elif key == "id":
            self.id = value

        self._status.modified()

    @property
    def name(self):
        if self._name == "":
            return f"N{self.id}"

        return self._name

    @property
    def x(self):
        return self.pos.x

    @property
    def y(self):
        return self.pos.y

    def setPos(self, x, y):
        self.pos.x = x
        self.pos.y = y
        self._status.modified()
