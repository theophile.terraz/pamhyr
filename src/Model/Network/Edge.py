# Edge.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.Network.Node import Node


class Edge(object):
    _id_cnt = 0

    def __init__(self, id: int, name: str,
                 node1: Node = None,
                 node2: Node = None,
                 status=None):
        super(Edge, self).__init__()

        self._status = status

        if id == -1:
            type(self)._id_cnt += 1
            self.id = type(self)._id_cnt
        else:
            self.id = id

        self._name = name

        self.node1 = node1
        self.node2 = node2

        self._enable = True

    def __getitem__(self, name):
        ret = None

        if name == "name":
            ret = self.name
        elif name == "id":
            ret = self.id
        elif name == "node1":
            ret = self.node1
        elif name == "node2":
            ret = self.node2
        elif name == "enable":
            ret = self._enable

        return ret

    def __setitem__(self, name, value):
        if name == "name":
            self._name = value
        elif name == "id":
            self.id = value
        elif name == "node1":
            self.node1 = value
        elif name == "node2":
            self.node2 = value
        elif name == "enable":
            self._enable = value

        self._status.modified()

    @property
    def name(self):
        if self._name == "":
            return f"R{self.id}"

        return self._name

    def is_enable(self):
        return self._enable

    def enable(self, enable=True):
        self._enable = enable
        self._status.modified()

    def disable(self):
        self._enable = False
        self._status.modified()

    def reverse(self):
        tmp = self.node1
        self.node1 = self.node2
        self.node2 = tmp
        self._status.modified()
