# test_Network.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import os
import unittest
import tempfile

from Model.Saved import SavedStatus

from Model.Network.Graph import Graph
from Model.Network.Edge import Edge
from Model.Network.Node import Node


def new_graph():
    status = SavedStatus()
    g = Graph(status=status)

    return g


class GraphTestCase(unittest.TestCase):
    def test_graph_0(self):
        g = new_graph()

        self.assertEqual(g.nodes_counts(), 0)
        self.assertEqual(g.edges_counts(), 0)

    def test_graph_1(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()

        e0 = g.add_edge(n0, n1)

        self.assertEqual(g.nodes_counts(), 2)
        self.assertEqual(g.edges_counts(), 1)

    def test_graph_2(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()

        e0 = g.add_edge(n0, n1)

        g.remove_edge(e0)

        self.assertEqual(g.nodes_counts(), 2)
        self.assertEqual(g.edges_counts(), 0)

    def test_graph_3(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()

        e0 = g.add_edge(n0, n1)

        g.remove_node(n0)

        self.assertEqual(g.nodes_counts(), 1)
        self.assertEqual(g.edges_counts(), 0)

    def test_graph_4(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()
        n2 = g.add_node()

        e0 = g.add_edge(n0, n1)
        e1 = g.add_edge(n1, n2)

        g.remove_node(n1)

        self.assertEqual(g.nodes_counts(), 2)
        self.assertEqual(g.edges_counts(), 0)

    def test_graph_upstream_0(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()
        n2 = g.add_node()

        e0 = g.add_edge(n0, n1)
        e1 = g.add_edge(n1, n2)

        self.assertEqual(g.is_upstream_node(n0), True)
        self.assertEqual(g.is_upstream_node(n1), False)
        self.assertEqual(g.is_upstream_node(n2), False)

    def test_graph_upstream_1(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()
        n2 = g.add_node()

        e0 = g.add_edge(n0, n1)
        e1 = g.add_edge(n2, n1)

        self.assertEqual(g.is_upstream_node(n0), True)
        self.assertEqual(g.is_upstream_node(n1), False)
        self.assertEqual(g.is_upstream_node(n2), True)

    def test_graph_upstream_disable(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()
        n2 = g.add_node()

        e0 = g.add_edge(n0, n1)
        e1 = g.add_edge(n1, n2)

        e0.disable()

        # self.assertEqual(g.is_upstream_node(n0), False)
        self.assertEqual(g.is_upstream_node(n1), True)
        self.assertEqual(g.is_upstream_node(n2), False)

    def test_graph_downstream_0(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()
        n2 = g.add_node()

        e0 = g.add_edge(n0, n1)
        e1 = g.add_edge(n1, n2)

        self.assertEqual(g.is_downstream_node(n0), False)
        self.assertEqual(g.is_downstream_node(n1), False)
        self.assertEqual(g.is_downstream_node(n2), True)

    def test_graph_downstream_1(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()
        n2 = g.add_node()

        e0 = g.add_edge(n0, n1)
        e1 = g.add_edge(n2, n1)

        self.assertEqual(g.is_downstream_node(n0), False)
        self.assertEqual(g.is_downstream_node(n1), True)
        self.assertEqual(g.is_downstream_node(n2), False)

    def test_graph_downstream_disable(self):
        g = new_graph()

        n0 = g.add_node()
        n1 = g.add_node()
        n2 = g.add_node()

        e0 = g.add_edge(n0, n1)
        e1 = g.add_edge(n1, n2)

        e1.disable()

        self.assertEqual(g.is_downstream_node(n0), False)
        self.assertEqual(g.is_downstream_node(n1), True)
        # self.assertEqual(g.is_downstream_node(n2), False)
