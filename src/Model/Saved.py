# Saved.py -- Pamhyr model status class
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging

logger = logging.getLogger()


class SavedStatus(object):
    def __init__(self, version=0):
        super(SavedStatus, self).__init__()
        self._version = version
        self._saved = True

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version

    def is_saved(self):
        return self._saved

    def save(self):
        logger.debug("model status set as saved")
        self._saved = True

    def modified(self):
        if self._saved:
            self._version += 1

        logger.debug(
            "STATUS: Model status set as modified " +
            f"at version {self._version}"
        )
        self._saved = False
