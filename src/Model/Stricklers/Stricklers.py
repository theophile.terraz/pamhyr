# Stricklers.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from tools import trace, timer

from Model.Tools.PamhyrDB import SQLSubModel


class Stricklers(SQLSubModel):
    _id_cnt = 0
    _sub_classes = []

    def __init__(self, id: int = -1,
                 name: str = "",
                 comment: str = "",
                 minor: float = 35.0,
                 medium: float = 15.0,
                 status=None):
        super(Stricklers, self).__init__()
        self._status = status

        if id == -1:
            self.id = Stricklers._id_cnt
        else:
            self.id = id

        Stricklers._id_cnt = max(Stricklers._id_cnt + 1, id + 1)

        self._name = name
        self._comment = comment

        self._minor = minor
        self._medium = medium

    @classmethod
    def _db_create(cls, execute):
        execute("""
          CREATE TABLE stricklers(
            id INTEGER NOT NULL PRIMARY KEY,
            name TEXT,
            comment TEXT,
            minor REAL NOT NULL,
            medium REAL NOT NULL
          )
        """)

        return cls._create_submodel(execute)

    @classmethod
    def _db_update(cls, execute, version):
        return cls._update_submodel(execute, version)

    @classmethod
    def _db_load(cls, execute, data=None):
        stricklers = []
        status = data["status"]

        table = execute(
            "SELECT id, name, comment, minor, medium " +
            "FROM stricklers"
        )

        if table is None:
            return None

        for row in table:
            id = row[0]
            name = row[1]
            comment = row[2]
            minor = row[3]
            medium = row[4]

            new = cls(
                id=id,
                name=name,
                comment=comment,
                minor=minor, medium=medium,
                status=status
            )

            stricklers.append(new)

        return stricklers

    def _db_save(self, execute, data=None):
        sql = (
            "INSERT INTO " +
            "stricklers(id, name, comment, minor, medium) " +
            "VALUES (" +
            f"{self.id}, " +
            f"'{self._db_format(self.name)}', " +
            f"'{self._db_format(self.comment)}', " +
            f"{float(self.minor)}, {float(self.medium)}" +
            ")"
        )
        execute(sql)

        return True

    def __str__(self):
        return f"{self.name} ({self._minor}, {self._medium})"

    @property
    def name(self):
        if self._name == "":
            return f"K{self.id + 1}"
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def comment(self):
        return self._comment

    @comment.setter
    def comment(self, comment):
        self._comment = comment

    @property
    def minor(self):
        return self._minor

    @minor.setter
    def minor(self, minor):
        self._minor = float(minor)

    @property
    def medium(self):
        return self._medium

    @medium.setter
    def medium(self, medium):
        self._medium = float(medium)
