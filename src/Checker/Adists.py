# Adists.py -- Pamhyr study checkers
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import time
import logging
from functools import reduce

from PyQt5.QtCore import QCoreApplication

from Modules import Modules
from Checker.Checker import AbstractModelChecker, STATUS

_translate = QCoreApplication.translate

logger = logging.getLogger()


class AdistsOutputRKChecker(AbstractModelChecker):
    def __init__(self):
        super(AdistsOutputRKChecker, self).__init__()

        self._name = _translate("Checker", "AdisTS output RK checker")
        self._description = _translate(
            "Checker", "Check output RK"
        )
        self._modules = Modules.OUTPUT_RK

    def run(self, study):
        ok = True
        nerror = 0
        self._summary = "ok"
        self._status = STATUS.OK

        if not self.basic_check(study):
            return False

        return ok
