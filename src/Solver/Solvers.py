# Solvers.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from PyQt5.QtCore import QCoreApplication

from Solver.GenericSolver import GenericSolver
from Solver.Mage import (
    Mage7, Mage8, MageFake7,
)
from Solver.AdisTS import AdisTSwc
from Solver.RubarBE import Rubar3, RubarBE

_translate = QCoreApplication.translate

solver_long_name = {
    # "generic": "Generic",
    # "mage7": "Mage v7",
    "mage8": "Mage v8",
    # "mage_fake7": "Mage fake v7",
    "adistswc": "Adis-TS_WC",
    # "rubarbe": "RubarBE",
    # "rubar3": "Rubar3",
}

solver_type_list = {
    # "generic": GenericSolver,
    # "mage7": Mage7,
    "mage8": Mage8,
    # "mage_fake7": MageFake7,
    "adistswc": AdisTSwc,
    # "rubarbe": RubarBE,
    # "rubar3": Rubar3,
}
