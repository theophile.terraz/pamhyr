# test_Model.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import os
import unittest
import warnings
import tempfile

from Model.Saved import SavedStatus
from Model.Study import Study
from Model.River import River

from Meshing.Mage import MeshingWithMage, MeshingWithMageMailleurTT

mage_mesher = os.path.exists(MeshingWithMage._lib_path())
mage_mesher_TT = os.path.exists(MeshingWithMageMailleurTT._exe_path())


class MeshingTestCase(unittest.TestCase):
    @unittest.skipIf(not mage_mesher, "libbief is not available")
    def test_create_meshingtool_mage(self):
        mesher = MeshingWithMage()
        self.assertNotEqual(mesher, None)

    @unittest.skipIf(not mage_mesher_TT, "mailleurTT is not available")
    def test_create_meshingtool_mageTT(self):
        mesher = MeshingWithMageMailleurTT()
        self.assertNotEqual(mesher, None)
