# Run.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import os
import logging

from queue import Queue

from PyQt5.QtCore import QProcess

from Scripts.AScript import AScript
from Model.Study import Study

logger = logging.getLogger()


class ScriptRun(AScript):
    name = "Run"
    description = "Run solver on Pamhyr2 a study"

    def usage(self):
        logger.info(
            f"Usage : {self._args[0]} {self._args[1]} " +
            "<SOLVER> <STUDY>"
        )

    def run(self):
        if len(self._args) < 4:
            return 1

        command = self._args[1]
        solver_name = self._args[2]
        study_file = os.path.abspath(
            self._args[3]
        )

        try:
            solver = next(
                filter(
                    lambda solver: solver.name == solver_name,
                    self._conf.solvers
                )
            )
        except Exception as e:
            logger.error(f"No solver found: {e}")
            return 2

        study = Study.open(study_file)

        self._solver = solver
        self._study = study

        logger.info(
            f"Run {solver.name} ({solver.type}) " +
            "on study '{study.name}' ({study_file})"
        )

        # Workdir
        workdir = os.path.join(
            os.path.dirname(study.filename),
            "_PAMHYR_",
            study.name.replace(" ", "_"),
            solver.name.replace(" ", "_")
        )
        os.makedirs(workdir, exist_ok=True)
        logger.info(f"Set working dir to {workdir}")

        # Preparate process
        p = QProcess(None)
        p.setWorkingDirectory(workdir)

        self._q = Queue()

        # Export and Run
        logger.info(f"~Export~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        solver.export(study, workdir, qlog=self._q)

        while self._q.qsize() != 0:
            s = self._q.get()
            logger.info(s)

        if command == "run":
            logger.info(f"~Run~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            solver.run(
                study,
                process=p,
                output_queue=self._q
            )
            p.waitForFinished()

        logger.info(f"~End~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

        return 0


class ScriptExport(ScriptRun):
    name = "Export"
    description = "Export Pamhyr2 study for solver"
