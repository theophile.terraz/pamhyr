# Hello.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import logging
from Scripts.AScript import AScript

logger = logging.getLogger()


class ScriptHello(AScript):
    name = "Hello"
    description = "Display hello"

    def usage(self):
        logger.info(f"Usage : {self._args[0]} hello")

    def run(self):
        logger.info("Hello from Pamhyr2 script !")
        return 0
