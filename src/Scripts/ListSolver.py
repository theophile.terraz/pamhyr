# ListSolver.py -- Pamhyr
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import os
import logging

from Scripts.AScript import AScript

logger = logging.getLogger()


class ScriptListSolver(AScript):
    name = "ListSolver"
    description = "List configured solver(s) for Pamhyr2"

    def usage(self):
        logger.info(f"Usage : {self._args[0]} {self._args[1]}")

    def run(self):
        for solver in self._conf.solvers:
            print(f"{solver.name:<16} ({solver.type}): {solver.description}")

        return 0
