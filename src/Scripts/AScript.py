# AScript.py -- Pamhyr abstract script class
# Copyright (C) 2023-2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

from Model.Except import NotImplementedMethodeError


class AScript(object):
    name = ""
    description = ""

    def __init__(self, app, conf, args):
        super(AScript, self).__init__()

        self._app = app
        self._conf = conf
        self._args = args.copy()

    def usage(self):
        raise NotImplementedMethodeError(self, self.usage)

    def run(self):
        raise NotImplementedMethodeError(self, self.run)
