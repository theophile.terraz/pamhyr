#! /bin/sh

# linux.sh -- Pamhyr
# Copyright (C) 2023  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

echo " *** RM OLD ENV"

rm dist/ -rf
rm linux-pamhyr-venv -rf

echo " *** SETUP ENV"

python3 -m venv linux-pamhyr-venv
. linux-pamhyr-venv/bin/activate
pip3 install -U pip
pip3 install -r ../requirements.txt

echo " *** PYINSTALL"

pyinstaller -y                                                          \
            --paths linux-pamhyr-venv/lib/python3.8/site-packages       \
            ../src/pamhyr.py

echo " *** MAKE QM"

# Update TS and build QM files
OLD_PWD=$PWD
cd ../src/lang/
./create_ts.sh
cd $OLD_PWD

echo " *** COPY DATA"

mkdir -p dist/pamhyr/_internal/View/ui
mkdir -p dist/pamhyr/_internal/View/ui/Widgets
cp -r ../src/View/ui/ressources/ dist/pamhyr/_internal/View/ui/
cp -r ../src/View/ui/Widgets/*.ui dist/pamhyr/_internal/View/ui/Widgets/
cp -r ../src/View/ui/*.ui dist/pamhyr/_internal/View/ui/

mkdir -p dist/pamhyr/lang
cp -r ../src/lang/*.qm dist/pamhyr/lang/

mkdir dist/pamhyr/_internal/
#cp ../VERSION dist/pamhyr/_internal/
VERSION=$(cat ../VERSION)
echo "local-build-$VERSION" > dist/pamhyr/_internal/VERSION
cp ../AUTHORS dist/pamhyr/_internal/
cp ../LICENSE dist/pamhyr/_internal/
cp ../src/motd.txt dist/pamhyr/_internal/

mkdir -p dist/pamhyr/mage/
cp ../mage/mage dist/pamhyr/mage/
cp ../mage/mage_extraire dist/pamhyr/mage/
cp ../mage/mailleurPF dist/pamhyr/mage/

mkdir -p dist/pamhyr/tests_cases/
mkdir -p dist/pamhyr/tests_cases/Saar
cp ../tests_cases/Saar/Saar.pamhyr dist/pamhyr/tests_cases/Saar/

mkdir -p dist/pamhyr/doc/
cp ../doc/dev/documentation.pdf dist/pamhyr/doc/Pamhyr2-dev.pdf
cp ../doc/dev/documentation.html dist/pamhyr/doc/Pamhyr2-dev.html

cp ../doc/users/documentation.pdf dist/pamhyr/doc/Pamhyr2-users.pdf
cp ../doc/users/documentation.html dist/pamhyr/doc/Pamhyr2-users.html

mkdir -p dist/pamhyr/doc/images
cp ../doc/users/images/* dist/pamhyr/doc/images/
cp ../doc/dev/images/* dist/pamhyr/doc/images/
cp ../doc/images/* dist/pamhyr/doc/images/

echo " *** MAKE SRC PACKAGE"

OLD_PWD=$PWD
cd ../
git archive --format=tar.gz -o pamhyr-src.tar.gz HEAD
cd $OLD_PWD

mv ../pamhyr-src.tar.gz ./

echo " *** MAKE BIN PACKAGE"

# OLD_PWD=$PWD
# cd dist/
# tar --xz -cf pamhyr-gnulinux-amd64.tar.xz pamhyr --checkpoint=.100
# cd $OLD_PWD

# mv dist/pamhyr-gnulinux-amd64.tar.xz ./

echo " *** DONE"
