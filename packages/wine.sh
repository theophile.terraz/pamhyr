#! /bin/sh

# wine.sh -- Pamhyr
# Copyright (C) 2023  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

echo " *** SETUP ENV"

export WINARCH=win64

if [ "$1" = "ci" ];
then
    # HACK: Set wine prefix for gitlab-ci runner (DONT WORKS)
    cp -rfv /home/parouby/git/pamhyr/packages/wine-pamhyr-build $PWD/wine-pamhyr-build
    chown $USER -R $PWD/wine-pamhyr-build
    chmod 755 $PWD/wine-pamhyr-build
    export WINEPREFIX=$PWD/wine-pamhyr-build
fi


if [ -z $WINEPREFIX ];
then
    # FIXME Not tested yet...
    export WINEPREFIX=$(realpath ./wine-pamhyr-build)

    if [ ! -f python-3.8.10-amd64.exe ];
    then
        wget https://www.python.org/ftp/python/3.8.10/python-3.8.10-amd64.exe
    fi

    if [ ! -f nsis-3.08-setup.exe ];
    then
        wget https://freefr.dl.sourceforge.net/project/nsis/NSIS%203/3.08/nsis-3.08-setup.exe
    fi

    # Dummy display
    Xvfb :42 -screen 0 1024x768x16 &
    jid=$!

    # Install soft
    winetricks                                  \
        corefonts                               \
        7zip                                    \
        win10

    echo " *** PYTHON INSTALL"
    if [ ! -f "$WINEPREFIX/drive_c/Program Files/Python38/python.exe" ];
    then
        #DISPLAY=:42.0 wine cmd /c python-3.8.10-amd64.exe /quiet PrependPath=1
        wine cmd /c python-3.8.10-amd64.exe PrependPath=1
        wine python $WINEPREFIX/drive_c/Program\ Files/Python38/Tools/scripts/win_add2path.py
    fi

    echo " *** PIP UPGRADE AND DEPENDENCIES"
    wine python -m pip install --upgrade pip
    wine python -m pip install pyinstaller

    echo " *** NSIS INSTALL"
    if [ ! -f $WINEPREFIX/drive_c/Program\ Files\ \(x86\)/NSIS/makensis.exe ];
    then
        wine cmd /c nsis-3.08-setup.exe
    fi
fi

echo " *** PYINSTALL"

wine python -m pip install -r $(realpath ../requirements.txt)
wine pyinstaller ../src/pamhyr.py -y

echo " *** COPY DATA"

mkdir -p pamhyr
mkdir -p pamhyr/pamhyr

# Copy Pamhyr info
cp -v ../VERSION pamhyr/pamhyr/
cp -v ../AUTHORS pamhyr/pamhyr/
cp -v ../LICENSE pamhyr/pamhyr/

# Copy mage
mkdir -p pamhyr/mage
cp -v ../mage/* pamhyr/mage/

# Copy Pamhyr
cp -r dist/pamhyr pamhyr/

# Copy Pamhyr resources
mkdir -p pamhyr/pamhyr/View
mkdir -p pamhyr/pamhyr/View/ui
mkdir -p pamhyr/pamhyr/View/ui/Widgets
cp -r ../src/View/ui/*.ui pamhyr/pamhyr/View/ui/
cp -r ../src/View/ui/ressources/ pamhyr/pamhyr/View/ui
cp -r ../src/View/ui/Widgets/*.ui pamhyr/pamhyr/View/ui/Widgets

# Copy lang
mkdir -p pamhyr/pamhyr/lang
cp -r ../src/lang/*.qm pamhyr/pamhyr/lang

# Copy tests cases
mkdir -p pamhyr/tests_cases/
mkdir -p pamhyr/tests_cases/Saar
cp ../tests_cases/Saar/Saar.pamhyr pamhyr/tests_cases/Saar/

# Copy documentations
mkdir -p pamhyr/doc/
cp ../doc/users/documentation.pdf pamhyr/doc/Pamhyr2-users.pdf
cp ../doc/dev/documentation.pdf pamhyr/doc/Pamhyr2-dev.pdf

# Update TS and build QM files
OLD_PWD=$PWD
cd ../src/lang/
./create_ts.sh
cd $OLD_PWD

mkdir -p dist/pamhyr/lang
cp -r ../src/lang/*.qm dist/pamhyr/lang/

echo " *** MAKE PACKAGES"

echo " TAR pamhyr-win-amd64.tar.xz"

# OLD_PWD=$PWD
# cd dist/
# tar --xz -cf pamhyr-win-amd64.tar.xz pamhyr --checkpoint=.100
# cd $OLD_PWD
# mv dist/pamhyr-win-amd64.tar.xz ./

echo " ZIP pamhyr-win-amd64.zip"

# OLD_PWD=$PWD
# cd dist/
# zip -r pamhyr-win-amd64.zip pamhyr/
# cd $OLD_PWD
# mv dist/pamhyr-win-amd64.zip ./

echo " *** MAKE INSTALLER"

wine $WINEPREFIX/drive_c/Program\ Files\ \(x86\)/NSIS/makensis.exe   \
     pamhyr.nsi
