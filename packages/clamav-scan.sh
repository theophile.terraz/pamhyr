#! /bin/bash
set +x

OPTS="-r -a -i"

if [ "$1" == "builds" ]; then
    FILES="../windows/ ../linux/"
else
    FILES="../windows/pamhyr-windows.exe ../windows/pamhyr-windows.zip ../linux/pamhyr-gnulinux.tar.xz"
fi

clamscan $OPTS $FILES | tee clamav.log

RES=$(cat clamav.log | grep "Infected files: 0")
if [ -z "$RES" ]; then
    echo "Infected files"
    exit -1
else
    echo "OK"
    exit 0
fi
