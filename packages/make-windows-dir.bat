rem windows.bat -- Pamhyr Windows batch for windows version building
rem Copyright (C) 2023  INRAE
rem
rem This program is free software: you can redistribute it and/or modify
rem it under the terms of the GNU General Public License as published by
rem the Free Software Foundation, either version 3 of the License, or
rem (at your option) any later version.
rem
rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.
rem
rem You should have received a copy of the GNU General Public License
rem along with this program.  If not, see <https://www.gnu.org/licenses/>.

@ECHO ON

mkdir pamhyr\_internal
xcopy /s /e /y dist\pamhyr pamhyr

rem Copy data
mkdir pamhyr\_internal\lang
mkdir pamhyr\_internal\View\ui\ressources
mkdir pamhyr\_internal\View\ui\Widgets

rem UI
copy /y ..\src\View\ui\ressources\ pamhyr\_internal\View\ui\ressources
copy /y ..\src\View\ui\Widgets\*.ui pamhyr\_internal\View\ui\Widgets
copy /y ..\src\View\ui\*.ui pamhyr\_internal\View\ui\

rem Lang
copy /y ..\src\lang\*.qm pamhyr\_internal\lang\

rem Information
copy /y ..\VERSION pamhyr\_internal
copy /y ..\AUTHORS pamhyr\_internal
copy /y ..\LICENSE pamhyr\_internal
copy /y ..\src\motd.txt pamhyr\_internal

rem MAGE
mkdir pamhyr\mage8
copy /y ..\mage8-windows\mage.exe pamhyr\mage8\
copy /y ..\mage8-windows\mage_as7.exe pamhyr\mage8\
copy /y ..\mage8-windows\mage_extraire.exe pamhyr\mage8\
copy /y ..\mage8-windows\mailleurTT.exe pamhyr\mage8\
copy /y ..\mage8-windows\libbief.dll pamhyr\mage8\

rem adists
mkdir pamhyr\adists
copy /y ..\adists-windows\adists.exe pamhyr\adists\

rem Copy tests_cases
mkdir pamhyr\tests_cases
mkdir pamhyr\tests_cases\Saar
copy /y ..\tests_cases\Saar\Saar.pamhyr pamhyr\tests_cases\Saar\

rem Documentations
mkdir pamhyr\doc
mkdir pamhyr\doc\images
rem copy /y ..\doc\users\documentation.pdf pamhyr\doc\Pamhyr2-users.pdf
copy /y ..\doc\dev\documentation.pdf pamhyr\doc\Pamhyr2-dev.pdf
copy /y ..\doc\mage8.pdf pamhyr\doc\mage8.pdf
rem copy /y ..\doc\users\documentation.html pamhyr\doc\Pamhyr2-users.html
copy /y ..\doc\dev\documentation.html pamhyr\doc\Pamhyr2-dev.html
copy /y ..\doc\users\images\* pamhyr\doc\images
copy /y ..\doc\dev\images\* pamhyr\doc\images
copy /y ..\doc\images\* pamhyr\doc\images

rem Tuto
mkdir pamhyr\doc\TP_Hydraulique_Hogneau\
mkdir pamhyr\doc\TP_Hydraulique_Hogneau\data
copy /y ..\doc\users\TP_Hydraulique_Hogneau\Hogneau_Engees2022.pdf pamhyr\doc\TP_Hydraulique_Hogneau\Hogneau_Engees2022.pdf
copy /y ..\doc\users\TP_Hydraulique_Hogneau\step-by-step.pdf pamhyr\doc\TP_Hydraulique_Hogneau\Tuto1-en.pdf
copy /y ..\doc\users\TP_Hydraulique_Hogneau\pas-a-pas.pdf pamhyr\doc\TP_Hydraulique_Hogneau\Tuto1-fr.pdf
copy /y ..\doc\users\TP_Hydraulique_Hogneau\Tuto-2-fr.pdf pamhyr\doc\TP_Hydraulique_Hogneau\Tuto2-fr.pdf
copy /y ..\doc\users\TP_Hydraulique_Hogneau\*.pamhyr pamhyr\doc\TP_Hydraulique_Hogneau\
copy /y ..\doc\users\TP_Hydraulique_Hogneau\data\* pamhyr\doc\TP_Hydraulique_Hogneau\data\

rem TP charriage
mkdir pamhyr\doc\TP_charriage
mkdir pamhyr\doc\TP_charriage\Donnees_TP_Pamhyr2_charriage
copy /y ..\doc\users\TP_charriage\TP_Pamhyr2_charriage.pdf pamhyr\doc\TP_charriage\
copy /y ..\doc\users\TP_charriage\Donnees_TP_Pamhyr2_charriage\* pamhyr\doc\TP_charriage\Donnees_TP_Pamhyr2_charriage\

rem TP AdisTS
mkdir pamhyr\doc\TP_AdisTS_Vieux_Rhone\
mkdir pamhyr\doc\TP_AdisTS_Vieux_Rhone\data
copy /y ..\doc\users\TP_AdisTS_Vieux_Rhone\*.pdf pamhyr\doc\TP_AdisTS_Vieux_Rhone\
copy /y ..\doc\users\TP_AdisTS_Vieux_Rhone\*.pamhyr pamhyr\doc\TP_AdisTS_Vieux_Rhone\
copy /y ..\doc\users\TP_AdisTS_Vieux_Rhone\data\* pamhyr\doc\TP_AdisTS_Vieux_Rhone\data\
