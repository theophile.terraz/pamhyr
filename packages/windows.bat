rem windows.bat -- Pamhyr Windows batch for windows version building
rem Copyright (C) 2023  INRAE
rem
rem This program is free software: you can redistribute it and/or modify
rem it under the terms of the GNU General Public License as published by
rem the Free Software Foundation, either version 3 of the License, or
rem (at your option) any later version.
rem
rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.
rem
rem You should have received a copy of the GNU General Public License
rem along with this program.  If not, see <https://www.gnu.org/licenses/>.

@ECHO ON

rem Python environment
python -m pip install -r ..\requirements.txt

rem Build windows version
pyinstaller ..\src\pamhyr.py -y

rem Copy data
mkdir dist\pamhyr\View\ui\ressources
mkdir dist\pamhyr\View\ui\Widgets

rem UI
copy /y ..\src\View\ui\ressources\ dist\pamhyr\View\ui\ressources
copy /y ..\src\View\ui\Widgets\*.ui dist\pamhyr\View\ui\Widgets
copy /y ..\src\View\ui\*.ui dist\pamhyr\View\ui\

rem Lang
copy /y ..\src\lang\*.qm dist\pamhyr\lang\

rem Information
copy /y ..\VERSION dist\pamhyr\
copy /y ..\AUTHORS dist\pamhyr\
copy /y ..\LICENSE dist\pamhyr\

rem MAGE
mkdir dist\pamhyr\mage
copy /y ..\mage\mage.exe dist\pamhyr\mage\
copy /y ..\mage\mage_extraire.exe dist\pamhyr\mage\
copy /y ..\mage\mailleurPF.exe dist\pamhyr\mage\

rem Copy tests_cases
mkdir dist\pamhyr\tests_cases
mkdir dist\pamhyr\tests_cases\Saar
copy /y ..\tests_cases\Saar\Saar.pamhyr dist\pamhyr\tests_cases\Saar\

rem Make installer
"C:\Program Files (x86)\NSIS\makensis.exe" pamhyr.nsi
