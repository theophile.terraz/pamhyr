!include "x64.nsh"

!define LIC_NAME "LICENSE"
!define APP_NAME "PAMHYR"

Icon "pamhyr\_internal\View\ui\ressources\icon.ico"
Name "Pamhyr2"
OutFile "pamhyr-windows.exe"
LicenseData "pamhyr\_internal\LICENSE"
LicenseText "I Agree"

RequestExecutionLevel admin

Unicode True
InstallDir $PROGRAMFILES\Pamhyr2

InstallDirRegKey HKLM "Software\Pamhyr2" "Install_Dir"

Page license
Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

Section "Pamhyr2"
  SectionIn RO

  SetOutPath $INSTDIR

  File /r "pamhyr\*"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pamhyr2" "DisplayName" "NSIS Pamhyr2"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pamhyr2" "UninstallString" "$INSTDIR\uninstall.exe"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pamhyr2" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pamhyr2" "NoRepair" 1
  WriteUninstaller "$INSTDIR\uninstall.exe"

SectionEnd

Section "Start Menu Shortcuts"
  CreateDirectory "$SMPROGRAMS\Pamhyr2"
  CreateShortcut "$SMPROGRAMS\Pamhyr2\Uninstall.lnk" "$INSTDIR\uninstall.exe"
  CreateShortcut "$SMPROGRAMS\Pamhyr2\Pamhyr2.lnk" "$INSTDIR\pamhyr.exe"
SectionEnd

Section "Desktop Shortcuts"
  SetShellVarContext all
  CreateShortcut "$desktop\Pamhyr2.lnk" "$instdir\pamhyr.exe"
SectionEnd

Section "Uninstall"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Pamhyr2"
  DeleteRegKey HKLM SOFTWARE\NSIS_Pamhyr2

  Delete "$SMPROGRAMS\Pamhyr2\*.lnk"
  Delete "$desktop\Pamhyr2.lnk"

  RMDir "$SMPROGRAMS\Pamhyr2"
  RMDir /R "$INSTDIR"
SectionEnd