#! /bin/sh

echo " PDF  documentation.org"
emacs -nw --batch                                       \
      --eval "(require 'org)"                           \
      --eval "(require 'ox-latex)"                      \
      --eval "(load-file \"../tools/setup.el\")"        \
      --file ./documentation.org                        \
      -f org-latex-export-to-pdf

echo " HTML documentation.org"
emacs -nw --batch                                       \
      --eval "(require 'org)"                           \
      --eval "(require 'ox-latex)"                      \
      --eval "(require 'ox-html)"                       \
      --eval "(load-file \"../tools/setup.el\")"        \
      --file ./documentation.org                        \
      -f org-html-export-to-html
