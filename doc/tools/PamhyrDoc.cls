\ProvidesClass{Pamhyr}[Pamhyr class]
\NeedsTeXFormat{LaTeX2e}
\ProcessOptions\relax
\LoadClass[11pt,a4paper]{article}

\usepackage[hmargin=2cm, vmargin=3cm]{geometry}
\usepackage[british,UKenglish,USenglish,english,american]{babel}
\usepackage[T1]{fontenc}
\usepackage{times}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[absolute]{textpos}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage[minted,xparse,breakable]{tcolorbox}
\usepackage{pmboxdraw}

\usepackage{enumitem}
\setlist[enumerate]{itemsep=-0.5em}
\setlist[itemize]{itemsep=-0.5em}

%%
%% Bibtex
%%

\bibliographystyle{plain}

%%
%% Colors
%%

\definecolor{primary}{HTML}{212121}
\definecolor{lightheadings}{HTML}{5c5c5c}
\definecolor{headings}{HTML}{212121}
\definecolor{links}{HTML}{e12121}

%%
%% Title
%%

\setlength{\columnsep}{20pt}
\setlength{\parskip}{0pt}      %% inter paragraph space

\renewcommand{\maketitle}{
    {
      \centering
      {
        \fontsize{35}{40}
        \selectfont
        \scshape
        Pamhyr2 \\
        \medskip
      }
      {
        %\centering
        \scshape
        \fontsize{20pt}{24pt}
        \selectfont
        A graphical user interface for 1D hydro-sedimentary modelling
        of rivers \\
        \medskip
      }
      \rule{3cm}{0.4pt}\\
      \vspace{0.3cm}
      {
        %\centering
        \scshape
        \fontsize{26pt}{24pt}
        \selectfont
        \@title \\
        Document licence: GPLv3 \\
        \medskip
        \includegraphics[width=3cm]{../images/GPLv3_Logo.png} \\
        \medskip
        \fontsize{14pt}{16pt}
        \selectfont
        © \@author \\
        \medskip
      }
      {
        %\centering
        %\scshape
        \fontsize{12pt}{14pt}
        \selectfont
        \@date
        %\medskip
      }\\
      \vspace{0.3cm}
      \rule{3cm}{0.4pt}\\
      \vspace{0.3cm}
      {
        \begin{minipage}[h]{0.24\textwidth}
          \includegraphics[width=4cm]{../../src/View/ui/ressources/Logo-INRAE.png}
        \end{minipage}
        \begin{minipage}[h]{0.24\textwidth}
          \includegraphics[width=4cm]{../images/riverly.png}
        \end{minipage}
      }\\
      \vspace{0.3cm}
      %%\rule{\textwidth}{0.4pt}
      %%\newpage
    }
}


%%
%% Texttt
%%

\usepackage[htt]{hyphenat}      % Make texttt split at end of line

%%
%% Verbatim
%%

\AtBeginEnvironment{verbatim}{\footnotesize}

%% Add box arrond of verbatim
\BeforeBeginEnvironment{verbatim}{\begin{tcolorbox}[boxsep=0pt, left=0.1cm, right=0.1cm, arc=0pt, boxrule=0.5pt, colback=white]}%
\AfterEndEnvironment{verbatim}{\end{tcolorbox}}%

\makeatletter
\AtBeginEnvironment{verbatim}{\dontdofcolorbox}

\def\dontdofcolorbox{\renewcommand\fcolorbox[4][]{##4}}
\makeatother

%%
%% Source code blocks
%%

%% Use minted
\usepackage{minted}
\usemintedstyle{tango}
\setminted[c]{fontsize=\footnotesize,encoding=utf8,linenos}
\setminted[c++]{breaklines,fontsize=\footnotesize,encoding=utf8,linenos}
\setminted[python]{breaklines,fontsize=\footnotesize,encoding=utf8,linenos}
\setminted[sql]{breaklines,fontsize=\footnotesize,encoding=utf8,linenos}
\setminted[shell]{breaklines,fontsize=\footnotesize,encoding=utf8}
\setminted[scheme]{breaklines,fontsize=\footnotesize,encoding=utf8}
\setminted[commun-lisp]{breaklines,fontsize=\footnotesize,encoding=utf8}
\setminted[text]{breaklines,fontsize=\footnotesize,encoding=utf8}
\setminted[llvm]{breaklines,fontsize=\footnotesize,encoding=utf8}

%% Add box arrond of minted
\BeforeBeginEnvironment{minted}{\begin{tcolorbox}[breakable, boxsep=0pt, left=0.1cm, right=0.1cm, arc=0pt, boxrule=0.5pt, colback=white]}%
\AfterEndEnvironment{minted}{\end{tcolorbox}}%

\makeatletter
\AtBeginEnvironment{minted}{\dontdofcolorbox}

\def\dontdofcolorbox{\renewcommand\fcolorbox[4][]{##4}}
\makeatother


%%
%% Water mark
%%

%% FIXME: Delete me for the first realease
\usepackage{draftwatermark}
\SetWatermarkLightness{0.8}
\SetWatermarkAngle{25}
\SetWatermarkScale{3}
\SetWatermarkFontSize{1cm}
\SetWatermarkText{Work in progress}

%% Icons

\usepackage{fontawesome5}
