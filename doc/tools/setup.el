(require 'org)
(require 'subr-x)

;; LaTeX config

(add-to-list
 'org-latex-classes
 '("PamhyrDoc"
   "\\documentclass{../tools/PamhyrDoc}"
   ("\\section{%s}" . "\\section*{%s}")
   ("\\subsection{%s}" . "\\subsection*{%s}")
   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
   ("\\paragraph{%s}" . "\\paragraph*{%s}")
   ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(setq org-confirm-babel-evaluate nil)
(setq org-latex-caption-above nil)

(org-babel-do-load-languages
   'org-babel-load-languages
   (add-to-list 'org-babel-load-languages '(shell . t)))

(org-babel-do-load-languages
 'org-babel-load-languages
 (add-to-list 'org-babel-load-languages '(dot . t)))

(org-babel-do-load-languages
 'org-babel-load-languages
 (add-to-list 'org-babel-load-languages '(python . t)))

(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-src-fontify-natively t)

(setq org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "bibtex documentation"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

;; Citations

(org-add-link-type
 "cite" 'ebib
 (lambda (path desc format)
   (cond
    ((eq format 'html)
     (format "(<cite>%s</cite>)" path))
    ((eq format 'latex)
     (if (or (not desc) (equal 0 (search "cite:" desc)))
         (format "\\cite{%s}" path)
       (format "\\cite[%s][%s]{%s}"
               (cadr (split-string desc ";"))
               (car (split-string desc ";"))  path))))))

;; Functools

(defun pamhyr-version ()
  "Return the contents of the pamhyr version file."
  (with-temp-buffer
    (insert-file-contents "../../VERSION")
    (buffer-string)))

(defun pamhyr-current-file ()
  (let ((lst (split-string (buffer-file-name) "/")))
    (string-join (nthcdr (- (length lst) 3) lst) "/")))
