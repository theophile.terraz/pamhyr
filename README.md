# Pamhyr2

Pamhyr is a free and open source graphical user interface for 1D hydro-sedimentary
modelling of rivers.

![logo](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/raw/master/src/View/ui/ressources/Pamhyr2_logo.png)

## Features

+ Define a complex river hydraulics network
+ Define the river geometry
+ Define boundary conditions
+ Define lateral contribution
+ Define river friction
+ Define reservoirs
+ Define hydraulic structures
+ Define or generate initial conditions
+ Define river sedimentary layers
+ Save the study in a unique file
+ Run Mage 8 on a study and visualize results

Let see the
[documentation](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/wikis/home)
([:fr:](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/wikis/home-fr),
[:gb:](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/wikis/home-en))
for more details.

## Install

### GNU/Linux

See documentation:
- [French :fr:](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/wikis/home/fr/Install%20on%20GNULinux)
- [English :gb:](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/wikis/home/en/Install%20on%20GNULinux)

### Windows

See documentation:
- [French :fr:](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/wikis/home/fr/Install%20on%20Windows)
- [English :gb:](https://gitlab.irstea.fr/theophile.terraz/pamhyr/-/wikis/home/en/Install%20on%20Windows)
